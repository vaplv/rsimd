/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_AVXF_H
#define RSIMD_AVXF_H

/*
 * 8 packed single precision floating-point values
 */

#include "avx.h"

#include <rsys/math.h>
#include <immintrin.h>

typedef __m256 v8f_T;

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE float*
v8f_store(float dst[8], v8f_T v)
{
  ASSERT(dst && IS_ALIGNED(dst, 32));
  _mm256_store_ps(dst, v);
  return dst;
}

static FINLINE v8f_T
v8f_load(const float src[8])
{
  ASSERT(src && IS_ALIGNED(src, 32));
  return _mm256_load_ps(src);
}

static FINLINE v8f_T
v8f_loadu(const float f[8])
{
  ASSERT(f);
  return _mm256_set_ps(f[7], f[6], f[5], f[4], f[3],f[2], f[1], f[0]);
}

static FINLINE v8f_T
v8f_set1(const float x)
{
  return _mm256_set1_ps(x);
}

static FINLINE v8f_T
v8f_set
  (const float a, const float b, const float c, const float d,
   const float e, const float f, const float g, const float h)
{
  return _mm256_set_ps(h, g, f, e, d, c, b, a);
}

static FINLINE v8f_T
v8f_zero(void)
{
  return _mm256_setzero_ps();
}

static FINLINE v8f_T
v8f_mask
  (const int32_t a, const int32_t b, const int32_t c, const int32_t d,
   const int32_t e, const int32_t f, const int32_t g, const int32_t h)
{
  return _mm256_castsi256_ps(_mm256_set_epi32(h, g, f, e, d, c, b, a));
}

static FINLINE v8f_T
v8f_mask1(const int32_t x)
{
  return _mm256_castsi256_ps(_mm256_set1_epi32(x));
}

static FINLINE v8f_T
v8f_true(void)
{
  return _mm256_castsi256_ps(_mm256_set1_epi32(~0));
}

static FINLINE v8f_T
v8f_false(void)
{
  return v8f_zero();
}

/*******************************************************************************
 * Extract components
 ******************************************************************************/
static FINLINE v4f_T
v8f_abcd(const v8f_T v)
{
  return _mm256_extractf128_ps(v, 0);
}

static FINLINE v4f_T
v8f_efgh(const v8f_T v)
{
  return  _mm256_extractf128_ps(v, 1);
}

static FINLINE int
v8f_movemask(const v8f_T v)
{
  return _mm256_movemask_ps(v);
}

/*******************************************************************************
 * Bitwise operations
 ******************************************************************************/
static FINLINE v8f_T
v8f_or(const v8f_T v0, const v8f_T v1)
{
  return _mm256_or_ps(v0, v1);
}

static FINLINE v8f_T
v8f_and(const v8f_T v0, const v8f_T v1)
{
  return _mm256_and_ps(v0, v1);
}

static FINLINE v8f_T
v8f_andnot(const v8f_T v0, const v8f_T v1)
{
  return _mm256_andnot_ps(v0, v1);
}

static FINLINE v8f_T
v8f_xor(const v8f_T v0, const v8f_T v1)
{
  return _mm256_xor_ps(v0, v1);
}

static FINLINE v8f_T
v8f_sel(const v8f_T vfalse, const v8f_T vtrue, const v8f_T vcond)
{
  return _mm256_blendv_ps(vfalse, vtrue, vcond);
}

/*******************************************************************************
 * Arithmetic operations
 ******************************************************************************/
static FINLINE v8f_T
v8f_minus(const v8f_T v)
{
  return v8f_xor(v8f_set1(-0.f), v);
}

static FINLINE v8f_T
v8f_add(const v8f_T v0, const v8f_T v1)
{
  return _mm256_add_ps(v0, v1);
}

static FINLINE v8f_T
v8f_sub(const v8f_T v0, const v8f_T v1)
{
  return _mm256_sub_ps(v0, v1);
}

static FINLINE v8f_T
v8f_mul(const v8f_T v0, const v8f_T v1)
{
  return _mm256_mul_ps(v0, v1);
}

static FINLINE v8f_T
v8f_div(const v8f_T v0, const v8f_T v1)
{
  return _mm256_div_ps(v0, v1);
}

static FINLINE v8f_T
v8f_madd(const v8f_T v0, const v8f_T v1, const v8f_T v2)
{
  return _mm256_add_ps(_mm256_mul_ps(v0, v1), v2);
}

static FINLINE v8f_T
v8f_abs(const v8f_T v)
{
  const union { int32_t i; float f; } mask = { 0x7fffffff };
  return v8f_and(v, v8f_set1(mask.f));
}

static FINLINE v8f_T
v8f_sqrt(const v8f_T v)
{
  return _mm256_sqrt_ps(v);
}

static FINLINE v8f_T
v8f_rsqrte(const v8f_T v)
{
  return _mm256_rsqrt_ps(v);
}

static FINLINE v8f_T
v8f_rsqrt(const v8f_T v)
{
  const v8f_T y = v8f_rsqrte(v);
  const v8f_T yyv = v8f_mul(v8f_mul(y, y), v);
  const v8f_T tmp = v8f_sub(v8f_set1(1.5f), v8f_mul(yyv, v8f_set1(0.5f)));
  return v8f_mul(tmp, y);
}

static FINLINE v8f_T
v8f_rcpe(const v8f_T v)
{
  return _mm256_rcp_ps(v);
}

static FINLINE v8f_T
v8f_rcp(const v8f_T v)
{
  const v8f_T y = v8f_rcpe(v);
  const v8f_T tmp = v8f_sub(v8f_set1(2.f), v8f_mul(y, v));
  return v8f_mul(tmp, y);
}

static FINLINE v8f_T
v8f_lerp(const v8f_T from, const v8f_T to, const v8f_T param)
{
  return v8f_madd(v8f_sub(to, from), param, from);
}

/*******************************************************************************
 * Comparators
 ******************************************************************************/
static FINLINE v8f_T
v8f_eq(const v8f_T v0, const v8f_T v1)
{
  return _mm256_cmp_ps(v0, v1, _CMP_EQ_OS);
}

static FINLINE v8f_T
v8f_neq(const v8f_T v0, const v8f_T v1)
{
  return _mm256_cmp_ps(v0, v1, _CMP_NEQ_OS);
}

static FINLINE v8f_T
v8f_ge(const v8f_T v0, const v8f_T v1)
{
  return _mm256_cmp_ps(v0, v1, _CMP_GE_OS);
}

static FINLINE v8f_T
v8f_le(const v8f_T v0, const v8f_T v1)
{
  return _mm256_cmp_ps(v0, v1, _CMP_LE_OS);
}

static FINLINE v8f_T
v8f_gt(const v8f_T v0, const v8f_T v1)
{
  return _mm256_cmp_ps(v0, v1, _CMP_GT_OS);
}

static FINLINE v8f_T
v8f_lt(const v8f_T v0, const v8f_T v1)
{
  return _mm256_cmp_ps(v0, v1, _CMP_LT_OS);
}

static FINLINE v8f_T
v8f_eq_eps(const v8f_T v0, const v8f_T v1, const v8f_T eps)
{
  return v8f_le(v8f_abs(v8f_sub(v0, v1)), eps);
}

static FINLINE v8f_T
v8f_min(const v8f_T v0, const v8f_T v1)
{
  return _mm256_min_ps(v0, v1);
}

static FINLINE v8f_T
v8f_max(const v8f_T v0, const v8f_T v1)
{
  return _mm256_max_ps(v0, v1);
}

static FINLINE float
v8f_reduce_min(const v8f_T v0)
{
  ALIGN(32) float tmp[8];
  const v8f_T v1 = _mm256_permute_ps(v0, _MM_SHUFFLE(1, 0, 3, 2));
  const v8f_T v2 = _mm256_min_ps(v0, v1);
  const v8f_T v3 = _mm256_permute_ps(v2, _MM_SHUFFLE(2, 3, 0, 1));
  const v8f_T v4 = _mm256_min_ps(v2, v3);
  _mm256_store_ps(tmp, v4);
  return MMIN(tmp[0], tmp[4]);
}

static FINLINE float
v8f_reduce_max(const v8f_T v0)
{
  ALIGN(32) float tmp[8];
  const v8f_T v1 = _mm256_permute_ps(v0, _MM_SHUFFLE(1, 0, 3, 2));
  const v8f_T v2 = _mm256_max_ps(v0, v1);
  const v8f_T v3 = _mm256_permute_ps(v2, _MM_SHUFFLE(2, 3, 0, 1));
  const v8f_T v4 = _mm256_max_ps(v2, v3);
  _mm256_store_ps(tmp, v4);
  return MMAX(tmp[0], tmp[4]);
}

static FINLINE v8f_T
v8f_clamp(const v8f_T v, const v8f_T vmin, const v8f_T vmax)
{
  return v8f_min(v8f_max(v, vmin), vmax);
}

#endif /* RSIMD_AVX_H */

