/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_AVXI_H
#define RSIMD_AVXI_H

/*
 * 8 packed signed integers
 */

#include <rsys/math.h>
#include <immintrin.h>

typedef __m256i v8i_T;

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE int32_t*
v8i_store(int32_t dst[8], v8i_T v)
{
  ASSERT(dst && IS_ALIGNED(dst, 32));
  _mm256_store_si256((v8i_T*)dst, v);
  return dst;
}

static FINLINE v8i_T
v8i_load(const int32_t src[8])
{
  ASSERT(src && IS_ALIGNED(src, 32));
  return _mm256_load_si256((const v8i_T*)src);
}

static FINLINE v8i_T
v8i_set1(const int32_t i)
{
  return _mm256_set1_epi32(i);
}

static FINLINE v8i_T
v8i_set
  (const int32_t a, const int32_t b, const int32_t c, const int32_t d,
   const int32_t e, const int32_t f, const int32_t g, const int32_t h)
{
  return _mm256_set_epi32(h, g, f, e, d, c, b, a);
}

static FINLINE v8i_T
v8i_zero(void)
{
  return _mm256_setzero_si256();
}

static FINLINE v8i_T
v8i_set_v4i(const v4i_T abcd, const v4i_T efgh)
{
  v8i_T tmp = v8i_zero();
  tmp = _mm256_insertf128_si256(tmp, abcd, 0);
  tmp = _mm256_insertf128_si256(tmp, efgh, 1);
  return tmp;
}

/*******************************************************************************
 * Extract components
 ******************************************************************************/
static FINLINE v4i_T
v8i_abcd(const v8i_T v)
{
  return _mm256_extractf128_si256(v, 0);
}

static FINLINE v4i_T
v8i_efgh(const v8i_T v)
{
  return _mm256_extractf128_si256(v, 1);
}

/*******************************************************************************
 * Bitwise operators
 ******************************************************************************/
static FINLINE v8i_T
v8i_or(const v8i_T v0, const v8i_T v1)
{
  const v8f_T a = _mm256_castsi256_ps(v0);
  const v8f_T b = _mm256_castsi256_ps(v1);
  const v8f_T c = _mm256_or_ps(a, b);
  return _mm256_castps_si256(c);
}

static FINLINE v8i_T
v8i_and(const v8i_T v0, const v8i_T v1)
{
  const v8f_T a = _mm256_castsi256_ps(v0);
  const v8f_T b = _mm256_castsi256_ps(v1);
  const v8f_T c = _mm256_and_ps(a, b);
  return _mm256_castps_si256(c);
}

static FINLINE v8i_T
v8i_andnot(const v8i_T v0, const v8i_T v1)
{
  const v8f_T a = _mm256_castsi256_ps(v0);
  const v8f_T b = _mm256_castsi256_ps(v1);
  const v8f_T c = _mm256_andnot_ps(a, b);
  return _mm256_castps_si256(c);
}

static FINLINE v8i_T
v8i_xor(const v8i_T v0, const v8i_T v1)
{
  const v8f_T a = _mm256_castsi256_ps(v0);
  const v8f_T b = _mm256_castsi256_ps(v1);
  const v8f_T c = _mm256_xor_ps(a, b);
  return _mm256_castps_si256(c);
}

/*******************************************************************************
 * Comparators
 ******************************************************************************/
static FINLINE v8i_T
v8i_eq(const v8i_T v0, const v8i_T v1)
{
  const v4i_T v0_abcd = v8i_abcd(v0);
  const v4i_T v0_efgh = v8i_efgh(v0);
  const v4i_T v1_abcd = v8i_abcd(v1);
  const v4i_T v1_efgh = v8i_efgh(v1);
  const v4i_T abcd = v4i_eq(v0_abcd, v1_abcd);
  const v4i_T efgh = v4i_eq(v0_efgh, v1_efgh);
  return v8i_set_v4i(abcd, efgh);
}

static FINLINE v8i_T
v8i_neq(const v8i_T v0, const v8i_T v1)
{
  const v4i_T v0_abcd = v8i_abcd(v0);
  const v4i_T v0_efgh = v8i_efgh(v0);
  const v4i_T v1_abcd = v8i_abcd(v1);
  const v4i_T v1_efgh = v8i_efgh(v1);
  const v4i_T abcd = v4i_neq(v0_abcd, v1_abcd);
  const v4i_T efgh = v4i_neq(v0_efgh, v1_efgh);
  return v8i_set_v4i(abcd, efgh);
}

static FINLINE v8i_T
v8i_sel(const v8i_T vfalse, const v8i_T vtrue, const v8i_T vcond)
{
  const v8f_T a = _mm256_castsi256_ps(vfalse);
  const v8f_T b = _mm256_castsi256_ps(vtrue);
  const v8f_T c = _mm256_castsi256_ps(vcond);
  return _mm256_castps_si256(_mm256_blendv_ps(a, b, c));
}

static FINLINE v8i_T
v8i_min(const v8i_T v0, const v8i_T v1)
{
  const v4i_T v0_abcd = v8i_abcd(v0);
  const v4i_T v0_efgh = v8i_efgh(v0);
  const v4i_T v1_abcd = v8i_abcd(v1);
  const v4i_T v1_efgh = v8i_efgh(v1);
  const v4i_T abcd = v4i_min(v0_abcd, v1_abcd);
  const v4i_T efgh = v4i_min(v0_efgh, v1_efgh);
  return v8i_set_v4i(abcd, efgh);
}

static FINLINE v8i_T
v8i_max(const v8i_T v0, const v8i_T v1)
{
  const v4i_T v0_abcd = v8i_abcd(v0);
  const v4i_T v0_efgh = v8i_efgh(v0);
  const v4i_T v1_abcd = v8i_abcd(v1);
  const v4i_T v1_efgh = v8i_efgh(v1);
  const v4i_T abcd = v4i_max(v0_abcd, v1_abcd);
  const v4i_T efgh = v4i_max(v0_efgh, v1_efgh);
  return v8i_set_v4i(abcd, efgh);
}

static FINLINE int32_t
v8i_reduce_min_i32(const v8i_T v)
{
  const v4i_T tmp = v4i_min(v8i_abcd(v), v8i_efgh(v));
  return v4i_x(v4i_reduce_min(tmp));
}

static FINLINE int32_t
v8i_reduce_max_i32(const v8i_T v)
{
  const v4i_T tmp = v4i_max(v8i_abcd(v), v8i_efgh(v));
  return v4i_x(v4i_reduce_max(tmp));
}

#endif /* RSIMD_AVXI_H */

