/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"

int
main(int argc, char** argv)
{
  v8i_T i, j, k;
  ALIGN(32) int32_t tmp[8] = {0,1,2,3,4,5,6,7};
  (void)argc, (void)argv;

  i = v8i_load(tmp);
  CHK(v4i_x(v8i_abcd(i)) == 0);
  CHK(v4i_y(v8i_abcd(i)) == 1);
  CHK(v4i_z(v8i_abcd(i)) == 2);
  CHK(v4i_w(v8i_abcd(i)) == 3);
  CHK(v4i_x(v8i_efgh(i)) == 4);
  CHK(v4i_y(v8i_efgh(i)) == 5);
  CHK(v4i_z(v8i_efgh(i)) == 6);
  CHK(v4i_w(v8i_efgh(i)) == 7);

  tmp[0]= tmp[1] = tmp[2] = tmp[3] = 0;
  tmp[4]= tmp[5] = tmp[6] = tmp[7] = 0;
  CHK(v8i_store(tmp, i) == tmp);
  CHK(tmp[0] == 0);
  CHK(tmp[1] == 1);
  CHK(tmp[2] == 2);
  CHK(tmp[3] == 3);
  CHK(tmp[4] == 4);
  CHK(tmp[5] == 5);
  CHK(tmp[6] == 6);
  CHK(tmp[7] == 7);

  i = v8i_set(1, 2, 3, 4, 5, 6, 7, 8);
  CHK(v4i_x(v8i_abcd(i)) == 1);
  CHK(v4i_y(v8i_abcd(i)) == 2);
  CHK(v4i_z(v8i_abcd(i)) == 3);
  CHK(v4i_w(v8i_abcd(i)) == 4);
  CHK(v4i_x(v8i_efgh(i)) == 5);
  CHK(v4i_y(v8i_efgh(i)) == 6);
  CHK(v4i_z(v8i_efgh(i)) == 7);
  CHK(v4i_w(v8i_efgh(i)) == 8);

  i = v8i_set1(-1);
  CHK(v4i_x(v8i_abcd(i)) == -1);
  CHK(v4i_y(v8i_abcd(i)) == -1);
  CHK(v4i_z(v8i_abcd(i)) == -1);
  CHK(v4i_w(v8i_abcd(i)) == -1);
  CHK(v4i_x(v8i_efgh(i)) == -1);
  CHK(v4i_y(v8i_efgh(i)) == -1);
  CHK(v4i_z(v8i_efgh(i)) == -1);
  CHK(v4i_w(v8i_efgh(i)) == -1);

  i = v8i_zero();
  CHK(v4i_x(v8i_abcd(i)) == 0);
  CHK(v4i_y(v8i_abcd(i)) == 0);
  CHK(v4i_z(v8i_abcd(i)) == 0);
  CHK(v4i_w(v8i_abcd(i)) == 0);
  CHK(v4i_x(v8i_efgh(i)) == 0);
  CHK(v4i_y(v8i_efgh(i)) == 0);
  CHK(v4i_z(v8i_efgh(i)) == 0);
  CHK(v4i_w(v8i_efgh(i)) == 0);

  i = v8i_set_v4i(v4i_set(-1,-2,3,4), v4i_set(5,6,-7,-8));
  CHK(v4i_x(v8i_abcd(i)) ==-1);
  CHK(v4i_y(v8i_abcd(i)) ==-2);
  CHK(v4i_z(v8i_abcd(i)) == 3);
  CHK(v4i_w(v8i_abcd(i)) == 4);
  CHK(v4i_x(v8i_efgh(i)) == 5);
  CHK(v4i_y(v8i_efgh(i)) == 6);
  CHK(v4i_z(v8i_efgh(i)) ==-7);
  CHK(v4i_w(v8i_efgh(i)) ==-8);

  i = v8i_set
    (0x00010203, 0x04050607, 0x08090A0B, 0x0C0D0E0F,
     0x00102030, 0x40506070, (int32_t)0x8090A0B0, (int32_t)0xC0D0E0F0);
  j = v8i_set
    (0x01020401, 0x70605040, 0x0F1F2F3F, 0x00000000,
     0x10204010, 0x06050400, (int32_t)0xF1F2F3F0, 0x10000000);
  k = v8i_or(i, j);
  CHK(v4i_x(v8i_abcd(k)) == (int32_t)0x01030603);
  CHK(v4i_y(v8i_abcd(k)) == (int32_t)0x74655647);
  CHK(v4i_z(v8i_abcd(k)) == (int32_t)0x0F1F2F3F);
  CHK(v4i_w(v8i_abcd(k)) == (int32_t)0x0C0D0E0F);
  CHK(v4i_x(v8i_efgh(k)) == (int32_t)0x10306030);
  CHK(v4i_y(v8i_efgh(k)) == (int32_t)0x46556470);
  CHK(v4i_z(v8i_efgh(k)) == (int32_t)0xF1F2F3F0);
  CHK(v4i_w(v8i_efgh(k)) == (int32_t)0xD0D0E0F0);

  k = v8i_and(i, j);
  CHK(v4i_x(v8i_abcd(k)) == (int32_t)0x00000001);
  CHK(v4i_y(v8i_abcd(k)) == (int32_t)0x00000000);
  CHK(v4i_z(v8i_abcd(k)) == (int32_t)0x08090A0B);
  CHK(v4i_w(v8i_abcd(k)) == (int32_t)0x00000000);
  CHK(v4i_x(v8i_efgh(k)) == (int32_t)0x00000010);
  CHK(v4i_y(v8i_efgh(k)) == (int32_t)0x00000000);
  CHK(v4i_z(v8i_efgh(k)) == (int32_t)0x8090A0B0);
  CHK(v4i_w(v8i_efgh(k)) == (int32_t)0x00000000);

  k = v8i_andnot(i, j);
  CHK(v4i_x(v8i_abcd(k)) == (int32_t)0x01020400);
  CHK(v4i_y(v8i_abcd(k)) == (int32_t)0x70605040);
  CHK(v4i_z(v8i_abcd(k)) == (int32_t)0x07162534);
  CHK(v4i_w(v8i_abcd(k)) == (int32_t)0x00000000);
  CHK(v4i_x(v8i_efgh(k)) == (int32_t)0x10204000);
  CHK(v4i_y(v8i_efgh(k)) == (int32_t)0x06050400);
  CHK(v4i_z(v8i_efgh(k)) == (int32_t)0x71625340);
  CHK(v4i_w(v8i_efgh(k)) == (int32_t)0x10000000);

  k = v8i_xor(i, j);
  CHK(v4i_x(v8i_abcd(k)) == (int32_t)0x01030602);
  CHK(v4i_y(v8i_abcd(k)) == (int32_t)0x74655647);
  CHK(v4i_z(v8i_abcd(k)) == (int32_t)0x07162534);
  CHK(v4i_w(v8i_abcd(k)) == (int32_t)0x0C0D0E0F);
  CHK(v4i_x(v8i_efgh(k)) == (int32_t)0x10306020);
  CHK(v4i_y(v8i_efgh(k)) == (int32_t)0x46556470);
  CHK(v4i_z(v8i_efgh(k)) == (int32_t)0x71625340);
  CHK(v4i_w(v8i_efgh(k)) == (int32_t)0XD0D0E0F0);

  i = v8i_set( 1, 2,3,4,5, 6,7,8);
  j = v8i_set(-2,-4,3,6,5,-1,8,8);

  k = v8i_eq(i, j);
  CHK(v4i_x(v8i_abcd(k)) == 0);
  CHK(v4i_y(v8i_abcd(k)) == 0);
  CHK(v4i_z(v8i_abcd(k)) ==~0);
  CHK(v4i_w(v8i_abcd(k)) == 0);
  CHK(v4i_x(v8i_efgh(k)) ==~0);
  CHK(v4i_y(v8i_efgh(k)) == 0);
  CHK(v4i_z(v8i_efgh(k)) == 0);
  CHK(v4i_w(v8i_efgh(k)) ==~0);

  k = v8i_neq(i, j);
  CHK(v4i_x(v8i_abcd(k)) ==~0);
  CHK(v4i_y(v8i_abcd(k)) ==~0);
  CHK(v4i_z(v8i_abcd(k)) == 0);
  CHK(v4i_w(v8i_abcd(k)) ==~0);
  CHK(v4i_x(v8i_efgh(k)) == 0);
  CHK(v4i_y(v8i_efgh(k)) ==~0);
  CHK(v4i_z(v8i_efgh(k)) ==~0);
  CHK(v4i_w(v8i_efgh(k)) == 0);

  k = v8i_sel(i, j, v8i_set(~0,~0,0,~0,0,0,~0,0));
  CHK(v4i_x(v8i_abcd(k)) ==-2);
  CHK(v4i_y(v8i_abcd(k)) ==-4);
  CHK(v4i_z(v8i_abcd(k)) == 3);
  CHK(v4i_w(v8i_abcd(k)) == 6);
  CHK(v4i_x(v8i_efgh(k)) == 5);
  CHK(v4i_y(v8i_efgh(k)) == 6);
  CHK(v4i_z(v8i_efgh(k)) == 8);
  CHK(v4i_w(v8i_efgh(k)) == 8);

  k = v8i_min(i, j);
  CHK(v4i_x(v8i_abcd(k)) ==-2);
  CHK(v4i_y(v8i_abcd(k)) ==-4);
  CHK(v4i_z(v8i_abcd(k)) == 3);
  CHK(v4i_w(v8i_abcd(k)) == 4);
  CHK(v4i_x(v8i_efgh(k)) == 5);
  CHK(v4i_y(v8i_efgh(k)) ==-1);
  CHK(v4i_z(v8i_efgh(k)) == 7);
  CHK(v4i_w(v8i_efgh(k)) == 8);

  k = v8i_max(i, j);
  CHK(v4i_x(v8i_abcd(k)) == 1);
  CHK(v4i_y(v8i_abcd(k)) == 2);
  CHK(v4i_z(v8i_abcd(k)) == 3);
  CHK(v4i_w(v8i_abcd(k)) == 6);
  CHK(v4i_x(v8i_efgh(k)) == 5);
  CHK(v4i_y(v8i_efgh(k)) == 6);
  CHK(v4i_z(v8i_efgh(k)) == 8);
  CHK(v4i_w(v8i_efgh(k)) == 8);

  CHK(v8i_reduce_min_i32(i) == 1);
  CHK(v8i_reduce_min_i32(j) ==-4);
  CHK(v8i_reduce_max_i32(i) == 8);
  CHK(v8i_reduce_max_i32(j) == 8);

  return 0;
}
