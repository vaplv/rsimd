/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

/*
 * Header used to generate funcs on SoA SIMD float vectors of Y dimensions
 */

#ifndef SOAXFY_BEGIN_H
  #error "The soaXfY_begin.h header must be included first"
#endif

/* Force GCC to unroll the loops */
#ifdef COMPILER_GCC
  #pragma GCC push_options
  #pragma GCC optimize("unroll-loops")
#endif

#if RSIMD_SOA_DIMENSION__ <= 4
static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY_PREFIX__
  (RSIMD_vXf_T__* dst
  ,const RSIMD_vXf_T__ x
  ,const RSIMD_vXf_T__ y
#if RSIMD_SOA_DIMENSION__ > 2
  ,const RSIMD_vXf_T__ z
#endif
#if RSIMD_SOA_DIMENSION__ > 3
  ,const RSIMD_vXf_T__ w
#endif
  )
{
  ASSERT(dst);
  dst[0] = x;
  dst[1] = y;
#if RSIMD_SOA_DIMENSION__ > 2
  dst[2] = z;
#endif
#if RSIMD_SOA_DIMENSION__ > 3
  dst[3] = w;
#endif
  return dst;
}
#endif

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(splat)(RSIMD_vXf_T__* dst, const RSIMD_vXf_T__ val)
{
  int i;
  ASSERT(dst);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    dst[i] = val;
  return dst;
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(set__)(RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* src)
{
  int i;
  ASSERT(dst && src);
  ASSERT(!MEM_AREA_OVERLAP(dst, SIZEOF_RSIMD_soaXfY__, src, SIZEOF_RSIMD_soaXfY__));
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    dst[i] = src[i];
  return dst;
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(set)(RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* src)
{
  ASSERT(dst && src);
  if(!MEM_AREA_OVERLAP(dst, SIZEOF_RSIMD_soaXfY__, src, SIZEOF_RSIMD_soaXfY__)) {
    return RSIMD_soaXfY__(set__)(dst, src);
  } else {
    RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
    return RSIMD_soaXfY__(set__)(dst, RSIMD_soaXfY__(set__)(tmp, src));
  }
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(dot)(const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ dot;
  int i;
  ASSERT(a && b);
  dot = RSIMD_vXf__(mul)(a[0], b[0]);
  FOR_EACH(i, 1, RSIMD_SOA_DIMENSION__) {
    dot = RSIMD_vXf__(madd)(a[i], b[i], dot);
  }
  return dot;
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(len)(const RSIMD_vXf_T__* a)
{
  ASSERT(a);
  return RSIMD_vXf__(sqrt)(RSIMD_soaXfY__(dot)(a, a));
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(normalize)(RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  RSIMD_vXf_T__ sqr_len, rcp_len;
  RSIMD_vXf_T__ mask;
  int i;
  ASSERT(dst && a);

  sqr_len = RSIMD_soaXfY__(dot)(a, a);
  mask = RSIMD_vXf__(neq)(sqr_len, RSIMD_vXf__(zero)());
  rcp_len = RSIMD_vXf__(rsqrt)(sqr_len);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(and)(mask, RSIMD_vXf__(mul)(a[i], rcp_len));
  RSIMD_soaXfY__(set__)(dst, tmp);
  return RSIMD_vXf__(mul)(sqr_len, rcp_len);
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(is_normalized)(const RSIMD_vXf_T__* a)
{
  return RSIMD_vXf__(eq_eps)
    (RSIMD_soaXfY__(len)(a),
     RSIMD_vXf__(set1)(1.f),
     RSIMD_vXf__(set1)(1.e-6f));
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(add)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a && b);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(add)(a[i], b[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(addf)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__ f)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(add)(a[i], f);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(sub)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a && b);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(sub)(a[i], b[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(subf)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__ f)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(sub)(a[i], f);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(mul)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a && b);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(mul)(a[i], b[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(mulf)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__ f)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(mul)(a[i], f);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(div)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a && b);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(div)(a[i], b[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(divf)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__ f)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(div)(a[i], f);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(minus)(RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(minus)(a[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(sum)(const RSIMD_vXf_T__* a)
{
  RSIMD_vXf_T__ f;
  int i = 0;
  ASSERT(a);
  f = a[i];
  FOR_EACH(i, 1, RSIMD_SOA_DIMENSION__)
    f = RSIMD_vXf__(add)(f, a[i]);
  return f;
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(lerp)
  (RSIMD_vXf_T__* dst,
   const RSIMD_vXf_T__* from,
   const RSIMD_vXf_T__* to,
   const RSIMD_vXf_T__ t)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && from && to);

  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(lerp)(from[i], to[i], t);
  RSIMD_soaXfY__(set__)(dst, tmp);
  return dst;
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(eq)(const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ is_eq;
  int i = 0;
  ASSERT(a && b);
  is_eq = RSIMD_vXf__(eq)(a[0], b[0]);
  FOR_EACH(i, 1, RSIMD_SOA_DIMENSION__)
    is_eq = RSIMD_vXf__(and)(is_eq, RSIMD_vXf__(eq)(a[i], b[i]));
  return is_eq;
}

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(eq_eps)
  (const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b, const RSIMD_vXf_T__ eps)
{
  RSIMD_vXf_T__ is_eq;
  int i = 0;
  ASSERT(a && b);
  is_eq = RSIMD_vXf__(eq_eps)(a[0], b[0], eps);
  FOR_EACH(i, 1, RSIMD_SOA_DIMENSION__)
    is_eq = RSIMD_vXf__(and)(is_eq, RSIMD_vXf__(eq_eps)(a[i], b[i], eps));
  return is_eq;
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(max)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a && b);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(max)(a[i], b[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(min)
  (RSIMD_vXf_T__* dst, const RSIMD_vXf_T__* a, const RSIMD_vXf_T__* b)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && a && b);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(min)(a[i], b[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(sel)
  (RSIMD_vXf_T__* dst,
   const RSIMD_vXf_T__* vfalse,
   const RSIMD_vXf_T__* vtrue,
   const RSIMD_vXf_T__ cond)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && vfalse && vtrue);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(sel)(vfalse[i], vtrue[i], cond);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

static FINLINE RSIMD_vXf_T__*
RSIMD_soaXfY__(selv)
  (RSIMD_vXf_T__* dst,
   const RSIMD_vXf_T__* vfalse,
   const RSIMD_vXf_T__* vtrue,
   const RSIMD_vXf_T__* vcond)
{
  RSIMD_vXf_T__ tmp[RSIMD_SOA_DIMENSION__];
  int i;
  ASSERT(dst && vfalse && vtrue);
  FOR_EACH(i, 0, RSIMD_SOA_DIMENSION__)
    tmp[i] = RSIMD_vXf__(sel)(vfalse[i], vtrue[i], vcond[i]);
  return RSIMD_soaXfY__(set__)(dst, tmp);
}

/* Restore compilation parameters */
#ifdef COMPILER_GCC
  #pragma GCC pop_options
#endif

