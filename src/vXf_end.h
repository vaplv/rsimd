/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXF_BEGIN_H
  #error "The vXf_begin.h file must be included"
#endif

/* Undef helper macros */
#undef RSIMD_vXf__
#undef RSIMD_vXf_T__
#undef RSIMD_Sleef__
#undef RSIMD_Sleef_ULP__
#undef RSIMD_Sleef_vecf__

/* Undef parameters */
#undef RSIMD_WIDTH__

#undef VXF_BEGIN_H

