/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SOAXFY_BEGIN_H
  #error "The soaXfY_begin.h file must be included"
#endif

/* Undef helper macros */
#undef RSIMD_soaXfY_PREFIX__
#undef RSIMD_soaXfY__
#undef SIZEOF_RSIMD_soaXfY__

/* Undef parameters */
#undef RSIMD_SOA_DIMENSION__
#undef RSIMD_WIDTH__

#undef SOAXFY_BEGIN_H

#include "vXf_end.h"
