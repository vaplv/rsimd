/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"

int
main(int argc, char** argv)
{
  v8f_T i, j, k;
  ALIGN(32) union { int32_t i[8]; float f[8]; } cast;
  ALIGN(32) float tmp[9] = {0.f,1.f,2.f,3.f,4.f,5.f,6.f,7.f,8.f};
  (void)argc, (void)argv;

  i = v8f_loadu(tmp+1);
  CHK(v4f_x(v8f_abcd(i)) == 1.f);
  CHK(v4f_y(v8f_abcd(i)) == 2.f);
  CHK(v4f_z(v8f_abcd(i)) == 3.f);
  CHK(v4f_w(v8f_abcd(i)) == 4.f);
  CHK(v4f_x(v8f_efgh(i)) == 5.f);
  CHK(v4f_y(v8f_efgh(i)) == 6.f);
  CHK(v4f_z(v8f_efgh(i)) == 7.f);
  CHK(v4f_w(v8f_efgh(i)) == 8.f);

  i = v8f_load(tmp);
  CHK(v4f_x(v8f_abcd(i)) == 0.f);
  CHK(v4f_y(v8f_abcd(i)) == 1.f);
  CHK(v4f_z(v8f_abcd(i)) == 2.f);
  CHK(v4f_w(v8f_abcd(i)) == 3.f);
  CHK(v4f_x(v8f_efgh(i)) == 4.f);
  CHK(v4f_y(v8f_efgh(i)) == 5.f);
  CHK(v4f_z(v8f_efgh(i)) == 6.f);
  CHK(v4f_w(v8f_efgh(i)) == 7.f);

  tmp[0] = tmp[1] = tmp[2] = tmp[3] = 0.f;
  tmp[4] = tmp[5] = tmp[6] = tmp[7] = 0.f;
  CHK(v8f_store(tmp, i) == tmp);
  CHK(tmp[0] == 0.f);
  CHK(tmp[1] == 1.f);
  CHK(tmp[2] == 2.f);
  CHK(tmp[3] == 3.f);
  CHK(tmp[4] == 4.f);
  CHK(tmp[5] == 5.f);
  CHK(tmp[6] == 6.f);
  CHK(tmp[7] == 7.f);
  CHK(tmp[8] == 8.f);

  i = v8f_set1(-2.f);
  CHK(v4f_x(v8f_abcd(i)) == -2.f);
  CHK(v4f_y(v8f_abcd(i)) == -2.f);
  CHK(v4f_z(v8f_abcd(i)) == -2.f);
  CHK(v4f_w(v8f_abcd(i)) == -2.f);
  CHK(v4f_x(v8f_efgh(i)) == -2.f);
  CHK(v4f_y(v8f_efgh(i)) == -2.f);
  CHK(v4f_z(v8f_efgh(i)) == -2.f);
  CHK(v4f_w(v8f_efgh(i)) == -2.f);

  i = v8f_set(0.f,1.f,2.f,3.f,4.f,5.f,6.f,7.f);
  CHK(v4f_x(v8f_abcd(i)) == 0.f);
  CHK(v4f_y(v8f_abcd(i)) == 1.f);
  CHK(v4f_z(v8f_abcd(i)) == 2.f);
  CHK(v4f_w(v8f_abcd(i)) == 3.f);
  CHK(v4f_x(v8f_efgh(i)) == 4.f);
  CHK(v4f_y(v8f_efgh(i)) == 5.f);
  CHK(v4f_z(v8f_efgh(i)) == 6.f);
  CHK(v4f_w(v8f_efgh(i)) == 7.f);

  i = v8f_zero();
  CHK(v4f_x(v8f_abcd(i)) == 0.f);
  CHK(v4f_y(v8f_abcd(i)) == 0.f);
  CHK(v4f_z(v8f_abcd(i)) == 0.f);
  CHK(v4f_w(v8f_abcd(i)) == 0.f);
  CHK(v4f_x(v8f_efgh(i)) == 0.f);
  CHK(v4f_y(v8f_efgh(i)) == 0.f);
  CHK(v4f_z(v8f_efgh(i)) == 0.f);
  CHK(v4f_w(v8f_efgh(i)) == 0.f);

  i = v8f_mask(~0,~0,0,0,0,~0,~0,0);
  cast.f[0] = v4f_x(v8f_abcd(i)); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(v8f_abcd(i)); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(v8f_abcd(i)); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(v8f_abcd(i)); CHK(cast.i[3] == (int32_t)0x00000000);
  cast.f[4] = v4f_x(v8f_efgh(i)); CHK(cast.i[4] == (int32_t)0x00000000);
  cast.f[5] = v4f_y(v8f_efgh(i)); CHK(cast.i[5] == (int32_t)0xFFFFFFFF);
  cast.f[6] = v4f_z(v8f_efgh(i)); CHK(cast.i[6] == (int32_t)0xFFFFFFFF);
  cast.f[7] = v4f_w(v8f_efgh(i)); CHK(cast.i[7] == (int32_t)0x00000000);

  i = v8f_mask1(~0);
  cast.f[0] = v4f_x(v8f_abcd(i)); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(v8f_abcd(i)); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(v8f_abcd(i)); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(v8f_abcd(i)); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);
  cast.f[4] = v4f_x(v8f_efgh(i)); CHK(cast.i[4] == (int32_t)0xFFFFFFFF);
  cast.f[5] = v4f_y(v8f_efgh(i)); CHK(cast.i[5] == (int32_t)0xFFFFFFFF);
  cast.f[6] = v4f_z(v8f_efgh(i)); CHK(cast.i[6] == (int32_t)0xFFFFFFFF);
  cast.f[7] = v4f_w(v8f_efgh(i)); CHK(cast.i[7] == (int32_t)0xFFFFFFFF);

  i = v8f_true();
  cast.f[0] = v4f_x(v8f_abcd(i)); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(v8f_abcd(i)); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(v8f_abcd(i)); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(v8f_abcd(i)); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);
  cast.f[4] = v4f_x(v8f_efgh(i)); CHK(cast.i[4] == (int32_t)0xFFFFFFFF);
  cast.f[5] = v4f_y(v8f_efgh(i)); CHK(cast.i[5] == (int32_t)0xFFFFFFFF);
  cast.f[6] = v4f_z(v8f_efgh(i)); CHK(cast.i[6] == (int32_t)0xFFFFFFFF);
  cast.f[7] = v4f_w(v8f_efgh(i)); CHK(cast.i[7] == (int32_t)0xFFFFFFFF);

  i = v8f_false();
  cast.f[0] = v4f_x(v8f_abcd(i)); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(v8f_abcd(i)); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(v8f_abcd(i)); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(v8f_abcd(i)); CHK(cast.i[3] == (int32_t)0x00000000);
  cast.f[4] = v4f_x(v8f_efgh(i)); CHK(cast.i[4] == (int32_t)0x00000000);
  cast.f[5] = v4f_y(v8f_efgh(i)); CHK(cast.i[5] == (int32_t)0x00000000);
  cast.f[6] = v4f_z(v8f_efgh(i)); CHK(cast.i[6] == (int32_t)0x00000000);
  cast.f[7] = v4f_w(v8f_efgh(i)); CHK(cast.i[7] == (int32_t)0x00000000);

  i = v8f_mask(~0,~0,0,0,0,~0,~0,0);
  j = v8f_mask(~0,0,~0,0,0,~0,0,~0);
  k = v8f_or(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == (int32_t)0x00000000);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == (int32_t)0x00000000);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == (int32_t)0xFFFFFFFF);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == (int32_t)0xFFFFFFFF);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == (int32_t)0xFFFFFFFF);

  k = v8f_and(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == (int32_t)0x00000000);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == (int32_t)0x00000000);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == (int32_t)0xFFFFFFFF);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == (int32_t)0x00000000);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == (int32_t)0x00000000);

  k = v8f_andnot(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == (int32_t)0x00000000);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == (int32_t)0x00000000);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == (int32_t)0x00000000);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == (int32_t)0x00000000);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == (int32_t)0xFFFFFFFF);

  k = v8f_xor(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == (int32_t)0x00000000);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == (int32_t)0x00000000);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == (int32_t)0x00000000);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == (int32_t)0xFFFFFFFF);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == (int32_t)0xFFFFFFFF);

  CHK(v8f_movemask(k) == 0xC6);
  i = v8f_mask
    ((int32_t)0x01020401, (int32_t)0x80605040, (int32_t)0x7F1F2F3F, (int32_t)0,
     (int32_t)0xF0000000, (int32_t)0xFFFFFFFF, (int32_t)0x7FFFFFFF, (int32_t)~0);
  CHK(v8f_movemask(i) == 0xB2);

  i = v8f_set(0.f,1.f,2.f,3.f,4.f,5.f,6.f,7.f);
  j = v8f_set(8.f,9.f,10.f,11.f,12.f,13.f,14.f,15.f);
  k = v8f_sel(i, j, v8f_mask(~0,~0,0,0,0,~0,~0,0));
  CHK(v4f_x(v8f_abcd(k)) == 8.f);
  CHK(v4f_y(v8f_abcd(k)) == 9.f);
  CHK(v4f_z(v8f_abcd(k)) == 2.f);
  CHK(v4f_w(v8f_abcd(k)) == 3.f);
  CHK(v4f_x(v8f_efgh(k)) == 4.f);
  CHK(v4f_y(v8f_efgh(k)) == 13.f);
  CHK(v4f_z(v8f_efgh(k)) == 14.f);
  CHK(v4f_w(v8f_efgh(k)) == 7.f);

  k = v8f_minus(i);
  CHK(v4f_x(v8f_abcd(k)) == -0.f);
  CHK(v4f_y(v8f_abcd(k)) == -1.f);
  CHK(v4f_z(v8f_abcd(k)) == -2.f);
  CHK(v4f_w(v8f_abcd(k)) == -3.f);
  CHK(v4f_x(v8f_efgh(k)) == -4.f);
  CHK(v4f_y(v8f_efgh(k)) == -5.f);
  CHK(v4f_z(v8f_efgh(k)) == -6.f);
  CHK(v4f_w(v8f_efgh(k)) == -7.f);

  k = v8f_add(i, j);
  CHK(v4f_x(v8f_abcd(k)) == 8.f);
  CHK(v4f_y(v8f_abcd(k)) == 10.f);
  CHK(v4f_z(v8f_abcd(k)) == 12.f);
  CHK(v4f_w(v8f_abcd(k)) == 14.f);
  CHK(v4f_x(v8f_efgh(k)) == 16.f);
  CHK(v4f_y(v8f_efgh(k)) == 18.f);
  CHK(v4f_z(v8f_efgh(k)) == 20.f);
  CHK(v4f_w(v8f_efgh(k)) == 22.f);

  k = v8f_sub(i, j);
  CHK(v4f_x(v8f_abcd(k)) == -8.f);
  CHK(v4f_y(v8f_abcd(k)) == -8.f);
  CHK(v4f_z(v8f_abcd(k)) == -8.f);
  CHK(v4f_w(v8f_abcd(k)) == -8.f);
  CHK(v4f_x(v8f_efgh(k)) == -8.f);
  CHK(v4f_y(v8f_efgh(k)) == -8.f);
  CHK(v4f_z(v8f_efgh(k)) == -8.f);
  CHK(v4f_w(v8f_efgh(k)) == -8.f);

  k = v8f_mul(i, j);
  CHK(v4f_x(v8f_abcd(k)) == 0.f);
  CHK(v4f_y(v8f_abcd(k)) == 9.f);
  CHK(v4f_z(v8f_abcd(k)) == 20.f);
  CHK(v4f_w(v8f_abcd(k)) == 33.f);
  CHK(v4f_x(v8f_efgh(k)) == 48.f);
  CHK(v4f_y(v8f_efgh(k)) == 65.f);
  CHK(v4f_z(v8f_efgh(k)) == 84.f);
  CHK(v4f_w(v8f_efgh(k)) == 105.f);

  k = v8f_div(i, j);
  CHK(v4f_x(v8f_abcd(k)) == 0.f);
  CHK(v4f_y(v8f_abcd(k)) == 1.f/9.f);
  CHK(v4f_z(v8f_abcd(k)) == 0.2f);
  CHK(v4f_w(v8f_abcd(k)) == 3.f/11.f);
  CHK(v4f_x(v8f_efgh(k)) == 1.f/3.f);
  CHK(v4f_y(v8f_efgh(k)) == 5.f/13.f);
  CHK(v4f_z(v8f_efgh(k)) == 3.f/7.f);
  CHK(v4f_w(v8f_efgh(k)) == 7.f/15.f);

  k = v8f_set(0.1f,0.2f,0.3f,0.4f,0.5f,0.6f,0.7f,0.8f);
  k = v8f_madd(i, j, k);
  CHK(v4f_x(v8f_abcd(k)) == 0.1f);
  CHK(v4f_y(v8f_abcd(k)) == 9.2f);
  CHK(v4f_z(v8f_abcd(k)) == 20.3f);
  CHK(v4f_w(v8f_abcd(k)) == 33.4f);
  CHK(v4f_x(v8f_efgh(k)) == 48.5f);
  CHK(v4f_y(v8f_efgh(k)) == 65.6f);
  CHK(v4f_z(v8f_efgh(k)) == 84.7f);
  CHK(v4f_w(v8f_efgh(k)) == 105.8f);

  k = v8f_abs(v8f_minus(i));
  CHK(v4f_x(v8f_abcd(k)) == 0.f);
  CHK(v4f_y(v8f_abcd(k)) == 1.f);
  CHK(v4f_z(v8f_abcd(k)) == 2.f);
  CHK(v4f_w(v8f_abcd(k)) == 3.f);
  CHK(v4f_x(v8f_efgh(k)) == 4.f);
  CHK(v4f_y(v8f_efgh(k)) == 5.f);
  CHK(v4f_z(v8f_efgh(k)) == 6.f);
  CHK(v4f_w(v8f_efgh(k)) == 7.f);

  i = v8f_set(1.f, 4.f, 9.f, 16.f, 25.f, 36.f, 49.f, 64.f);
  k = v8f_sqrt(i);
  CHK(v4f_x(v8f_abcd(k)) == 1.f);
  CHK(v4f_y(v8f_abcd(k)) == 2.f);
  CHK(v4f_z(v8f_abcd(k)) == 3.f);
  CHK(v4f_w(v8f_abcd(k)) == 4.f);
  CHK(v4f_x(v8f_efgh(k)) == 5.f);
  CHK(v4f_y(v8f_efgh(k)) == 6.f);
  CHK(v4f_z(v8f_efgh(k)) == 7.f);
  CHK(v4f_w(v8f_efgh(k)) == 8.f);

  k = v8f_rsqrte(i);
  CHK(eq_epsf(v4f_x(v8f_abcd(k)), 1.f/1.f, 1.e-3f));
  CHK(eq_epsf(v4f_y(v8f_abcd(k)), 1.f/2.f, 1.e-3f));
  CHK(eq_epsf(v4f_z(v8f_abcd(k)), 1.f/3.f, 1.e-3f));
  CHK(eq_epsf(v4f_w(v8f_abcd(k)), 1.f/4.f, 1.e-3f));
  CHK(eq_epsf(v4f_x(v8f_efgh(k)), 1.f/5.f, 1.e-3f));
  CHK(eq_epsf(v4f_y(v8f_efgh(k)), 1.f/6.f, 1.e-3f));
  CHK(eq_epsf(v4f_z(v8f_efgh(k)), 1.f/7.f, 1.e-3f));
  CHK(eq_epsf(v4f_w(v8f_efgh(k)), 1.f/8.f, 1.e-3f));

  k = v8f_rsqrt(i);
  CHK(eq_epsf(v4f_x(v8f_abcd(k)), 1.f/1.f, 1.e-6f));
  CHK(eq_epsf(v4f_y(v8f_abcd(k)), 1.f/2.f, 1.e-6f));
  CHK(eq_epsf(v4f_z(v8f_abcd(k)), 1.f/3.f, 1.e-6f));
  CHK(eq_epsf(v4f_w(v8f_abcd(k)), 1.f/4.f, 1.e-6f));
  CHK(eq_epsf(v4f_x(v8f_efgh(k)), 1.f/5.f, 1.e-6f));
  CHK(eq_epsf(v4f_y(v8f_efgh(k)), 1.f/6.f, 1.e-6f));
  CHK(eq_epsf(v4f_z(v8f_efgh(k)), 1.f/7.f, 1.e-6f));
  CHK(eq_epsf(v4f_w(v8f_efgh(k)), 1.f/8.f, 1.e-6f));

  i = v8f_set(1.f,2.f,3.f,4.f,5.f,6.f,7.f,8.f);
  k = v8f_rcpe(i);
  CHK(eq_epsf(v4f_x(v8f_abcd(k)), 1.f/1.f, 1.e-3f));
  CHK(eq_epsf(v4f_y(v8f_abcd(k)), 1.f/2.f, 1.e-3f));
  CHK(eq_epsf(v4f_z(v8f_abcd(k)), 1.f/3.f, 1.e-3f));
  CHK(eq_epsf(v4f_w(v8f_abcd(k)), 1.f/4.f, 1.e-3f));
  CHK(eq_epsf(v4f_x(v8f_efgh(k)), 1.f/5.f, 1.e-3f));
  CHK(eq_epsf(v4f_y(v8f_efgh(k)), 1.f/6.f, 1.e-3f));
  CHK(eq_epsf(v4f_z(v8f_efgh(k)), 1.f/7.f, 1.e-3f));
  CHK(eq_epsf(v4f_w(v8f_efgh(k)), 1.f/8.f, 1.e-3f));

  k = v8f_rcp(i);
  CHK(eq_epsf(v4f_x(v8f_abcd(k)), 1.f/1.f, 1.e-6f));
  CHK(eq_epsf(v4f_y(v8f_abcd(k)), 1.f/2.f, 1.e-6f));
  CHK(eq_epsf(v4f_z(v8f_abcd(k)), 1.f/3.f, 1.e-6f));
  CHK(eq_epsf(v4f_w(v8f_abcd(k)), 1.f/4.f, 1.e-6f));
  CHK(eq_epsf(v4f_x(v8f_efgh(k)), 1.f/5.f, 1.e-6f));
  CHK(eq_epsf(v4f_y(v8f_efgh(k)), 1.f/6.f, 1.e-6f));
  CHK(eq_epsf(v4f_z(v8f_efgh(k)), 1.f/7.f, 1.e-6f));
  CHK(eq_epsf(v4f_w(v8f_efgh(k)), 1.f/8.f, 1.e-6f));

  j = v8f_set(2.f,3.f,4.f,5.f,6.f,7.f,8.f,9.f);
  k = v8f_lerp(i, j, v8f_set1(0.5f));
  CHK(v4f_x(v8f_abcd(k)) == 1.5f);
  CHK(v4f_y(v8f_abcd(k)) == 2.5f);
  CHK(v4f_z(v8f_abcd(k)) == 3.5f);
  CHK(v4f_w(v8f_abcd(k)) == 4.5f);
  CHK(v4f_x(v8f_efgh(k)) == 5.5f);
  CHK(v4f_y(v8f_efgh(k)) == 6.5f);
  CHK(v4f_z(v8f_efgh(k)) == 7.5f);
  CHK(v4f_w(v8f_efgh(k)) == 8.5f);

  i = v8f_set(0.f, 1.f,2.f,3.f, 4.f,5.f,6.f,7.f);
  j = v8f_set(0.f,-1.f,4.f,4.f,-2.f,6.f,6.f,8.f);

  k = v8f_eq(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] ==~0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == 0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == 0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == 0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == 0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == 0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] ==~0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == 0);

  k = v8f_neq(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == 0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] ==~0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] ==~0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] ==~0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] ==~0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] ==~0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == 0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] ==~0);

  k = v8f_ge(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] ==~0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] ==~0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == 0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == 0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] ==~0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == 0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] ==~0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == 0);

  k = v8f_le(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] ==~0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == 0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] ==~0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] ==~0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == 0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] ==~0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] ==~0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] ==~0);

  k = v8f_gt(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == 0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] ==~0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] == 0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == 0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] ==~0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == 0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == 0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] == 0);

  k = v8f_lt(i, j);
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] == 0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] == 0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] ==~0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] ==~0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == 0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] ==~0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] == 0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] ==~0);

  j = v8f_set(0.0001f, 0.99999f, 2.f, 3.1f, 4.001f, 5.0002f, 6.f, 6.999999f);
  k = v8f_eq_eps(i, j, v8f_set1(1.e-4f));
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] ==~0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] ==~0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] ==~0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] == 0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] == 0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] == 0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] ==~0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] ==~0);

  k = v8f_eq_eps(i, j, v8f_set(1.e-4f, 1.e-4f, 0.f, 0.1f, 1.e-3f, 2.e-4f, 0.f, 1.e-5f));
  cast.f[0] = v4f_x(v8f_abcd(k)); CHK(cast.i[0] ==~0);
  cast.f[1] = v4f_y(v8f_abcd(k)); CHK(cast.i[1] ==~0);
  cast.f[2] = v4f_z(v8f_abcd(k)); CHK(cast.i[2] ==~0);
  cast.f[3] = v4f_w(v8f_abcd(k)); CHK(cast.i[3] ==~0);
  cast.f[4] = v4f_x(v8f_efgh(k)); CHK(cast.i[4] ==~0);
  cast.f[5] = v4f_y(v8f_efgh(k)); CHK(cast.i[5] ==~0);
  cast.f[6] = v4f_z(v8f_efgh(k)); CHK(cast.i[6] ==~0);
  cast.f[7] = v4f_w(v8f_efgh(k)); CHK(cast.i[7] ==~0);

  i = v8f_set(0.f, 1.f,2.f,3.f, 4.f,5.f,6.f,7.f);
  j = v8f_set(0.f,-1.f,4.f,4.f,-2.f,6.f,6.f,8.f);

  k = v8f_min(i, j);
  CHK(v4f_x(v8f_abcd(k)) == 0.f);
  CHK(v4f_y(v8f_abcd(k)) ==-1.f);
  CHK(v4f_z(v8f_abcd(k)) == 2.f);
  CHK(v4f_w(v8f_abcd(k)) == 3.f);
  CHK(v4f_x(v8f_efgh(k)) ==-2.f);
  CHK(v4f_y(v8f_efgh(k)) == 5.f);
  CHK(v4f_z(v8f_efgh(k)) == 6.f);
  CHK(v4f_w(v8f_efgh(k)) == 7.f);

  k = v8f_max(i, j);
  CHK(v4f_x(v8f_abcd(k)) == 0.f);
  CHK(v4f_y(v8f_abcd(k)) == 1.f);
  CHK(v4f_z(v8f_abcd(k)) == 4.f);
  CHK(v4f_w(v8f_abcd(k)) == 4.f);
  CHK(v4f_x(v8f_efgh(k)) == 4.f);
  CHK(v4f_y(v8f_efgh(k)) == 6.f);
  CHK(v4f_z(v8f_efgh(k)) == 6.f);
  CHK(v4f_w(v8f_efgh(k)) == 8.f);

  CHK(v8f_reduce_min(i) == 0.f);
  CHK(v8f_reduce_min(j) ==-2.f);
  CHK(v8f_reduce_max(i) == 7.f);
  CHK(v8f_reduce_max(j) == 8.f);

  k = v8f_clamp(i,
    v8f_set(1.f, 1.f, 3.1f, 5.f, 4.f, 0.f, 0.f, -1.f),
    v8f_set(1.f, 1.f, 4.f, 6.f, 4.f, 1.f, 6.f, 5.f));

  CHK(v4f_x(v8f_abcd(k)) == 1.f);
  CHK(v4f_y(v8f_abcd(k)) == 1.f);
  CHK(v4f_z(v8f_abcd(k)) == 3.1f);
  CHK(v4f_w(v8f_abcd(k)) == 5.f);
  CHK(v4f_x(v8f_efgh(k)) == 4.f);
  CHK(v4f_y(v8f_efgh(k)) == 1.f);
  CHK(v4f_z(v8f_efgh(k)) == 6.f);
  CHK(v4f_w(v8f_efgh(k)) == 5.f);

  return 0;
}

