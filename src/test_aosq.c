/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "aosq.h"
#include "aosf33.h"
#include <rsys/float33.h>

#define AOSF33_EQ_EPS(M, A, B, C, D, E, F, G, H, I, Eps)                       \
  {                                                                            \
    float a[9], b[9];                                                          \
    b[0] = (A); b[1] = (B); b[2] = (C);                                        \
    b[3] = (D); b[4] = (E); b[5] = (F);                                        \
    b[6] = (G); b[7] = (H); b[8] = (I);                                        \
    CHK(f33_eq_eps(aosf33_store(a, (M)), b, Eps) == 1);                        \
  } (void)0

int
main(int argc, char** argv)
{
  union { int32_t i; float f; } cast;
  v4f_T q0, q1, q2, t;
  v4f_T m[3];
  (void)argc, (void)argv;

  q0 = aosq_identity();
  CHK(v4f_x(q0) == 0.f);
  CHK(v4f_y(q0) == 0.f);
  CHK(v4f_z(q0) == 0.f);
  CHK(v4f_w(q0) == 1.f);

  q0 = aosq_set_axis_angle(v4f_set(2.f, 5.f, 1.f, 0.f), v4f_set1((float)PI*0.3f));
  CHK(eq_eps(v4f_x(q0), 0.907981f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(q0), 2.269953f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(q0), 0.453991f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(q0), 0.891007f, 1.e-6f) == 1);

  q0 = v4f_set(1.f, 2.f, 3.f, -3.f);
  q1 = v4f_set(1.f, 2.f, 3.f, -3.f);
  t = aosq_eq(q0, q1);
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0xFFFFFFFF);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0xFFFFFFFF);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0xFFFFFFFF);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0xFFFFFFFF);

  q1 = v4f_set(0.f, 2.f, 3.f, -3.f);
  t = aosq_eq(q0, q1);
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0x00000000);

  q1 = v4f_set(1.f, 0.f, 3.f, -3.f);
  t = aosq_eq(q0, q1);
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0x00000000);

  q1 = v4f_set(1.f, 2.f, 0.f, -3.f);
  t = aosq_eq(q0, q1);
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0x00000000);

  q1 = v4f_set(1.f, 2.f, 3.f, 0.f);
  t = aosq_eq(q0, q1);
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0x00000000);

  q1 = v4f_set(1.01f, 2.f, 3.02f, -3.f);
  t = aosq_eq_eps(q0, q1, v4f_set1(0.01f));
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0x00000000);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0x00000000);
  t = aosq_eq_eps(q0, q1, v4f_set1(0.02f));
  cast.f = v4f_x(t); CHK(cast.i == (int32_t)0xFFFFFFFF);
  cast.f = v4f_y(t); CHK(cast.i == (int32_t)0xFFFFFFFF);
  cast.f = v4f_z(t); CHK(cast.i == (int32_t)0xFFFFFFFF);
  cast.f = v4f_w(t); CHK(cast.i == (int32_t)0xFFFFFFFF);

  q0 = v4f_set(1.f, 2.f, 3.f, 4.f);
  q1 = v4f_set(5.f, 6.f, 7.f, 8.f);
  q2 = aosq_mul(q0, q1);
  CHK(v4f_x(q2) == 24.f);
  CHK(v4f_y(q2) == 48.f);
  CHK(v4f_z(q2) == 48.f);
  CHK(v4f_w(q2) == -6.f);

  q2 = aosq_conj(q0);
  CHK(v4f_x(q2) == -1.f);
  CHK(v4f_y(q2) == -2.f);
  CHK(v4f_z(q2) == -3.f);
  CHK(v4f_w(q2) == 4.f);

  q0 = v4f_normalize(v4f_set(1.f, 2.f, 5.f, 0.5f));
  q1 = v4f_xyzz(q0);
  q1 = v4f_xyzd(q1, aosq_calca(q1));
  CHK(v4f_x(q0) == v4f_x(q1));
  CHK(v4f_y(q0) == v4f_y(q1));
  CHK(v4f_z(q0) == v4f_z(q1));
  CHK(eq_eps(v4f_w(q0), v4f_w(q1), 1.e-6f) == 1);

  q0 = v4f_set(1.f, 2.f, 3.f, 5.f);
  q1 = v4f_set(2.f, 6.f, 7.f, 6.f);
  q2 = aosq_slerp(q0, q1, v4f_set1(0.3f));
  CHK(eq_eps(v4f_x(q2), 1.3f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(q2), 3.2f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(q2), 4.2f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(q2), 5.3f, 1.e-6f) == 1);

  q0 = v4f_set(2.f, 5.f, 17.f, 9.f);
  aosq_to_aosf33(q0, m);
  AOSF33_EQ_EPS(m,
    -627.f, 326.f, -22.f,
    -286.f, -585.f, 206.f,
    158.f, 134.f, -57.f,
    1.e-6f);

  q0 = v4f_normalize(q0);
  aosq_to_aosf33(q0, m);
  AOSF33_EQ_EPS(m,
    -0.573935f, 0.817043f, -0.055138f,
    -0.716792f, -0.468672f, 0.516291f,
    0.395990f, 0.335840f, 0.854637f,
    1.e-6f);

  return 0;
}

