/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(copysign)
  (const RSIMD_vXf_T__ x,
   const RSIMD_vXf_T__ y);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(floor)
  (const RSIMD_vXf_T__ x);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(pow)
  (const RSIMD_vXf_T__ x,
   const RSIMD_vXf_T__ y);

/*******************************************************************************
 * Exponentatial functions
 ******************************************************************************/
RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(exp2)
  (const RSIMD_vXf_T__ x);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(exp)
  (const RSIMD_vXf_T__ x);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(exp10)
  (const RSIMD_vXf_T__ x);

/*******************************************************************************
 * Log functions
 ******************************************************************************/
RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(log2)
  (const RSIMD_vXf_T__ x);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(log)
  (const RSIMD_vXf_T__ x);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(log10)
  (const RSIMD_vXf_T__ x);

/*******************************************************************************
 * Trigonometric functions
 ******************************************************************************/
RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(sin)
  (const RSIMD_vXf_T__ v);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(asin)
  (const RSIMD_vXf_T__ v);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(cos)
  (const RSIMD_vXf_T__ v);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(acos)
  (const RSIMD_vXf_T__ v);

RSIMD_API void
RSIMD_vXf__(sincos)
  (const RSIMD_vXf_T__ v,
   RSIMD_vXf_T__* RESTRICT s,
   RSIMD_vXf_T__* RESTRICT c);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(tan)
  (const RSIMD_vXf_T__ v);

RSIMD_API RSIMD_vXf_T__
RSIMD_vXf__(atan)
  (const RSIMD_vXf_T__ v);
