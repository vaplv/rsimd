/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef AOSF44_H
#define AOSF44_H

#include "aosf33.h"
#include "rsimd.h"

/*
 * Functions on column major AoS float44 matrices. A 4x4 matrix is a set of 4
 * 4-wide SIMD float vectors, each representing a matrix column.
 */

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE float*
aosf44_store(float dst[16], const v4f_T m[4])
{
  ASSERT(m && dst);

  if(IS_ALIGNED(dst, 16)) {
    v4f_store(dst + 0, m[0]);
    v4f_store(dst + 4, m[1]);
    v4f_store(dst + 8, m[2]);
    v4f_store(dst + 12, m[3]);
  } else {
    ALIGN(16) float tmp[4];
    int i;
    FOR_EACH(i, 0, 4) {
      v4f_store(tmp, m[i]);
      dst[i*4 + 0] = tmp[0];
      dst[i*4 + 1] = tmp[1];
      dst[i*4 + 2] = tmp[2];
      dst[i*4 + 3] = tmp[3];
    }
  }
  return dst;
}

static FINLINE v4f_T*
aosf44_load(v4f_T m[4], const float src[16])
{
  ASSERT(m && src);
  if(IS_ALIGNED(src, 16)) {
    m[0] = v4f_load(src + 0);
    m[1] = v4f_load(src + 4);
    m[2] = v4f_load(src + 8);
    m[3] = v4f_load(src + 12);
  } else {
    int i;
    FOR_EACH(i, 0, 4)
      m[i] = v4f_set(src[i*3+0], src[i*3+1], src[i*4+2], src[i*4+3]);
  }
  return m;
}

static FINLINE v4f_T*
aosf44_set
  (v4f_T m[4], const v4f_T c0, const v4f_T c1, const v4f_T c2, const v4f_T c3)
{
  ASSERT(m);
  m[0] = c0, m[1] = c1, m[2] = c2, m[3] = c3;
  return m;
}

static FINLINE v4f_T*
aosf44_identity(v4f_T m[4])
{
  ASSERT(m);
  m[0] = v4f_set(1.f, 0.f, 0.f, 0.f);
  m[1] = v4f_set(0.f, 1.f, 0.f, 0.f);
  m[2] = v4f_set(0.f, 0.f, 1.f, 0.f);
  m[3] = v4f_set(0.f, 0.f, 0.f, 1.f);
  return m;
}

static FINLINE v4f_T*
aosf44_zero(v4f_T m[4])
{
  ASSERT(m);
  m[0] = v4f_zero();
  m[1] = v4f_zero();
  m[2] = v4f_zero();
  m[3] = v4f_zero();
  return m;
}

static FINLINE v4f_T*
aosf44_set_row0(v4f_T m[4], const v4f_T v)
{
  const v4f_T xyzw = v;
  const v4f_T yyww = v4f_yyww(v);
  const v4f_T zwzw = v4f_zwzw(v);
  const v4f_T wwww = v4f_yyww(zwzw);
  ASSERT(m);
  m[0] = v4f_ayzw(m[0], xyzw);
  m[1] = v4f_ayzw(m[1], yyww);
  m[2] = v4f_ayzw(m[2], zwzw);
  m[3] = v4f_ayzw(m[3], wwww);
  return m;
}

static FINLINE v4f_T*
aosf44_set_row1(v4f_T m[4], const v4f_T v)
{
  ASSERT(m);
  m[0] = v4f_xbzw(m[0], v4f_xxyy(v));
  m[1] = v4f_xbzw(m[1], v);
  m[2] = v4f_xbzw(m[2], v4f_zzww(v));
  m[3] = v4f_xbzw(m[3], v4f_zwzw(v));
  return m;
}

static FINLINE v4f_T*
aosf44_set_row2(v4f_T m[4], const v4f_T v)
{
  ASSERT(m);
  m[0] = v4f_xycw(m[0], v4f_xyxy(v));
  m[1] = v4f_xycw(m[1], v4f_xxyy(v));
  m[2] = v4f_xycw(m[2], v);
  m[3] = v4f_xycw(m[3], v4f_zzww(v));
  return m;
}

static FINLINE v4f_T*
aosf44_set_row3(v4f_T m[4], const v4f_T v)
{
  ASSERT(m);
  m[0] = v4f_xyzd(m[0], v4f_xxxx(v));
  m[1] = v4f_xyzd(m[1], v4f_xxyy(v));
  m[2] = v4f_xyzd(m[2], v4f_xxzz(v));
  m[3] = v4f_xyzd(m[3], v);
  return m;
}

static FINLINE v4f_T*
aosf44_set_row(v4f_T m[4], const v4f_T v, const int id)
{
  const v4f_T mask = v4f_mask(-(id==0), -(id==1), -(id==2), -(id==3));
  ASSERT(m && id >= 0 && id <= 3);
  m[0] = v4f_sel(m[0], v4f_xxxx(v), mask);
  m[1] = v4f_sel(m[1], v4f_yyyy(v), mask);
  m[2] = v4f_sel(m[2], v4f_zzzz(v), mask);
  m[3] = v4f_sel(m[3], v4f_wwww(v), mask);
  return m;
}

static FINLINE v4f_T*
aosf44_set_col(v4f_T m[4], const v4f_T v, const int id)
{
  ASSERT(m && id >= 0 && id <= 3);
  m[id] = v;
  return m;
}

/*******************************************************************************
 * Get operations
 ******************************************************************************/
static FINLINE v4f_T
aosf44_row0(const v4f_T m[4])
{
  ASSERT(m);
  return v4f_048C
    (v4f_xxxx(m[0]), v4f_xxxx(m[1]), v4f_xxxx(m[2]), v4f_xxxx(m[3]));
}

static FINLINE v4f_T
aosf44_row1(const v4f_T m[4])
{
  ASSERT(m);
  return v4f_048C
    (v4f_yyyy(m[0]), v4f_yyyy(m[1]), v4f_yyyy(m[2]), v4f_yyyy(m[3]));
}

static FINLINE v4f_T
aosf44_row2(const v4f_T m[4])
{
  ASSERT(m);
  return v4f_048C
    (v4f_zzzz(m[0]), v4f_zzzz(m[1]), v4f_zzzz(m[2]), v4f_zzzz(m[3]));
}

static FINLINE v4f_T
aosf44_row3(const v4f_T m[4])
{
  ASSERT(m);
  return v4f_048C
    (v4f_wwww(m[0]), v4f_wwww(m[1]), v4f_wwww(m[2]), v4f_wwww(m[3]));
}

static FINLINE v4f_T
aosf44_row(const v4f_T m[4], const int id)
{
  ASSERT(m && id >= 0 && id <= 3);
  if(id == 0) {
    return aosf44_row0(m);
  } else if(id == 1) {
    return aosf44_row1(m);
  } else if(id == 2) {
    return aosf44_row2(m);
  } else {
    return aosf44_row3(m);
  }
}

static FINLINE v4f_T
aosf44_col(const v4f_T m[4], const int id)
{
  ASSERT(m && id >= 0 && id <= 3);
  return m[id];
}

/*******************************************************************************
 * Arithmetic operations
 ******************************************************************************/
static FINLINE v4f_T*
aosf44_add(v4f_T res[4], const v4f_T m0[4], const v4f_T m1[4])
{
  ASSERT(res && m0 && m1);
  res[0] = v4f_add(m0[0], m1[0]);
  res[1] = v4f_add(m0[1], m1[1]);
  res[2] = v4f_add(m0[2], m1[2]);
  res[3] = v4f_add(m0[3], m1[3]);
  return res;
}

static FINLINE v4f_T*
aosf44_sub(v4f_T res[4], const v4f_T m0[4], const v4f_T m1[4])
{
  ASSERT(res && m0 && m1);
  res[0] = v4f_sub(m0[0], m1[0]);
  res[1] = v4f_sub(m0[1], m1[1]);
  res[2] = v4f_sub(m0[2], m1[2]);
  res[3] = v4f_sub(m0[3], m1[3]);
  return res;
}

static FINLINE v4f_T*
aosf44_minus(v4f_T res[4], const v4f_T m[4])
{
  ASSERT(res && m);
  res[0] = v4f_minus(m[0]);
  res[1] = v4f_minus(m[1]);
  res[2] = v4f_minus(m[2]);
  res[3] = v4f_minus(m[3]);
  return res;
}

static FINLINE v4f_T*
aosf44_abs(v4f_T res[4], const v4f_T m[4])
{
  ASSERT(res && m);
  res[0] = v4f_abs(m[0]);
  res[1] = v4f_abs(m[1]);
  res[2] = v4f_abs(m[2]);
  res[3] = v4f_abs(m[3]);
  return res;
}

static FINLINE v4f_T*
aosf44_mul(v4f_T res[4], const v4f_T m[4], const v4f_T v)
{
  ASSERT(res && m);
  res[0] = v4f_mul(m[0], v);
  res[1] = v4f_mul(m[1], v);
  res[2] = v4f_mul(m[2], v);
  res[3] = v4f_mul(m[3], v);
  return res;
}

static FINLINE v4f_T
aosf44_mulf4(const v4f_T m[4], const v4f_T v)
{
  v4f_T r0, r1, r2;
  ASSERT(m);
  r0 = v4f_mul(m[0], v4f_xxxx(v));
  r1 = v4f_madd(m[1], v4f_yyyy(v), r0);
  r2 = v4f_madd(m[2], v4f_zzzz(v), r1);
  return v4f_madd(m[3], v4f_wwww(v), r2);
}

static FINLINE v4f_T
aosf4_mulf44(v4f_T v, const v4f_T m[4])
{
  v4f_T xxxx, yyyy, zzzz, wwww, xyxy, zwzw;
  ASSERT(m);
  xxxx = v4f_dot(v, m[0]);
  yyyy = v4f_dot(v, m[1]);
  zzzz = v4f_dot(v, m[2]);
  wwww = v4f_dot(v, m[3]);
  xyxy = v4f_xayb(xxxx, yyyy);
  zwzw = v4f_xayb(zzzz, wwww);
  return v4f_xyab(xyxy, zwzw);
}

static FINLINE v4f_T*
aosf44_mulf44
  (v4f_T res[4], const v4f_T m0[4], const v4f_T m1[4])
{
  v4f_T c0, c1, c2, c3;
  ASSERT(res && m0 && m1);
  c0 = aosf44_mulf4(m0, m1[0]);
  c1 = aosf44_mulf4(m0, m1[1]);
  c2 = aosf44_mulf4(m0, m1[2]);
  c3 = aosf44_mulf4(m0, m1[3]);
  res[0] = c0;
  res[1] = c1;
  res[2] = c2;
  res[3] = c3;
  return res;
}

static FINLINE v4f_T*
aosf44_transpose(v4f_T res[4], const v4f_T m[4])
{
  v4f_T in_c0, in_c1, in_c2, in_c3;
  v4f_T x0x2y0y2, x1x3y1y3, z0z2w0w2, z1z3w1w3;
  ASSERT(res && m);
  in_c0 = m[0];
  in_c1 = m[1];
  in_c2 = m[2];
  in_c3 = m[3];
  x0x2y0y2 = v4f_xayb(in_c0, in_c2);
  x1x3y1y3 = v4f_xayb(in_c1, in_c3);
  z0z2w0w2 = v4f_zcwd(in_c0, in_c2);
  z1z3w1w3 = v4f_zcwd(in_c1, in_c3);
  res[0] = v4f_xayb(x0x2y0y2, x1x3y1y3);
  res[1] = v4f_zcwd(x0x2y0y2, x1x3y1y3);
  res[2] = v4f_xayb(z0z2w0w2, z1z3w1w3);
  res[3] = v4f_zcwd(z0z2w0w2, z1z3w1w3);
  return res;
}

static FINLINE v4f_T
aosf44_det(const v4f_T m[4])
{
  v4f_T xxxx, yyyy, zzzz, wwww, xyxy, zwzw, xyzw;
  v4f_T f33_012_012[3], f33_012_013[3], f33_012_023[3], f33_012_123[3];
  ASSERT(m);
  aosf33_set(f33_012_012, m[0], m[1], m[2]);
  aosf33_set(f33_012_013, m[0], m[1], m[3]);
  aosf33_set(f33_012_023, m[0], m[2], m[3]);
  aosf33_set(f33_012_123, m[1], m[2], m[3]);
  xxxx = v4f_minus(aosf33_det(f33_012_123));
  yyyy = aosf33_det(f33_012_023);
  zzzz = v4f_minus(aosf33_det(f33_012_013));
  wwww = aosf33_det(f33_012_012);
  xyxy = v4f_xayb(xxxx, yyyy);
  zwzw = v4f_xayb(zzzz, wwww);
  xyzw = v4f_xyab(xyxy, zwzw);
  return v4f_dot(xyzw, aosf44_row3(m));
}

RSIMD_API v4f_T /* Return the determinant */
aosf44_inverse(v4f_T out[4], const v4f_T in[4]);

static FINLINE v4f_T /* Return the determinant */
aosf44_invtrans(v4f_T out[4], const v4f_T a[4])
{
  v4f_T det;
  ASSERT(out && a);
  det = aosf44_inverse(out, a);
  aosf44_transpose(out, out);
  return det;
}

static FINLINE v4f_T
aosf44_eq(const v4f_T a[4], const v4f_T b[4])
{
  ASSERT(a && b);
  if(a == b) {
    return v4f_true();
  } else {
    const v4f_T eq_c0 = v4f_eq(a[0], b[0]);
    const v4f_T eq_c1 = v4f_eq(a[1], b[1]);
    const v4f_T eq_c2 = v4f_eq(a[2], b[2]);
    const v4f_T eq_c3 = v4f_eq(a[3], b[3]);
    const v4f_T eq = v4f_and(v4f_and(eq_c0, eq_c1), v4f_and(eq_c2, eq_c3));
    const v4f_T tmp = v4f_and(v4f_xzxz(eq), v4f_ywyw(eq));
    const v4f_T ret = v4f_and(tmp, v4f_yxwz(tmp));
    return ret;
  }
}

#endif /* AOSF44_H */

