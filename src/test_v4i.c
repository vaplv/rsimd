/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"

int
main(int argc, char** argv)
{
  v4i_T i, j, k;
  ALIGN(16) int32_t tmp[4] = { 0, 1, 2, 3 };
  (void)argc, (void)argv;

  i = v4i_load(tmp);
  CHK(v4i_x(i) == 0);
  CHK(v4i_y(i) == 1);
  CHK(v4i_z(i) == 2);
  CHK(v4i_w(i) == 3);

  tmp[0] = tmp[1] = tmp[2] = tmp[3] = 0;
  CHK(v4i_store(tmp, i) == tmp);
  CHK(tmp[0] == 0);
  CHK(tmp[1] == 1);
  CHK(tmp[2] == 2);
  CHK(tmp[3] == 3);

  i = v4i_set(1, 2, 3, 4);
  CHK(v4i_x(i) == 1);
  CHK(v4i_y(i) == 2);
  CHK(v4i_z(i) == 3);
  CHK(v4i_w(i) == 4);

  i = v4i_set1(-1);
  CHK(v4i_x(i) == -1);
  CHK(v4i_y(i) == -1);
  CHK(v4i_z(i) == -1);
  CHK(v4i_w(i) == -1);

  i = v4i_zero();
  CHK(v4i_x(i) == 0);
  CHK(v4i_y(i) == 0);
  CHK(v4i_z(i) == 0);
  CHK(v4i_w(i) == 0);

  i = v4i_set(1, 2, 3, 4);
  j = v4i_set(5, 6, 7, 8);
  k = v4i_xayb(i, j);
  CHK(v4i_x(k) == 1);
  CHK(v4i_y(k) == 5);
  CHK(v4i_z(k) == 2);
  CHK(v4i_w(k) == 6);

  k = v4i_zcwd(i, j);
  CHK(v4i_x(k) == 3);
  CHK(v4i_y(k) == 7);
  CHK(v4i_z(k) == 4);
  CHK(v4i_w(k) == 8);

  i = v4i_set(0x00010203, 0x04050607, 0x08090A0B, 0x0C0D0E0F);
  j = v4i_set(0x01020401, 0x70605040, 0x0F1F2F3F, 0x00000000);
  k = v4i_or(i, j);
  CHK(v4i_x(k) == (int32_t)0x01030603);
  CHK(v4i_y(k) == (int32_t)0x74655647);
  CHK(v4i_z(k) == (int32_t)0x0F1F2F3F);
  CHK(v4i_w(k) == (int32_t)0x0C0D0E0F);

  k = v4i_and(i, j);
  CHK(v4i_x(k) == (int32_t)0x00000001);
  CHK(v4i_y(k) == (int32_t)0x00000000);
  CHK(v4i_z(k) == (int32_t)0x08090A0B);
  CHK(v4i_w(k) == (int32_t)0x00000000);

  k = v4i_andnot(i, j);
  CHK(v4i_x(k) == (int32_t)0x01020400);
  CHK(v4i_y(k) == (int32_t)0x70605040);
  CHK(v4i_z(k) == (int32_t)0x07162534);
  CHK(v4i_w(k) == (int32_t)0x00000000);

  k = v4i_xor(i, j);
  CHK(v4i_x(k) == (int32_t)0x01030602);
  CHK(v4i_y(k) == (int32_t)0x74655647);
  CHK(v4i_z(k) == (int32_t)0x07162534);
  CHK(v4i_w(k) == (int32_t)0x0C0D0E0F);

  k = v4i_not(i);
  CHK(v4i_x(k) == (int32_t)0xFFFEFDFC);
  CHK(v4i_y(k) == (int32_t)0xFBFAF9F8);
  CHK(v4i_z(k) == (int32_t)0xF7F6F5F4);
  CHK(v4i_w(k) == (int32_t)0xF3F2F1F0);

  i = v4i_set(32, 16, 8, 4);
  k = v4i_rshift(i, 4);
  CHK(v4i_x(k) == 2);
  CHK(v4i_y(k) == 1);
  CHK(v4i_z(k) == 0);
  CHK(v4i_w(k) == 0);

  k = v4i_rshift(i, 1);
  CHK(v4i_x(k) == 16);
  CHK(v4i_y(k) == 8);
  CHK(v4i_z(k) == 4);
  CHK(v4i_w(k) == 2);

  k = v4i_lshift(i, 4);
  CHK(v4i_x(k) == 512);
  CHK(v4i_y(k) == 256);
  CHK(v4i_z(k) == 128);
  CHK(v4i_w(k) == 64);

  i = v4i_set(1, 2, 3, 4);
  j = v4i_set(-2, -4, 3, 6);
  k = v4i_add(i, j);
  CHK(v4i_x(k) == -1);
  CHK(v4i_y(k) == -2);
  CHK(v4i_z(k) == 6);
  CHK(v4i_w(k) == 10);

  k = v4i_sub(i, j);
  CHK(v4i_x(k) == 3);
  CHK(v4i_y(k) == 6);
  CHK(v4i_z(k) == 0);
  CHK(v4i_w(k) == -2);

  k = v4i_minus(j);
  CHK(v4i_x(k) == -v4i_x(j));
  CHK(v4i_y(k) == -v4i_y(j));
  CHK(v4i_z(k) == -v4i_z(j));
  CHK(v4i_w(k) == -v4i_w(j));

  k = v4i_eq(i, j);
  CHK(v4i_x(k) == (int32_t)0x00000000);
  CHK(v4i_y(k) == (int32_t)0x00000000);
  CHK(v4i_z(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_w(k) == (int32_t)0x00000000);

  k = v4i_neq(i, j);
  CHK(v4i_x(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_y(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_z(k) == (int32_t)0x00000000);
  CHK(v4i_w(k) == (int32_t)0xFFFFFFFF);

  k = v4i_gt(i, j);
  CHK(v4i_x(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_y(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_z(k) == (int32_t)0x00000000);
  CHK(v4i_w(k) == (int32_t)0x00000000);

  k = v4i_lt(i, j);
  CHK(v4i_x(k) == (int32_t)0x00000000);
  CHK(v4i_y(k) == (int32_t)0x00000000);
  CHK(v4i_z(k) == (int32_t)0x00000000);
  CHK(v4i_w(k) == (int32_t)0xFFFFFFFF);

  k = v4i_ge(i, j);
  CHK(v4i_x(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_y(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_z(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_w(k) == (int32_t)0x00000000);

  k = v4i_le(i, j);
  CHK(v4i_x(k) == (int32_t)0x00000000);
  CHK(v4i_y(k) == (int32_t)0x00000000);
  CHK(v4i_z(k) == (int32_t)0xFFFFFFFF);
  CHK(v4i_w(k) == (int32_t)0xFFFFFFFF);

  k = v4i_sel(i, j, v4i_set(~0, 0, ~0, 0));
  CHK(v4i_x(k) == -2);
  CHK(v4i_y(k) == 2);
  CHK(v4i_z(k) == 3);
  CHK(v4i_w(k) == 4);

  k = v4i_xxxx(i);
  CHK(v4i_x(k) == 1);
  CHK(v4i_y(k) == 1);
  CHK(v4i_z(k) == 1);
  CHK(v4i_w(k) == 1);

  k = v4i_wwxy(i);
  CHK(v4i_x(k) == 4);
  CHK(v4i_y(k) == 4);
  CHK(v4i_z(k) == 1);
  CHK(v4i_w(k) == 2);

  k = v4i_xyxy(i);
  CHK(v4i_x(k) == 1);
  CHK(v4i_y(k) == 2);
  CHK(v4i_z(k) == 1);
  CHK(v4i_w(k) == 2);

  k = v4i_wyyz(i);
  CHK(v4i_x(k) == 4);
  CHK(v4i_y(k) == 2);
  CHK(v4i_z(k) == 2);
  CHK(v4i_w(k) == 3);

  i = v4i_set(1, 2, 3, 4);
  j = v4i_set(-2, -4, 3, 6);
  k = v4i_min(i, j);
  CHK(v4i_x(k) == -2);
  CHK(v4i_y(k) == -4);
  CHK(v4i_z(k) == 3);
  CHK(v4i_w(k) == 4);

  k = v4i_max(i, j);
  CHK(v4i_x(k) == 1);
  CHK(v4i_y(k) == 2);
  CHK(v4i_z(k) == 3);
  CHK(v4i_w(k) == 6);

  k = v4i_reduce_min(i);
  CHK(v4i_x(k) == 1);
  CHK(v4i_y(k) == 1);
  CHK(v4i_z(k) == 1);
  CHK(v4i_w(k) == 1);
  CHK(v4i_reduce_min_i32(i) == 1);

  k = v4i_reduce_min(j);
  CHK(v4i_x(k) == -4);
  CHK(v4i_y(k) == -4);
  CHK(v4i_z(k) == -4);
  CHK(v4i_w(k) == -4);
  CHK(v4i_reduce_min_i32(j) == -4);

  k = v4i_reduce_max(i);
  CHK(v4i_x(k) == 4);
  CHK(v4i_y(k) == 4);
  CHK(v4i_z(k) == 4);
  CHK(v4i_w(k) == 4);
  CHK(v4i_reduce_max_i32(i) == 4);

  k = v4i_reduce_max(j);
  CHK(v4i_x(k) == 6);
  CHK(v4i_y(k) == 6);
  CHK(v4i_z(k) == 6);
  CHK(v4i_w(k) == 6);
  CHK(v4i_reduce_max_i32(j) == 6);

  return 0;
}

