/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef AOSF33_H
#define AOSF33_H

#include "rsimd.h"
#include <math.h>

/*
 * Functions on column major AoS float33 matrices. A 3x3 matrix is a set of 3
 * 4-wide SIMD float vectors, each representing a matrix column. Actually the
 * fourth component of each vector is ignored and its value is thus undefined.
 */

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE float*
aosf33_store(float res[9]/* Column major */, const v4f_T m[3])
{
  ALIGN(16) float tmp[4];
  int i;
  ASSERT(res && m);
  FOR_EACH(i, 0, 3) {
    v4f_store(tmp, m[i]);
    res[i*3 + 0] = tmp[0];
    res[i*3 + 1] = tmp[1];
    res[i*3 + 2] = tmp[2];
  }
  return res;
}

static FINLINE v4f_T*
aosf33_load(v4f_T res[3], const float m[9]/* Column major */)
{
  int i;
  ASSERT(res && m);
  FOR_EACH(i, 0, 3)
    res[i] = v4f_set(m[i*3+0], m[i*3+1], m[i*3+2], 0.f);
  return res;
}

static FINLINE v4f_T*
aosf33_set(v4f_T m[3], const v4f_T c0, const v4f_T c1, const v4f_T c2)
{
  ASSERT(m);
  m[0] = c0;
  m[1] = c1;
  m[2] = c2;
  return m;
}

static FINLINE v4f_T*
aosf33_identity(v4f_T m[3])
{
  ASSERT(m);
  m[0] = v4f_set(1.f, 0.f, 0.f, 0.f);
  m[1] = v4f_set(0.f, 1.f, 0.f, 0.f);
  m[2] = v4f_set(0.f, 0.f, 1.f, 0.f);
  return m;
}

static FINLINE v4f_T*
aosf33_zero(v4f_T m[3])
{
  ASSERT(m);
  m[0] = v4f_zero();
  m[1] = v4f_zero();
  m[2] = v4f_zero();
  return m;
}

static FINLINE v4f_T*
aosf33_set_row0(v4f_T m[3], const v4f_T v)
{
  ASSERT(m);
  m[0] = v4f_ayzw(m[0], v);
  m[1] = v4f_ayzw(m[1], v4f_yyww(v));
  m[2] = v4f_ayzw(m[2], v4f_zwzw(v));
  return m;
}

static FINLINE v4f_T*
aosf33_set_row1(v4f_T m[3], const v4f_T v)
{
  ASSERT(m);
  m[0] = v4f_xbzw(m[0], v4f_xxyy(v));
  m[1] = v4f_xbzw(m[1], v);
  m[2] = v4f_xbzw(m[2], v4f_zzww(v));
  return m;
}

static FINLINE v4f_T*
aosf33_set_row2(v4f_T m[3], const v4f_T v)
{
  ASSERT(m);
  m[0] = v4f_xyab(m[0], v4f_xyxy(v));
  m[1] = v4f_xyab(m[1], v4f_yyzz(v));
  m[2] = v4f_xyab(m[2], v4f_zzww(v));
  return m;
}

static FINLINE v4f_T*
aosf33_set_row(v4f_T m[3], const v4f_T v, const int id)
{
  const v4f_T mask = v4f_mask(-(id==0), -(id==1), -(id==2), 0);
  ASSERT(m && id >= 0 && id <= 2);
  m[0] = v4f_sel(m[0], v4f_xxxx(v), mask);
  m[1] = v4f_sel(m[1], v4f_yyyy(v), mask);
  m[2] = v4f_sel(m[2], v4f_zzzz(v), mask);
  return m;
}

static FINLINE v4f_T*
aosf33_set_col(v4f_T m[3], const v4f_T v, const int id)
{
  ASSERT(m && id >= 0 && id <= 2);
  m[id] = v;
  return m;
}

/*******************************************************************************
 * Arithmetic operations
 ******************************************************************************/
static FINLINE v4f_T*
aosf33_add(v4f_T res[3], const v4f_T m0[3], const v4f_T m1[3])
{
  ASSERT(res && m0 && m1);
  res[0] = v4f_add(m0[0], m1[0]);
  res[1] = v4f_add(m0[1], m1[1]);
  res[2] = v4f_add(m0[2], m1[2]);
  return res;
}

static FINLINE v4f_T*
aosf33_sub(v4f_T res[3], const v4f_T m0[3], const v4f_T m1[3])
{
  ASSERT(res && m0 && m1);
  res[0] = v4f_sub(m0[0], m1[0]);
  res[1] = v4f_sub(m0[1], m1[1]);
  res[2] = v4f_sub(m0[2], m1[2]);
  return res;
}

static FINLINE v4f_T*
aosf33_minus(v4f_T res[3], const v4f_T m[3])
{
  ASSERT(res && m);
  res[0] = v4f_minus(m[0]);
  res[1] = v4f_minus(m[1]);
  res[2] = v4f_minus(m[2]);
  return res;
}

static FINLINE v4f_T*
aosf33_abs(v4f_T res[3], const v4f_T m[3])
{
  ASSERT(res && m);
  res[0] = v4f_abs(m[0]);
  res[1] = v4f_abs(m[1]);
  res[2] = v4f_abs(m[2]);
  return res;
}

static FINLINE v4f_T*
aosf33_mul(v4f_T res[3], const v4f_T m[3], const v4f_T v)
{
  ASSERT(res && m);
  res[0] = v4f_mul(m[0], v);
  res[1] = v4f_mul(m[1], v);
  res[2] = v4f_mul(m[2], v);
  return res;
}

static FINLINE v4f_T
aosf33_mulf3(const v4f_T m[3], const v4f_T v)
{
  v4f_T r0, r1;
  ASSERT(m);
  r0 = v4f_mul(m[0], v4f_xxxx(v));
  r1 = v4f_madd(m[1], v4f_yyyy(v), r0);
  return v4f_madd(m[2], v4f_zzzz(v), r1);
}

static FINLINE v4f_T
aosf3_mulf33(const v4f_T v, const v4f_T m[3])
{
  v4f_T xxxx, yyyy, zzzz, yyzz;
  ASSERT(m);
  xxxx = v4f_dot3(v, m[0]);
  yyyy = v4f_dot3(v, m[1]);
  zzzz = v4f_dot3(v, m[2]);
  yyzz = v4f_xyab(yyyy, zzzz);
  return v4f_ayzw(yyzz, xxxx);
}

static FINLINE v4f_T*
aosf33_mulf33(v4f_T res[3], const v4f_T a[3], const v4f_T b[3])
{
  v4f_T c0, c1, c2;
  ASSERT(res && a && b);
  c0 = aosf33_mulf3(a, b[0]);
  c1 = aosf33_mulf3(a, b[1]);
  c2 = aosf33_mulf3(a, b[2]);
  res[0] = c0;
  res[1] = c1;
  res[2] = c2;
  return res;
}

static FINLINE v4f_T*
aosf33_transpose(v4f_T res[3], const v4f_T m[3])
{
  v4f_T c0, c1, c2;
  v4f_T x0x2y0y2, z0z2w0w2, z1z1y1y1;
  ASSERT(res && m);
  c0 = m[0];
  c1 = m[1];
  c2 = m[2];
  x0x2y0y2 = v4f_xayb(c0, c2);
  z0z2w0w2 = v4f_zcwd(c0, c2);
  z1z1y1y1 = v4f_zzyy(c1);
  res[0] = v4f_xayb(x0x2y0y2, c1);
  res[1] = v4f_zcwd(x0x2y0y2, z1z1y1y1);
  res[2] = v4f_xayb(z0z2w0w2, z1z1y1y1);
  return res;
}

static FINLINE v4f_T
aosf33_det(const v4f_T m[3])
{
  ASSERT(m);
  return v4f_dot3(m[2], v4f_cross3(m[0], m[1]));
}

static FINLINE v4f_T /* Return the determinant */
aosf33_invtrans(v4f_T res[3], const v4f_T m[3])
{
  v4f_T t[3], det, invdet;
  ASSERT(res && m);
  t[0] = v4f_cross3(m[1], m[2]);
  t[1] = v4f_cross3(m[2], m[0]);
  t[2] = v4f_cross3(m[0], m[1]);
  det = v4f_dot3(t[2], m[2]);
  invdet = v4f_rcp(det);
  aosf33_mul(res, t, invdet);
  return det;
}

static FINLINE v4f_T
aosf33_inverse(v4f_T res[3], const v4f_T m[3])
{
  v4f_T det;
  ASSERT(res && m);
  det = aosf33_invtrans(res, m);
  aosf33_transpose(res, res);
  return det;
}

/*******************************************************************************
 * Get operations
 ******************************************************************************/
static FINLINE v4f_T
aosf33_row0(const v4f_T m[3])
{
  ASSERT(m);
  return v4f_ayzw(v4f_xyab(v4f_xxzz(m[1]), v4f_xxzz(m[2])), m[0]);
}

static FINLINE v4f_T
aosf33_row1(const v4f_T m[3])
{
  ASSERT(m);
  return v4f_ayzw(v4f_xyab(v4f_yyww(m[1]), v4f_yyww(m[2])), v4f_yyww(m[0]));
}

static FINLINE v4f_T
aosf33_row2(const v4f_T m[3])
{
  ASSERT(m);
  return v4f_ayzw(v4f_xyab(v4f_zzww(m[1]), v4f_zzww(m[2])), v4f_zzww(m[0]));
}

static FINLINE v4f_T
aosf33_row(const v4f_T m[3], int id)
{
  v4f_T t[3];
  ASSERT(m && id >= 0 && id <= 2);
  aosf33_transpose(t, m);
  return t[id];
}

static FINLINE v4f_T
aosf33_col(const v4f_T m[3], int id)
{
  ASSERT(m && id >= 0 && id <= 2);
  return m[id];
}

/*******************************************************************************
 * Build functions
 ******************************************************************************/
static FINLINE v4f_T* /* XYZ norm */
aosf33_rotation(v4f_T res[3], float pitch, float yaw, float roll)
{
  float c1, c2, c3, s1, s2, s3;
  ASSERT(res);
  c1 = (float)cos(pitch);
  c2 = (float)cos(yaw);
  c3 = (float)cos(roll);
  s1 = (float)sin(pitch);
  s2 = (float)sin(yaw);
  s3 = (float)sin(roll);
  res[0] = v4f_set(c2*c3, c1*s3 + c3*s1*s2, s1*s3 - c1*c3*s2, 0.f);
  res[1] = v4f_set(-c2*s3, c1*c3 - s1*s2*s3, c1*s2*s3 + c3*s1, 0.f);
  res[2] = v4f_set(s2, -c2*s1, c1*c2, 0.f);
  return res;
}

static FINLINE v4f_T* /* rotation around the Y axis */
aosf33_yaw_rotation(v4f_T res[3], float yaw)
{
  float c, s;
  ASSERT(res);
  c = (float)cos(yaw);
  s = (float)sin(yaw);
  res[0] = v4f_set(c, 0.f, -s, 0.f);
  res[1] = v4f_set(0.f, 1.f, 0.f, 0.f);
  res[2] = v4f_set(s, 0.f, c, 0.f);
  return res;
}

#endif /* AOSF33_H */

