/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef AOSQ_H
#define AOSQ_H

#include "rsimd.h"
#include "math.h"

/*
 * Functions on AoS quaternion encoded into a v4f_T as { i, j, k, a }
 */

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE v4f_T
aosq_identity(void)
{
  return v4f_set(0.f, 0.f, 0.f, 1.f);
}

static FINLINE v4f_T
aosq_set_axis_angle(const v4f_T xyz_, const v4f_T aaaa)
{
  const v4f_T half_angle = v4f_mul(aaaa, v4f_set1(0.5f));
  v4f_T s, c;
  v4f_T axis1;
  v4f_T sssc;

  v4f_sincos(half_angle, &s, &c);

  axis1 = v4f_xyzd(xyz_, v4f_set1(1.f));
  sssc = v4f_xyzd(s, c);

  /* { x*sin(a/2), y*sin(a/2), z*sin(a/2), cos(a/2) } */
  return v4f_mul(axis1, sssc);
}

/*******************************************************************************
 * Comparison operations
 ******************************************************************************/
static FINLINE v4f_T
aosq_eq(const v4f_T q0, const v4f_T q1)
{
  const v4f_T r0 = v4f_eq(q0, q1);
  const v4f_T r1 = v4f_and(v4f_xxyy(r0), v4f_zzww(r0));
  return v4f_and(v4f_xxyy(r1), v4f_zzww(r1));
}

static FINLINE v4f_T
aosq_eq_eps(const v4f_T q0, const v4f_T q1, const v4f_T eps)
{
  const v4f_T r0 = v4f_eq_eps(q0, q1, eps);
  const v4f_T r1 = v4f_and(v4f_xxyy(r0), v4f_zzww(r0));
  return v4f_and(v4f_xxyy(r1), v4f_zzww(r1));
}

/*******************************************************************************
 * Arithmetic operations
 ******************************************************************************/
#define SBIT__ (int32_t)0x80000000
static FINLINE v4f_T
aosq_mul(const v4f_T q0, const v4f_T q1)
{
  const v4f_T a = v4f_mul(v4f_xor(v4f_mask(0, 0, SBIT__, 0), q0), v4f_wzyx(q1));
  const v4f_T b = v4f_mul(v4f_xor(v4f_mask(SBIT__, 0, 0, 0), q0), v4f_zwxy(q1));
  const v4f_T c = v4f_mul(v4f_xor(v4f_mask(0, SBIT__, 0, 0), q0), v4f_yxwz(q1));
  const v4f_T d = v4f_mul(v4f_xor(v4f_mask(SBIT__, SBIT__, SBIT__, 0), q0), q1);
  const v4f_T ijij = v4f_xayb(v4f_sum(a), v4f_sum(b));
  const v4f_T kaka = v4f_xayb(v4f_sum(c), v4f_sum(d));
  return v4f_xyab(ijij, kaka);
}

static FINLINE v4f_T /* { -ix, -jy, -jz, a } */
aosq_conj(const v4f_T q)
{
  return v4f_xor(q, v4f_mask(SBIT__, SBIT__, SBIT__, 0));
}
#undef SBIT__

static FINLINE v4f_T
aosq_calca(const v4f_T ijk_)
{
  const v4f_T ijk_square_len = v4f_dot3(ijk_, ijk_);
  return v4f_sqrt(v4f_abs(v4f_sub(v4f_set1(1.f), ijk_square_len)));
}

static FINLINE v4f_T
aosq_nlerp(const v4f_T from, const v4f_T to, const v4f_T aaaa)
{
  return v4f_normalize(v4f_lerp(from, to, aaaa));
}

RSIMD_API v4f_T aosq_slerp(const v4f_T from, const v4f_T to, const v4f_T aaaa);

/*******************************************************************************
 * Conversion
 ******************************************************************************/
RSIMD_API void aosq_to_aosf33(const v4f_T q, v4f_T out[3]);

#endif /* AOSQ_H */


