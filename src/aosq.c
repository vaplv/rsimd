/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "aosq.h"

v4f_T
aosq_slerp(const v4f_T from, const v4f_T to, const v4f_T vvvv)
{
  v4f_T tmp_cos_omega, cos_omega, omega, rcp_sin_omega;
  v4f_T one_sub_v;
  v4f_T mask;
  v4f_T tmp0, tmp1, tmp2;
  v4f_T scale0, scale1;
  float f;

  f = v4f_x(vvvv);
  if(f == 0.f)
    return from;
  else if(f == 1.f)
    return to;

  tmp_cos_omega = v4f_dot(from, to);

  mask = v4f_lt(tmp_cos_omega, v4f_zero());
  tmp0 = v4f_sel(to, v4f_minus(to), mask);
  cos_omega = v4f_sel(tmp_cos_omega, v4f_minus(tmp_cos_omega), mask);

  omega = v4f_acos(cos_omega);
  rcp_sin_omega = v4f_rcp(v4f_sin(omega));
  one_sub_v = v4f_sub(v4f_set1(1.f), vvvv);
  tmp1 = v4f_mul(v4f_sin(v4f_mul(one_sub_v, omega)), rcp_sin_omega);
  tmp2 = v4f_mul(v4f_sin(v4f_mul(omega, vvvv)), rcp_sin_omega);

  mask = v4f_gt(v4f_sub(v4f_set1(1.f), cos_omega), v4f_set1(1.e-6f));
  scale0 = v4f_sel(one_sub_v, tmp1, mask);
  scale1 = v4f_sel(vvvv, tmp2, mask);

  return v4f_madd(from, scale0, v4f_mul(tmp0, scale1));
}

void
aosq_to_aosf33(const v4f_T q, v4f_T out[3])
{
  const v4f_T i2j2k2_ = v4f_add(q, q);

  const v4f_T r0 = /* { jj2 + kk2, ij2 + ak2, ik2 - aj2 } */
    v4f_madd(v4f_mul(v4f_zzyy(i2j2k2_), v4f_zwwz(q)),
             v4f_set(1.f, 1.f, -1.f, 0.f),
             v4f_mul(v4f_yyzz(i2j2k2_), v4f_yxxy(q)));
  const v4f_T r1 = /* { ij2 - ak2, ii2 + kk2, jk2 + ai2 } */
    v4f_madd(v4f_mul(v4f_zzxx(i2j2k2_), v4f_wzwz(q)),
             v4f_set(-1.f, 1.f, 1.f, 0.f),
             v4f_mul(v4f_yxzw(i2j2k2_), v4f_xxyy(q)));
  const v4f_T r2 = /* { ik2 + aj2, jk2 - ai2, ii2 + jj2 } */
    v4f_madd(v4f_mul(v4f_yxyx(i2j2k2_), v4f_wwyy(q)),
             v4f_set(1.f, -1.f, 1.f, 0.f),
             v4f_mul(v4f_zzxx(i2j2k2_), v4f_xyxy(q)));

  out[0] = /* { 1 - (jj2 + kk2), ij2 + ak2, ik2 - aj2 } */
    v4f_madd(r0, v4f_set(-1.f, 1.f, 1.f, 0.f), v4f_set(1.f, 0.f, 0.f, 0.f));
  out[1] = /* { ij2 - ak2, 1 - (ii2 + kk2), jk2 + ai2 } */
    v4f_madd(r1, v4f_set(1.f, -1.f, 1.f, 0.f), v4f_set(0.f, 1.f, 0.f, 0.f));
  out[2] = /* { ik2 + aj2, jk2 - ai2, 1 - (ii2 + jj2) } */
    v4f_madd(r2, v4f_set(1.f, 1.f, -1.f, 0.f), v4f_set(0.f, 0.f, 1.f, 0.f));
}

