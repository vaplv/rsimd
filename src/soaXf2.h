/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_WIDTH__
  #error "Undefined RSIMD_WIDTH__ macro"
#endif

#define RSIMD_SOA_DIMENSION__ 2
#include "soaXfY_begin.h"
#include "soaXfY.h"

static FINLINE RSIMD_vXf_T__
RSIMD_soaXfY__(cross)(const RSIMD_vXf_T__ a[2], const RSIMD_vXf_T__ b[2])
{
  ASSERT(a && b);
  return RSIMD_vXf__(sub)
    (RSIMD_vXf__(mul)(a[0], b[1]),
     RSIMD_vXf__(mul)(a[1], b[0]));
}

#include "soaXfY_end.h"
