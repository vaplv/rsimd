/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "aosf44.h"
#include <rsys/float44.h>

#define AOSF44_EQ_EPS(Mat, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Eps)\
  {                                                                            \
    float a[16], b[16];                                                        \
    b[0] = (A); b[1] = (B); b[2] = (C); b[3] = (D);                            \
    b[4] = (E); b[5] = (F); b[6] = (G); b[7] = (H);                            \
    b[8] = (I); b[9] = (J); b[10]= (K); b[11]= (L);                            \
    b[12]= (M); b[13]= (N); b[14]= (O); b[15]= (P);                            \
    CHK(f44_eq_eps(aosf44_store(a, (Mat)), b, Eps) == 1);                      \
  } (void)0
#define AOSF44_EQ(Mat, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P)         \
  AOSF44_EQ_EPS(Mat, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, 0.f)

int
main(int argc, char** argv)
{
  v4f_T m[4], n[4], o[4], v;
  ALIGN(16) float tmp[16];
  (void)argc, (void)argv;

  CHK(aosf44_set(m,
    v4f_set(0.f, 1.f, 2.f, 3.f),
    v4f_set(4.f, 5.f, 6.f, 7.f),
    v4f_set(8.f, 9.f, 10.f, 11.f),
    v4f_set(12.f, 13.f, 14.f, 15.f)) == m);
  AOSF44_EQ(m,
    0.f, 1.f, 2.f, 3.f,
    4.f, 5.f, 6.f, 7.f,
    8.f, 9.f, 10.f, 11.f,
    12.f, 13.f, 14.f, 15.f);

  CHK(aosf44_store(tmp, m) == tmp);
  CHK(tmp[0] == 0.f);
  CHK(tmp[1] == 1.f);
  CHK(tmp[2] == 2.f);
  CHK(tmp[3] == 3.f);
  CHK(tmp[4] == 4.f);
  CHK(tmp[5] == 5.f);
  CHK(tmp[6] == 6.f);
  CHK(tmp[7] == 7.f);
  CHK(tmp[8] == 8.f);
  CHK(tmp[9] == 9.f);
  CHK(tmp[10] == 10.f);
  CHK(tmp[11] == 11.f);
  CHK(tmp[12] == 12.f);
  CHK(tmp[13] == 13.f);
  CHK(tmp[14] == 14.f);
  CHK(tmp[15] == 15.f);

  tmp[0] = 0.f; tmp[1] = 2.f; tmp[2] = 4.f; tmp[3] = 6.f;
  tmp[4] = 8.f; tmp[5] = 10.f; tmp[6] = 12.f; tmp[7] = 14.f;
  tmp[8] = 16.f; tmp[9] = 18.f; tmp[10] = 20.f; tmp[11] = 22.f;
  tmp[12] = 24.f; tmp[13] = 26.f; tmp[14] = 28.f; tmp[15] = 30.f;
  CHK(aosf44_load(m, tmp) == m);
  AOSF44_EQ(m,
    0.f, 2.f, 4.f, 6.f,
    8.f, 10.f, 12.f, 14.f,
    16.f, 18.f, 20.f, 22.f,
    24.f, 26.f, 28.f, 30.f);

  CHK(aosf44_identity(m) == m);
  AOSF44_EQ(m,
    1.f, 0.f, 0.f, 0.f,
    0.f, 1.f, 0.f, 0.f,
    0.f, 0.f, 1.f, 0.f,
    0.f, 0.f, 0.f, 1.f);

  CHK(aosf44_zero(m) == m);
  AOSF44_EQ(m,
    0.f, 0.f, 0.f, 0.f,
    0.f, 0.f, 0.f, 0.f,
    0.f, 0.f, 0.f, 0.f,
    0.f, 0.f, 0.f, 0.f);

  CHK(aosf44_set_row0(m, v4f_set(0.f, 1.f, 2.f, 3.f)) == m);
  AOSF44_EQ(m,
    0.f, 0.f, 0.f, 0.f,
    1.f, 0.f, 0.f, 0.f,
    2.f, 0.f, 0.f, 0.f,
    3.f, 0.f, 0.f, 0.f);
  CHK(aosf44_set_row1(m, v4f_set(4.f, 5.f, 6.f, 7.f)) == m);
  AOSF44_EQ(m,
    0.f, 4.f, 0.f, 0.f,
    1.f, 5.f, 0.f, 0.f,
    2.f, 6.f, 0.f, 0.f,
    3.f, 7.f, 0.f, 0.f);
  CHK(aosf44_set_row2(m, v4f_set(8.f, 9.f, 10.f, 11.f)) == m);
  AOSF44_EQ(m,
    0.f, 4.f, 8.f, 0.f,
    1.f, 5.f, 9.f, 0.f,
    2.f, 6.f, 10.f, 0.f,
    3.f, 7.f, 11.f, 0.f);
  CHK(aosf44_set_row3(m, v4f_set(12.f, 13.f, 14.f, 15.f)) == m);
  AOSF44_EQ(m,
    0.f, 4.f, 8.f, 12.f,
    1.f, 5.f, 9.f, 13.f,
    2.f, 6.f, 10.f, 14.f,
    3.f, 7.f, 11.f, 15.f);

  CHK(aosf44_zero(m) == m);
  CHK(aosf44_set_row(m, v4f_set(0.f, 1.f, 2.f, 3.f), 0) == m);
  AOSF44_EQ(m,
    0.f, 0.f, 0.f, 0.f,
    1.f, 0.f, 0.f, 0.f,
    2.f, 0.f, 0.f, 0.f,
    3.f, 0.f, 0.f, 0.f);
  CHK(aosf44_set_row(m, v4f_set(4.f, 5.f, 6.f, 7.f), 1) == m);
  AOSF44_EQ(m,
    0.f, 4.f, 0.f, 0.f,
    1.f, 5.f, 0.f, 0.f,
    2.f, 6.f, 0.f, 0.f,
    3.f, 7.f, 0.f, 0.f);
  CHK(aosf44_set_row(m, v4f_set(8.f, 9.f, 10.f, 11.f), 2) == m);
  AOSF44_EQ(m,
    0.f, 4.f, 8.f, 0.f,
    1.f, 5.f, 9.f, 0.f,
    2.f, 6.f, 10.f, 0.f,
    3.f, 7.f, 11.f, 0.f);
  CHK(aosf44_set_row(m, v4f_set(12.f, 13.f, 14.f, 15.f), 3) == m);
  AOSF44_EQ(m,
    0.f, 4.f, 8.f, 12.f,
    1.f, 5.f, 9.f, 13.f,
    2.f, 6.f, 10.f, 14.f,
    3.f, 7.f, 11.f, 15.f);

  CHK(aosf44_zero(m) == m);
  CHK(aosf44_set_col(m, v4f_set(0.f, 1.f, 2.f, 3.f), 0) == m);
  AOSF44_EQ(m,
    0.f, 1.f, 2.f, 3.f,
    0.f, 0.f, 0.f, 0.f,
    0.f, 0.f, 0.f, 0.f,
    0.f, 0.f, 0.f, 0.f);
  CHK(aosf44_set_col(m, v4f_set(4.f, 5.f, 6.f, 7.f), 1) == m);
  AOSF44_EQ(m,
    0.f, 1.f, 2.f, 3.f,
    4.f, 5.f, 6.f, 7.f,
    0.f, 0.f, 0.f, 0.f,
    0.f, 0.f, 0.f, 0.f);
  CHK(aosf44_set_col(m, v4f_set(8.f, 9.f, 10.f, 11.f), 2) == m);
  AOSF44_EQ(m,
    0.f, 1.f, 2.f, 3.f,
    4.f, 5.f, 6.f, 7.f,
    8.f, 9.f, 10.f, 11.f,
    0.f, 0.f, 0.f, 0.f);
  CHK(aosf44_set_col(m, v4f_set(12.f, 13.f, 14.f, 15.f), 3) == m);
  AOSF44_EQ(m,
    0.f, 1.f, 2.f, 3.f,
    4.f, 5.f, 6.f, 7.f,
    8.f, 9.f, 10.f, 11.f,
    12.f, 13.f, 14.f, 15.f);

  v = aosf44_row0(m);
  CHK(v4f_x(v) == 0.f);
  CHK(v4f_y(v) == 4.f);
  CHK(v4f_z(v) == 8.f);
  CHK(v4f_w(v) == 12.f);

  v = aosf44_row1(m);
  CHK(v4f_x(v) == 1.f);
  CHK(v4f_y(v) == 5.f);
  CHK(v4f_z(v) == 9.f);
  CHK(v4f_w(v) == 13.f);

  v = aosf44_row2(m);
  CHK(v4f_x(v) == 2.f);
  CHK(v4f_y(v) == 6.f);
  CHK(v4f_z(v) == 10.f);
  CHK(v4f_w(v) == 14.f);

  v = aosf44_row3(m);
  CHK(v4f_x(v) == 3.f);
  CHK(v4f_y(v) == 7.f);
  CHK(v4f_z(v) == 11.f);
  CHK(v4f_w(v) == 15.f);

  v = aosf44_row(m, 0);
  CHK(v4f_x(v) == 0.f);
  CHK(v4f_y(v) == 4.f);
  CHK(v4f_z(v) == 8.f);
  CHK(v4f_w(v) == 12.f);

  v = aosf44_row(m, 1);
  CHK(v4f_x(v) == 1.f);
  CHK(v4f_y(v) == 5.f);
  CHK(v4f_z(v) == 9.f);
  CHK(v4f_w(v) == 13.f);

  v = aosf44_row(m, 2);
  CHK(v4f_x(v) == 2.f);
  CHK(v4f_y(v) == 6.f);
  CHK(v4f_z(v) == 10.f);
  CHK(v4f_w(v) == 14.f);

  v = aosf44_row(m, 3);
  CHK(v4f_x(v) == 3.f);
  CHK(v4f_y(v) == 7.f);
  CHK(v4f_z(v) == 11.f);
  CHK(v4f_w(v) == 15.f);

  v = aosf44_col(m, 0);
  CHK(v4f_x(v) == 0.f);
  CHK(v4f_y(v) == 1.f);
  CHK(v4f_z(v) == 2.f);
  CHK(v4f_w(v) == 3.f);

  v = aosf44_col(m, 1);
  CHK(v4f_x(v) == 4.f);
  CHK(v4f_y(v) == 5.f);
  CHK(v4f_z(v) == 6.f);
  CHK(v4f_w(v) == 7.f);

  v = aosf44_col(m, 2);
  CHK(v4f_x(v) == 8.f);
  CHK(v4f_y(v) == 9.f);
  CHK(v4f_z(v) == 10.f);
  CHK(v4f_w(v) == 11.f);

  v = aosf44_col(m, 3);
  CHK(v4f_x(v) == 12.f);
  CHK(v4f_y(v) == 13.f);
  CHK(v4f_z(v) == 14.f);
  CHK(v4f_w(v) == 15.f);

  CHK(aosf44_set(m,
    v4f_set(0.f, 1.f, 2.f, 3.f),
    v4f_set(4.f, 5.f, 6.f, 7.f),
    v4f_set(8.f, 9.f, 10.f, 11.f),
    v4f_set(12.f, 13.f, 14.f, 15.f)) == m);
  CHK(aosf44_set(n,
    v4f_set(0.f, 2.f, 1.f, 3.f),
    v4f_set(1.f, -2.f, -1.f, -3.f),
    v4f_set(1.f, 0.f, 0.f, 2.f),
    v4f_set(3.f, 2.f, 1.f, 0.f)) == n);
  CHK(aosf44_add(o, m, n) == o);
  AOSF44_EQ(o,
    0.f, 3.f, 3.f, 6.f,
    5.f, 3.f, 5.f, 4.f,
    9.f, 9.f, 10.f, 13.f,
    15.f, 15.f, 15.f, 15.f);

  CHK(aosf44_sub(o, m, n) == o);
  AOSF44_EQ(o,
    0.f, -1.f, 1.f, 0.f,
    3.f, 7.f, 7.f, 10.f,
    7.f, 9.f, 10.f, 9.f,
    9.f, 11.f, 13.f, 15.f);

  CHK(aosf44_minus(o, n) == o);
  AOSF44_EQ(o,
    0.f, -2.f, -1.f, -3.f,
    -1.f, 2.f, 1.f, 3.f,
    -1.f, 0.f, 0.f, -2.f,
    -3.f, -2.f, -1.f, 0.f);

  CHK(aosf44_abs(o, o) == o);
  AOSF44_EQ(o,
    0.f, 2.f, 1.f, 3.f,
    1.f, 2.f, 1.f, 3.f,
    1.f, 0.f, 0.f, 2.f,
    3.f, 2.f, 1.f, 0.f);

  CHK(aosf44_mul(o, n, v4f_set(1.f, 2.f, 3.f, 2.f)) == o);
  AOSF44_EQ(o,
    0.f, 4.f, 3.f, 6.f,
    1.f, -4.f, -3.f, -6.f,
    1.f, 0.f, 0.f, 4.f,
    3.f, 4.f, 3.f, 0.f);

  aosf44_set(m,
    v4f_set(0.f, 1.f, 2.f, 3.f),
    v4f_set(4.f, 5.f, 6.f, 7.f),
    v4f_set(8.f, 9.f, 10.f, 11.f),
    v4f_set(12.f, 13.f, 14.f, 15.f));
  v = aosf44_mulf4(m, v4f_set(1.f, 2.f, 3.f, 1.f));
  CHK(v4f_x(v) == 44.f);
  CHK(v4f_y(v) == 51.f);
  CHK(v4f_z(v) == 58.f);
  CHK(v4f_w(v) == 65.f);

  v = aosf4_mulf44(v4f_set(1.f, 2.f, 3.f, 1.f), m);
  CHK(v4f_x(v) == 11.f);
  CHK(v4f_y(v) == 39.f);
  CHK(v4f_z(v) == 67.f);
  CHK(v4f_w(v) == 95.f);

  aosf44_set(m,
    v4f_set(1.f, 2.f, 3.f, 4.f),
    v4f_set(4.f, 5.f, 6.f, 7.f),
    v4f_set(7.f, 8.f, 9.f, 10.f),
    v4f_set(10.f, 11.f, 12.f, 13.f));
  aosf44_set(n,
    v4f_set(2.f, 9.f, 8.f, 1.f),
    v4f_set(1.f, -2.f, 2.f, 1.f),
    v4f_set(1.f, -8.f, -4.f, 2.f),
    v4f_set(1.f, 3.f, 4.f, 2.f));
  CHK(aosf44_mulf44(o, m, n) == o);
  AOSF44_EQ(o,
    104.f, 124.f, 144.f, 164.f,
    17.f, 19.f, 21.f, 23.f,
    -39.f, -48.f, -57.f, -66.f,
    61.f, 71.f, 81.f, 91.f);

  CHK(aosf44_transpose(o, n) == o);
  AOSF44_EQ(o,
    2.f, 1.f, 1.f, 1.f,
    9.f, -2.f, -8.f, 3.f,
    8.f, 2.f, -4.f, 4.f,
    1.f, 1.f, 2.f, 2.f);

  v = aosf44_det(n);
  CHK(v4f_x(v) == 78.f);
  CHK(v4f_y(v) == 78.f);
  CHK(v4f_z(v) == 78.f);
  CHK(v4f_w(v) == 78.f);

  v = aosf44_inverse(m, n);
  CHK(v4f_x(v) == 78.f);
  CHK(v4f_y(v) == 78.f);
  CHK(v4f_z(v) == 78.f);
  CHK(v4f_w(v) == 78.f);
  CHK(aosf44_mulf44(o, m, n) == o);
  AOSF44_EQ_EPS(o,
    1.f, 0.f, 0.f, 0.f,
    0.f, 1.f, 0.f, 0.f,
    0.f, 0.f, 1.f, 0.f,
    0.f, 0.f, 0.f, 1.f,
    1.e-6f);

  v = aosf44_invtrans(o, n);
  CHK(v4f_x(v) == 78.f);
  CHK(v4f_y(v) == 78.f);
  CHK(v4f_z(v) == 78.f);
  CHK(v4f_w(v) == 78.f);
  AOSF44_EQ(o,
    v4f_x(m[0]), v4f_x(m[1]), v4f_x(m[2]), v4f_x(m[3]),
    v4f_y(m[0]), v4f_y(m[1]), v4f_y(m[2]), v4f_y(m[3]),
    v4f_z(m[0]), v4f_z(m[1]), v4f_z(m[2]), v4f_z(m[3]),
    v4f_w(m[0]), v4f_w(m[1]), v4f_w(m[2]), v4f_w(m[3]));

  aosf44_set(m,
    v4f_set(0.f, 1.f, 2.f, 3.f),
    v4f_set(5.f, 5.f, 6.f, 7.f),
    v4f_set(8.f, 9.f, 10.f, 11.f),
    v4f_set(12.f, 13.f, 14.f, 15.f));
  aosf44_set(n,
     v4f_set(0.f, 1.f, 2.f, 3.f),
     v4f_set(5.f, 5.f, 6.f, 7.f),
     v4f_set(8.f, 9.f, 10.f, 11.f),
     v4f_set(12.f, 13.f, 14.f, 15.f));

  v = aosf44_eq(m, n);
  CHK(v4f_mask_x(v) == ~0);
  CHK(v4f_mask_y(v) == ~0);
  CHK(v4f_mask_z(v) == ~0);
  CHK(v4f_mask_w(v) == ~0);

  n[0] = v4f_set(0.f, 1.0f, 2.f, 4.f);
  v = aosf44_eq(m, n);
  CHK(v4f_mask_x(v) == 0);
  CHK(v4f_mask_y(v) == 0);
  CHK(v4f_mask_z(v) == 0);
  CHK(v4f_mask_w(v) == 0);
  n[0] = v4f_set(0.f, 1.0f, 2.f, 3.f);

  n[1] = v4f_set(4.f, 5.0f, 6.f, 7.f);
  v = aosf44_eq(m, n);
  CHK(v4f_mask_x(v) == 0);
  CHK(v4f_mask_y(v) == 0);
  CHK(v4f_mask_z(v) == 0);
  CHK(v4f_mask_w(v) == 0);
  n[1] = v4f_set(5.f, 5.0f, 6.f, 7.f);

  m[2] = v4f_set(8.f, -9.0f, 10.f, 11.f);
  v = aosf44_eq(m, n);
  CHK(v4f_mask_x(v) == 0);
  CHK(v4f_mask_y(v) == 0);
  CHK(v4f_mask_z(v) == 0);
  CHK(v4f_mask_w(v) == 0);
  m[2] = v4f_set(8.f, 9.0f, 10.f, 11.f);

  n[3] = v4f_set(12.f, 13.1f, 14.f, 15.f);
  v = aosf44_eq(m, n);
  CHK(v4f_mask_x(v) == 0);
  CHK(v4f_mask_y(v) == 0);
  CHK(v4f_mask_z(v) == 0);
  CHK(v4f_mask_w(v) == 0);

  v = aosf44_eq(m, m);
  CHK(v4f_mask_x(v) == ~0);
  CHK(v4f_mask_y(v) == ~0);
  CHK(v4f_mask_z(v) == ~0);
  CHK(v4f_mask_w(v) == ~0);
  n[3] = v4f_set(12.f, 13.0f, 14.f, 15.f);

  v = aosf44_eq(m, n);
  CHK(v4f_mask_x(v) == ~0);
  CHK(v4f_mask_y(v) == ~0);
  CHK(v4f_mask_z(v) == ~0);
  CHK(v4f_mask_w(v) == ~0);
  return 0;
}


