/* Copyright (C) 2013-2021 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "rsimd.h"
#include "math.h"

#include <math.h>

#define LOG2E 1.4426950408889634074 /* log_2 e */
#define LN10 2.30258509299404568402 /* log_e 10 */

#define CHKV8_EPS(V, Ref, Eps) {                                               \
  CHK(eq_eps(v4f_x(v8f_abcd(V)), Ref[0], fabsf(Ref[0]) * Eps));                \
  CHK(eq_eps(v4f_y(v8f_abcd(V)), Ref[1], fabsf(Ref[1]) * Eps));                \
  CHK(eq_eps(v4f_z(v8f_abcd(V)), Ref[2], fabsf(Ref[2]) * Eps));                \
  CHK(eq_eps(v4f_w(v8f_abcd(V)), Ref[3], fabsf(Ref[3]) * Eps));                \
  CHK(eq_eps(v4f_x(v8f_efgh(V)), Ref[4], fabsf(Ref[4]) * Eps));                \
  CHK(eq_eps(v4f_y(v8f_efgh(V)), Ref[5], fabsf(Ref[5]) * Eps));                \
  CHK(eq_eps(v4f_z(v8f_efgh(V)), Ref[6], fabsf(Ref[6]) * Eps));                \
  CHK(eq_eps(v4f_w(v8f_efgh(V)), Ref[7], fabsf(Ref[7]) * Eps));                \
} (void)0

#define CHKV8_FUNC_EPS(V, Func, Eps) {                                         \
  const v8f_T r__ = v8f_##Func(V);                                             \
  float ref__[8];                                                              \
  ref__[0] = (float)Func(v4f_x(v8f_abcd(V)));                                  \
  ref__[1] = (float)Func(v4f_y(v8f_abcd(V)));                                  \
  ref__[2] = (float)Func(v4f_z(v8f_abcd(V)));                                  \
  ref__[3] = (float)Func(v4f_w(v8f_abcd(V)));                                  \
  ref__[4] = (float)Func(v4f_x(v8f_efgh(V)));                                  \
  ref__[5] = (float)Func(v4f_y(v8f_efgh(V)));                                  \
  ref__[6] = (float)Func(v4f_z(v8f_efgh(V)));                                  \
  ref__[7] = (float)Func(v4f_w(v8f_efgh(V)));                                  \
  CHKV8_EPS(r__, ref__, Eps);                                                  \
} (void)0

static void
test_trigo(void)
{
  v8f_T i, j, k;
  float ref[8];

  i = v8f_set
    ((float)PI/2.f, (float)PI/3.f, (float)PI/4.f, (float)PI/6.f,
     (float)PI/8.f, (float)PI/7.f, (float)PI/16.f, (float)PI/9.f);

  CHKV8_FUNC_EPS(i, cos, 1.e-6f);
  CHKV8_FUNC_EPS(i, sin, 1.e-6f);

  v8f_sincos(i, &k, &j);
  ref[0] = (float)sin(v4f_x(v8f_abcd(i)));
  ref[1] = (float)sin(v4f_y(v8f_abcd(i)));
  ref[2] = (float)sin(v4f_z(v8f_abcd(i)));
  ref[3] = (float)sin(v4f_w(v8f_abcd(i)));
  ref[4] = (float)sin(v4f_x(v8f_efgh(i)));
  ref[5] = (float)sin(v4f_y(v8f_efgh(i)));
  ref[6] = (float)sin(v4f_z(v8f_efgh(i)));
  ref[7] = (float)sin(v4f_w(v8f_efgh(i)));
  CHKV8_EPS(k, ref, 1.e-6f);
  ref[0] = (float)cos(v4f_x(v8f_abcd(i)));
  ref[1] = (float)cos(v4f_y(v8f_abcd(i)));
  ref[2] = (float)cos(v4f_z(v8f_abcd(i)));
  ref[3] = (float)cos(v4f_w(v8f_abcd(i)));
  ref[4] = (float)cos(v4f_x(v8f_efgh(i)));
  ref[5] = (float)cos(v4f_y(v8f_efgh(i)));
  ref[6] = (float)cos(v4f_z(v8f_efgh(i)));
  ref[7] = (float)cos(v4f_w(v8f_efgh(i)));
  CHKV8_EPS(j, ref, 1.e-6f);

  i = v8f_set
    ((float)PI/2.2f, (float)PI/3.f, (float)PI/4.f, (float)PI/6.f,
     (float)PI/8.f, (float)PI/7.f, (float)PI/16.f, (float)PI/9.f);

  CHKV8_FUNC_EPS(i, tan, 1.e-6);
  CHKV8_FUNC_EPS(v8f_cos(i), acos, 1.e-6f);
  CHKV8_FUNC_EPS(v8f_sin(i), asin, 1.e-6f);
  CHKV8_FUNC_EPS(v8f_tan(i), atan, 1.e-6f);
}

static void
test_exp(void)
{
  const v8f_T i = v8f_set
    (1.f, -1.234f, 0.f, 3.14156f, 0.9187f, 7.9f, 3.333f, 2.387e-7f);
  v8f_T j;
  float ref[8];

  CHKV8_FUNC_EPS(i, exp, 1.e-6f);
  CHKV8_FUNC_EPS(i, exp2, 1.e-6f);

  j = v8f_exp10(i);
  ref[0] = (float)exp2(LOG2E * LN10 * v4f_x(v8f_abcd(i)));
  ref[1] = (float)exp2(LOG2E * LN10 * v4f_y(v8f_abcd(i)));
  ref[2] = (float)exp2(LOG2E * LN10 * v4f_z(v8f_abcd(i)));
  ref[3] = (float)exp2(LOG2E * LN10 * v4f_w(v8f_abcd(i)));
  ref[4] = (float)exp2(LOG2E * LN10 * v4f_x(v8f_efgh(i)));
  ref[5] = (float)exp2(LOG2E * LN10 * v4f_y(v8f_efgh(i)));
  ref[6] = (float)exp2(LOG2E * LN10 * v4f_z(v8f_efgh(i)));
  ref[7] = (float)exp2(LOG2E * LN10 * v4f_w(v8f_efgh(i)));
  CHKV8_EPS(j, ref, 1.e-6f);
}

static void
test_log(void)
{
  const v8f_T i = v8f_set
    (4.675f, 3.14f, 9.99999f, 1.234e-13f, 3.33e-3f, 0.98f, 8.f, 9.87654f);
  CHKV8_FUNC_EPS(i, log, 1.e-6f);
  CHKV8_FUNC_EPS(i, log2, 1.e-6f);
  CHKV8_FUNC_EPS(i, log10, 1.e-6f);
}

static void
test_misc(void)
{
  v8f_T i, j, k;
  float ref[8];

  i = v8f_set(-1.2345f, 9.3e-7f, 3.879e9f, -10.56f, 9.9f, -3.1f, 0.33e-6f, 1.f);
  j = v8f_set(7.89e-9f, 0.12f, -4.9e10f, 3.14f, 5.f, 0.1e-19f, 1.234f, -0.45f);
  k = v8f_copysign(i, j);
  ref[0] = (float)copysign(v4f_x(v8f_abcd(i)), v4f_x(v8f_abcd(j)));
  ref[1] = (float)copysign(v4f_y(v8f_abcd(i)), v4f_y(v8f_abcd(j)));
  ref[2] = (float)copysign(v4f_z(v8f_abcd(i)), v4f_z(v8f_abcd(j)));
  ref[3] = (float)copysign(v4f_w(v8f_abcd(i)), v4f_w(v8f_abcd(j)));
  ref[4] = (float)copysign(v4f_x(v8f_efgh(i)), v4f_x(v8f_efgh(j)));
  ref[5] = (float)copysign(v4f_y(v8f_efgh(i)), v4f_y(v8f_efgh(j)));
  ref[6] = (float)copysign(v4f_z(v8f_efgh(i)), v4f_z(v8f_efgh(j)));
  ref[7] = (float)copysign(v4f_w(v8f_efgh(i)), v4f_w(v8f_efgh(j)));
  CHKV8_EPS(k, ref, 1.e-6f);

  CHKV8_FUNC_EPS(i, floor, 1.e-6f);

  k = v8f_pow(v8f_abs(i), j);
  ref[0] = (float)pow(fabsf(v4f_x(v8f_abcd(i))), v4f_x(v8f_abcd(j)));
  ref[1] = (float)pow(fabsf(v4f_y(v8f_abcd(i))), v4f_y(v8f_abcd(j)));
  ref[2] = (float)pow(fabsf(v4f_z(v8f_abcd(i))), v4f_z(v8f_abcd(j)));
  ref[3] = (float)pow(fabsf(v4f_w(v8f_abcd(i))), v4f_w(v8f_abcd(j)));
  ref[4] = (float)pow(fabsf(v4f_x(v8f_efgh(i))), v4f_x(v8f_efgh(j)));
  ref[5] = (float)pow(fabsf(v4f_y(v8f_efgh(i))), v4f_y(v8f_efgh(j)));
  ref[6] = (float)pow(fabsf(v4f_z(v8f_efgh(i))), v4f_z(v8f_efgh(j)));
  ref[7] = (float)pow(fabsf(v4f_w(v8f_efgh(i))), v4f_w(v8f_efgh(j)));
  CHKV8_EPS(k, ref, 1.e-6f);
}

int
main(int argc, char** argv)
{
  (void)argc, (void)argv;

  test_trigo();
  test_exp();
  test_log();
  test_misc();

  return 0;
}

