/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_SSE_H
#define RSIMD_SSE_H

#include "ssef.h"
#include "ssei.h"

static FINLINE v4i_T v4f_to_v4i(const v4f_T v) {return _mm_cvtps_epi32(v);}
static FINLINE v4f_T v4i_to_v4f(const v4i_T v) {return _mm_cvtepi32_ps(v);}
static FINLINE v4i_T v4f_trunk_v4i(const v4f_T v) {return _mm_cvttps_epi32(v);}

/* Reinterpret cast */
static FINLINE v4i_T v4f_rcast_v4i(const v4f_T v) {return _mm_castps_si128(v);}
static FINLINE v4f_T v4i_rcast_v4f(const v4i_T v) {return _mm_castsi128_ps(v);}

#endif /* SIMD_SSE_H */

