/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_SSE_SWZ_H
#define RSIMD_SSE_SWZ_H

/*
 * Helper macro for the generation of swizzling function
 */

/* Swizzle operands */
#define SWZ_X__ 0
#define SWZ_Y__ 1
#define SWZ_Z__ 2
#define SWZ_W__ 3

#define V4_SWZ_0__(Prefix, Func, Append, Op0, Op1, Op2, Op3)                   \
  static FINLINE Prefix##_T                                                    \
  Prefix ##_ ## Func ## Append(Prefix##_T vec)                                 \
  {                                                                            \
    return Prefix ## _SWZ__(vec, Op0, Op1, Op2, Op3);                          \
  }

#define V4_SWZ_1__(Prefix, Func, Append, Op0, Op1, Op2)                        \
  V4_SWZ_0__(Prefix, Func ## Append, x, Op0, Op1, Op2, SWZ_X__)                \
  V4_SWZ_0__(Prefix, Func ## Append, y, Op0, Op1, Op2, SWZ_Y__)                \
  V4_SWZ_0__(Prefix, Func ## Append, z, Op0, Op1, Op2, SWZ_Z__)                \
  V4_SWZ_0__(Prefix, Func ## Append, w, Op0, Op1, Op2, SWZ_W__)

#define V4_SWZ_2__(Prefix, Func, Append, Op0, Op1)                             \
  V4_SWZ_1__(Prefix, Func ## Append, x, Op0, Op1, SWZ_X__)                     \
  V4_SWZ_1__(Prefix, Func ## Append, y, Op0, Op1, SWZ_Y__)                     \
  V4_SWZ_1__(Prefix, Func ## Append, z, Op0, Op1, SWZ_Z__)                     \
  V4_SWZ_1__(Prefix, Func ## Append, w, Op0, Op1, SWZ_W__)

#define V4_SWZ_3__(Prefix, Func, Op0)                                          \
  V4_SWZ_2__(Prefix, Func, x, Op0, SWZ_X__)                                    \
  V4_SWZ_2__(Prefix, Func, y, Op0, SWZ_Y__)                                    \
  V4_SWZ_2__(Prefix, Func, z, Op0, SWZ_Z__)                                    \
  V4_SWZ_2__(Prefix, Func, w, Op0, SWZ_W__)

#define GENERATE_V4_SWZ_FUNCS__(Prefix)                                        \
  V4_SWZ_3__(Prefix, x, SWZ_X__)                                               \
  V4_SWZ_3__(Prefix, y, SWZ_Y__)                                               \
  V4_SWZ_3__(Prefix, z, SWZ_Z__)                                               \
  V4_SWZ_3__(Prefix, w, SWZ_W__)

#endif /* RSIMD_SSE_SWZ_H */


