/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_SSEF_H
#define RSIMD_SSEF_H

/*
 * 4 packed single precision floating-point values
 */

#include "sse_swz.h"

#include <rsys/math.h>
#include <xmmintrin.h>
#include <emmintrin.h>
#ifdef SIMD_SSE4_1
  #include <smmintrin.h>
#endif
#ifdef FMADD
  #include <immintrin.h>
#endif

typedef __m128 v4f_T;
#define V4F_AT__(Vec, Id) __builtin_ia32_vec_ext_v4sf(Vec, Id)

#define v4f_SWZ__(Vec, Op0, Op1, Op2, Op3)                                     \
  _mm_shuffle_ps(vec, vec, _MM_SHUFFLE(Op3, Op2, Op1, Op0))
GENERATE_V4_SWZ_FUNCS__(v4f) /* Swizzle operations */

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE float*
v4f_store(float dst[4], v4f_T v)
{
  ASSERT(dst && IS_ALIGNED(dst, 16));
  _mm_store_ps(dst, v);
  return dst;
}

static FINLINE v4f_T
v4f_load(const float src[4])
{
  ASSERT(src && IS_ALIGNED(src, 16));
  return _mm_load_ps(src);
}

static FINLINE v4f_T
v4f_loadu(const float src[4])
{
  ASSERT(src);
  return _mm_set_ps(src[3], src[2], src[1], src[0]);
}

static FINLINE v4f_T
v4f_loadu3(const float src[3])
{
  ASSERT(src);
  return _mm_set_ps(0.f, src[2], src[1], src[0]);
}

static FINLINE v4f_T
v4f_set1(const float x)
{
  return _mm_set1_ps(x);
}

static FINLINE v4f_T
v4f_set(const float x, const float y, const float z, const float w)
{
  return _mm_set_ps(w, z, y, x);
}

static FINLINE v4f_T
v4f_zero(void)
{
  return _mm_setzero_ps();
}

static FINLINE v4f_T
v4f_mask(const int32_t x, const int32_t y, const int32_t z, const int32_t w)
{
  return _mm_castsi128_ps(_mm_set_epi32(w, z, y, x));
}

static FINLINE v4f_T
v4f_mask1(const int32_t x)
{
  return _mm_castsi128_ps(_mm_set1_epi32(x));
}

static FINLINE v4f_T
v4f_true(void)
{
  return _mm_castsi128_ps(_mm_set1_epi32(~0));
}

static FINLINE v4f_T
v4f_false(void)
{
  return v4f_zero();
}

/*******************************************************************************
 * Extract components
 ******************************************************************************/
static FINLINE float v4f_x(const v4f_T v) { return V4F_AT__(v, 0); }
static FINLINE float v4f_y(const v4f_T v) { return V4F_AT__(v, 1); }
static FINLINE float v4f_z(const v4f_T v) { return V4F_AT__(v, 2); }
static FINLINE float v4f_w(const v4f_T v) { return V4F_AT__(v, 3); }

static FINLINE int32_t
v4f_mask_x(const v4f_T v)
{
  union { float f; int32_t i; } ucast;
  ucast.f = v4f_x(v);
  return ucast.i;
}

static FINLINE int32_t
v4f_mask_y(const v4f_T v)
{
  union { float f; int32_t i; } ucast;
  ucast.f = v4f_y(v);
  return ucast.i;
}

static FINLINE int32_t
v4f_mask_z(const v4f_T v)
{
  union { float f; int32_t i; } ucast;
  ucast.f = v4f_z(v);
  return ucast.i;
}

static FINLINE int32_t
v4f_mask_w(const v4f_T v)
{
  union { float f; int32_t i; } ucast;
  ucast.f = v4f_w(v);
  return ucast.i;
}

static FINLINE int
v4f_movemask(const v4f_T v)
{
  return _mm_movemask_ps(v);
}

/*******************************************************************************
 * Merge operations
 ******************************************************************************/
static FINLINE v4f_T
v4f_xayb(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_unpacklo_ps(xyzw, abcd);
}

static FINLINE v4f_T
v4f_xyab(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_movelh_ps(xyzw, abcd);
}

static FINLINE v4f_T
v4f_zcwd(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_unpackhi_ps(xyzw, abcd);
}

static FINLINE v4f_T
v4f_zwcd(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_movehl_ps(abcd, xyzw);
}

static FINLINE v4f_T
v4f_ayzw(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_move_ss(xyzw, abcd);
}

static FINLINE v4f_T
v4f_xycd(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_shuffle_ps(xyzw, abcd, _MM_SHUFFLE(3, 2, 1, 0));
}

static FINLINE v4f_T
v4f_ywbd(const v4f_T xyzw, const v4f_T abcd)
{
  return _mm_shuffle_ps(xyzw, abcd, _MM_SHUFFLE(3, 1, 3, 1));
}

static FINLINE v4f_T
v4f_xbzw(const v4f_T xyzw, const v4f_T abcd)
{
  const v4f_T zwzw = _mm_movehl_ps(xyzw, xyzw);
  const v4f_T abzw = _mm_movelh_ps(abcd, zwzw);
  return _mm_move_ss(abzw, xyzw);
}

static FINLINE v4f_T
v4f_xycw(const v4f_T xyzw, const v4f_T abcd)
{
#if 0 /* SSE3 */
  const v4f_T yyww = _mm_movehdup_ps(xyzw);
#else
  const v4f_T yyww = v4f_yyww(xyzw);
#endif
  const v4f_T cwdw = _mm_unpackhi_ps(abcd, yyww);
  return _mm_movelh_ps(xyzw, cwdw);
}

static FINLINE v4f_T
v4f_xyzd(const v4f_T xyzw, const v4f_T abcd)
{
#if 0 /* SSE3 */
  const v4f_T bbdd = _mm_movehdup_ps(abcd);
#else
  const v4f_T bbdd = v4f_yyww(abcd);
#endif
  const v4f_T zdwd = _mm_unpackhi_ps(xyzw, bbdd);
  return _mm_movelh_ps(xyzw, zdwd);
}

static FINLINE v4f_T
v4f_048C
  (const v4f_T v0123, const v4f_T v4567, const v4f_T v89AB, const v4f_T vCDEF)
{
  const v4f_T v0415 = v4f_xayb(v0123, v4567);
  const v4f_T v8C9D = v4f_xayb(v89AB, vCDEF);
  return v4f_xyab(v0415, v8C9D);
}

/*******************************************************************************
 * Bitwise operations
 ******************************************************************************/
static FINLINE v4f_T
v4f_or(const v4f_T v0, const v4f_T v1)
{
  return _mm_or_ps(v0, v1);
}

static FINLINE v4f_T
v4f_and(const v4f_T v0, const v4f_T v1)
{
  return _mm_and_ps(v0, v1);
}

static FINLINE v4f_T
v4f_andnot(const v4f_T v0, const v4f_T v1)
{
  return _mm_andnot_ps(v0, v1);
}

static FINLINE v4f_T
v4f_xor(const v4f_T v0, const v4f_T v1)
{
  return _mm_xor_ps(v0, v1);
}

static FINLINE v4f_T
v4f_sel(const v4f_T vfalse, const v4f_T vtrue, const v4f_T vcond)
{
#ifdef SIMD_SSE4_1
  return _mm_blendv_ps(vfalse, vtrue, vcond);
#else
  return v4f_xor(vfalse, v4f_and(vcond, v4f_xor(vfalse, vtrue)));
#endif
}

/*******************************************************************************
 * Arithmetic operations
 ******************************************************************************/
static FINLINE v4f_T
v4f_minus(const v4f_T v)
{
  return v4f_xor(v4f_set1(-0.f), v);
}

static FINLINE v4f_T
v4f_add(const v4f_T v0, const v4f_T v1)
{
  return _mm_add_ps(v0, v1);
}

static FINLINE v4f_T
v4f_sub(const v4f_T v0, const v4f_T v1)
{
  return _mm_sub_ps(v0, v1);
}

static FINLINE v4f_T
v4f_mul(const v4f_T v0, const v4f_T v1)
{
  return _mm_mul_ps(v0, v1);
}

static FINLINE v4f_T
v4f_div(const v4f_T v0, const v4f_T v1)
{
  return _mm_div_ps(v0, v1);
}

static FINLINE v4f_T
v4f_madd(const v4f_T v0, const v4f_T v1, const v4f_T v2)
{
#ifdef FMADD
  return _mm_fmadd_ps(v0, v1, v2);
#else
  return _mm_add_ps(_mm_mul_ps(v0, v1), v2);
#endif
}

static FINLINE v4f_T
v4f_abs(const v4f_T v)
{
  const union { int32_t i; float f; } mask = { 0x7fffffff };
  return v4f_and(v, v4f_set1(mask.f));
}

static FINLINE v4f_T
v4f_sqrt(const v4f_T v)
{
  return _mm_sqrt_ps(v);
}

static FINLINE v4f_T
v4f_rsqrte(const v4f_T v)
{
  return _mm_rsqrt_ps(v);
}

static FINLINE v4f_T
v4f_rsqrt(const v4f_T v)
{
  const v4f_T y = v4f_rsqrte(v);
  const v4f_T yyv = v4f_mul(v4f_mul(y, y), v);
  const v4f_T tmp = v4f_sub(v4f_set1(1.5f), v4f_mul(yyv, v4f_set1(0.5f)));
  return v4f_mul(tmp, y);
}

static FINLINE v4f_T
v4f_rcpe(const v4f_T v)
{
  return _mm_rcp_ps(v);
}

static FINLINE v4f_T
v4f_rcp(const v4f_T v)
{
  const v4f_T y = v4f_rcpe(v);
  const v4f_T tmp = v4f_sub(v4f_set1(2.f), v4f_mul(y, v));
  return v4f_mul(tmp, y);
}

static FINLINE v4f_T
v4f_lerp(const v4f_T from, const v4f_T to, const v4f_T param)
{
  return v4f_madd(v4f_sub(to, from), param, from);
}

static FINLINE v4f_T
v4f_sum(const v4f_T v)
{
#if 0 /* SSE3 */
  const v4f_T r0 = _mm_hadd_ps(v, v);
  return _mm_hadd_ps(r0, r0);
#else
  const v4f_T yxwz = v4f_yxwz(v);
  const v4f_T tmp0 = v4f_add(v, yxwz); /* x+y, y+x, z+w, w+z */
  const v4f_T tmp1 = v4f_wzyx(tmp0);   /* w+z, z+w, y+x, x+y */
  return v4f_add(tmp0, tmp1);
#endif
}

static FINLINE v4f_T
v4f_dot(const v4f_T v0, const v4f_T v1)
{
  return v4f_sum(v4f_mul(v0, v1));
}

static FINLINE v4f_T
v4f_len(const v4f_T v)
{
  return v4f_sqrt(v4f_dot(v, v));
}

static FINLINE v4f_T
v4f_normalize(const v4f_T v)
{
  return v4f_mul(v, v4f_rsqrt(v4f_dot(v, v)));
}

static FINLINE v4f_T
v4f_sum2(const v4f_T v)
{
#if 0 /* SSE3 */
  return v4f_xxxx(_mm_hadd_ps(v, v));
#else
  return v4f_add(v4f_xxyy(v), v4f_yyxx(v));
#endif
}

static FINLINE v4f_T
v4f_dot2(const v4f_T v0, const v4f_T v1)
{
  return v4f_sum2(v4f_mul(v0, v1));
}

static FINLINE v4f_T
v4f_len2(const v4f_T v)
{
  return v4f_sqrt(v4f_dot2(v, v));
}

static FINLINE v4f_T
v4f_cross2(const v4f_T v0, const v4f_T v1)
{
  const v4f_T v = v4f_mul(v0, v4f_yxyx(v1));
  return v4f_sub(v4f_xxxx(v), v4f_yyyy(v));
}

static FINLINE v4f_T
v4f_normalize2(const v4f_T v)
{
  return v4f_mul(v, v4f_rsqrt(v4f_dot2(v, v)));
}

static FINLINE v4f_T
v4f_sum3(const v4f_T v)
{
  const union { int32_t i; float f; } m = { ~0 };
  const v4f_T r0 = v4f_and(v4f_set(m.f, m.f, m.f, 0.f), v);
#if 0 /* SSE3 */
  const v4f_T r1 = _mm_hadd_ps(r0, r0);
  return _mm_hadd_ps(r1, r1);
#else
  return v4f_sum(r0);
#endif
}

static FINLINE v4f_T
v4f_dot3(const v4f_T v0, const v4f_T v1)
{
  return v4f_sum3(v4f_mul(v0, v1));
}

static FINLINE v4f_T
v4f_len3(const v4f_T v)
{
  return v4f_sqrt(v4f_dot3(v, v));
}

static FINLINE v4f_T
v4f_cross3(const v4f_T v0, const v4f_T v1)
{
  const v4f_T r0 = v4f_mul(v0, v4f_yzxw(v1));
  const v4f_T r1 = v4f_mul(v1, v4f_yzxw(v0));
  return v4f_yzxw(v4f_sub(r0, r1));
}

static FINLINE v4f_T
v4f_normalize3(const v4f_T v)
{
  return v4f_mul(v, v4f_rsqrt(v4f_dot3(v, v)));
}

/*******************************************************************************
 * Comparators
 ******************************************************************************/
static FINLINE v4f_T
v4f_eq(const v4f_T v0, const v4f_T v1)
{
  return _mm_cmpeq_ps(v0, v1);
}

static FINLINE v4f_T
v4f_neq(const v4f_T v0, const v4f_T v1)
{
  return _mm_cmpneq_ps(v0, v1);
}

static FINLINE v4f_T
v4f_ge(const v4f_T v0, const v4f_T v1)
{
  return _mm_cmpge_ps(v0, v1);
}

static FINLINE v4f_T
v4f_le(const v4f_T v0, const v4f_T v1)
{
  return _mm_cmple_ps(v0, v1);
}

static FINLINE v4f_T
v4f_gt(const v4f_T v0, const v4f_T v1)
{
  return _mm_cmpgt_ps(v0, v1);
}

static FINLINE v4f_T
v4f_lt(const v4f_T v0, const v4f_T v1)
{
  return _mm_cmplt_ps(v0, v1);
}

static FINLINE v4f_T
v4f_eq_eps(const v4f_T v0, const v4f_T v1, const v4f_T eps)
{
  return v4f_le(v4f_abs(v4f_sub(v0, v1)), eps);
}

static FINLINE v4f_T
v4f_min(const v4f_T v0, const v4f_T v1)
{
  return _mm_min_ps(v0, v1);
}

static FINLINE v4f_T
v4f_max(const v4f_T v0, const v4f_T v1)
{
  return _mm_max_ps(v0, v1);
}

static FINLINE v4f_T
v4f_reduce_min(const v4f_T v)
{
  const v4f_T tmp = v4f_min(v4f_yxwz(v), v);
  return v4f_min(v4f_zwxy(tmp), tmp);
}

static FINLINE v4f_T
v4f_reduce_max(const v4f_T v)
{
  const v4f_T tmp = v4f_max(v4f_yxwz(v), v);
  return v4f_max(v4f_zwxy(tmp), tmp);
}

static FINLINE v4f_T
v4f_clamp(const v4f_T v, const v4f_T vmin, const v4f_T vmax)
{
  return v4f_min(v4f_max(v, vmin), vmax);
}

#endif /* RSIMD_SSEF_H */

