/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_SSEI_H
#define RSIMD_SSEI_H

/*
 * 4 packed signed integers
 */

#include "sse_swz.h"

#include <rsys/math.h>
#include <xmmintrin.h>
#include <emmintrin.h>
#ifdef SIMD_SSE4_1
  #include <smmintrin.h>
#endif

typedef __m128i v4i_T;
#define V4I_AT__(Vec, Id) __builtin_ia32_vec_ext_v4si((__v4si)Vec, Id)

#define v4i_SWZ__(Vec, Op0, Op1, Op2, Op3)                                     \
  _mm_shuffle_epi32(Vec, _MM_SHUFFLE(Op3, Op2, Op1, Op0))
GENERATE_V4_SWZ_FUNCS__(v4i) /* Swizzle operations */

/*******************************************************************************
 * Set operations
 ******************************************************************************/
static FINLINE int32_t*
v4i_store(int32_t dst[4], v4i_T v)
{
  ASSERT(dst && IS_ALIGNED(dst, 16));
  _mm_store_si128((v4i_T*)dst, v);
  return dst;
}

static FINLINE v4i_T
v4i_load(const int32_t src[4])
{
  ASSERT(src && IS_ALIGNED(src, 16));
  return _mm_load_si128((const v4i_T*)src);
}

static FINLINE v4i_T
v4i_set1(const int32_t i)
{
  return _mm_set1_epi32(i);
}

static FINLINE v4i_T
v4i_set(const int32_t x, const int32_t y, const int32_t z, const int32_t w)
{
  return _mm_set_epi32(w, z, y, x);
}

static FINLINE v4i_T
v4i_zero(void)
{
  return _mm_setzero_si128();
}

/*******************************************************************************
 * Extract int32 from SIMD packed representation
 ******************************************************************************/
static FINLINE int32_t v4i_x(const v4i_T v) { return V4I_AT__(v, 0); }
static FINLINE int32_t v4i_y(const v4i_T v) { return V4I_AT__(v, 1); }
static FINLINE int32_t v4i_z(const v4i_T v) { return V4I_AT__(v, 2); }
static FINLINE int32_t v4i_w(const v4i_T v) { return V4I_AT__(v, 3); }

/*******************************************************************************
 * Merge operations
 ******************************************************************************/
static FINLINE v4i_T
v4i_xayb(const v4i_T xyzw, const v4i_T abcd)
{
  return _mm_unpacklo_epi32(xyzw, abcd);
}

static FINLINE v4i_T
v4i_zcwd(const v4i_T xyzw, const v4i_T abcd)
{
  return _mm_unpackhi_epi32(xyzw, abcd);
}

/*******************************************************************************
 * Bitwise operators
 ******************************************************************************/
static FINLINE v4i_T
v4i_or(const v4i_T v0, const v4i_T v1)
{
  return _mm_or_si128(v0, v1);
}

static FINLINE v4i_T
v4i_and(const v4i_T v0, const v4i_T v1)
{
  return _mm_and_si128(v0, v1);
}

static FINLINE v4i_T
v4i_andnot(const v4i_T v0, const v4i_T v1)
{
  return _mm_andnot_si128(v0, v1);
}

static FINLINE v4i_T
v4i_xor(const v4i_T v0, const v4i_T v1)
{
  return _mm_xor_si128(v0, v1);
}

static FINLINE v4i_T
v4i_not(const v4i_T v)
{
  return _mm_xor_si128(v, _mm_set1_epi32(-1));
}

static FINLINE v4i_T
v4i_rshift(const v4i_T v, const int32_t rshift)
{
  return _mm_srli_epi32(v, rshift);
}

static FINLINE v4i_T
v4i_lshift(const v4i_T v, const int32_t lshift)
{
  return _mm_slli_epi32(v, lshift);
}

/*******************************************************************************
 * Arithmetic operators
 ******************************************************************************/
static FINLINE v4i_T
v4i_add(const v4i_T v0, const v4i_T v1)
{
  return _mm_add_epi32(v0, v1);
}

static FINLINE v4i_T
v4i_sub(const v4i_T v0, const v4i_T v1)
{
  return _mm_sub_epi32(v0, v1);
}

static FINLINE v4i_T
v4i_minus(const v4i_T v)
{
  return v4i_add(v4i_not(v), v4i_set1(1));
}

/*******************************************************************************
 * Comparators
 ******************************************************************************/
static FINLINE v4i_T
v4i_eq(const v4i_T v0, const v4i_T v1)
{
  return _mm_cmpeq_epi32(v0, v1);
}

static FINLINE v4i_T
v4i_neq(const v4i_T v0, const v4i_T v1)
{
  return v4i_xor(v4i_eq(v0, v1), v4i_set1(-1));
}

static FINLINE v4i_T
v4i_gt(const v4i_T v0, const v4i_T v1)
{
  return _mm_cmpgt_epi32(v0, v1);
}

static FINLINE v4i_T
v4i_lt(const v4i_T v0, const v4i_T v1)
{
  return _mm_cmplt_epi32(v0, v1);
}

static FINLINE v4i_T
v4i_ge(const v4i_T v0, const v4i_T v1)
{
  return v4i_xor(v4i_lt(v0, v1), v4i_set1(-1));
}

static FINLINE v4i_T
v4i_le(const v4i_T v0, const v4i_T v1)
{
  return v4i_xor(v4i_gt(v0, v1), v4i_set1(-1));
}

static FINLINE v4i_T
v4i_sel(const v4i_T vfalse, const v4i_T vtrue, const v4i_T vcond)
{
#ifdef SIMD_SSE4_1
  return _mm_blendv_epi8(vfalse, vtrue, vcond);
#else
  return v4i_xor(vfalse, v4i_and(vcond, v4i_xor(vfalse, vtrue)));
#endif
}

static FINLINE v4i_T
v4i_min(const v4i_T v0, const v4i_T v1)
{
#ifdef SIMD_SSE4_1
  return _mm_min_epi32(v0, v1);
#else
  ALIGN(16) int32_t a[4];
  ALIGN(16) int32_t b[4];
  v4i_store(a, v0);
  v4i_store(b, v1);
  return v4i_set
    (MMIN(a[0], b[0]),
     MMIN(a[1], b[1]),
     MMIN(a[2], b[2]),
     MMIN(a[3], b[3]));
#endif
}

static FINLINE v4i_T
v4i_max(const v4i_T v0, const v4i_T v1)
{
#ifdef SIMD_SSE4_1
  return _mm_max_epi32(v0, v1);
#else
  ALIGN(16) int32_t a[4];
  ALIGN(16) int32_t b[4];
  v4i_store(a, v0);
  v4i_store(b, v1);
  return v4i_set
    (MMAX(a[0], b[0]),
     MMAX(a[1], b[1]),
     MMAX(a[2], b[2]),
     MMAX(a[3], b[3]));
#endif
}

static FINLINE v4i_T
v4i_reduce_min(const v4i_T v)
{
#ifdef SIMD_SSE4_1
  const v4i_T tmp = v4i_min(v4i_yxwz(v), v);
  return v4i_min(v4i_zwxy(tmp), tmp);
#else
  ALIGN(16) int32_t a[4];
  v4i_store(a, v);
  return v4i_set1(MMIN(MMIN(a[0], a[1]), MMIN(a[2], a[3])));
#endif
}

static FINLINE v4i_T
v4i_reduce_max(const v4i_T v)
{
#ifdef SIMD_SSE4_1
  const v4i_T tmp = v4i_max(v4i_yxwz(v), v);
  return v4i_max(v4i_zwxy(tmp), tmp);
#else
  ALIGN(16) int32_t a[4];
  v4i_store(a, v);
  return v4i_set1(MMAX(MMAX(a[0], a[1]), MMAX(a[2], a[3])));
#endif
}

static FINLINE int32_t
v4i_reduce_min_i32(const v4i_T v)
{
  return v4i_x(v4i_reduce_min(v));
}

static FINLINE int32_t
v4i_reduce_max_i32(const v4i_T v)
{
  return v4i_x(v4i_reduce_max(v));
}

#endif /* RSIMD_SSEI_H */

