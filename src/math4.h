/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_MATH4_H
#define RSIMD_MATH4_H

#define RSIMD_WIDTH__ 4
#include "vXf_begin.h"
#include "mathX.h"
#include "vXf_end.h"

/*******************************************************************************
 * Miscellaneous
 ******************************************************************************/
static FINLINE v4f_T /* Cartesian (xyz) to spherical (r, theta, phi)*/
v4f_xyz_to_rthetaphi(const v4f_T v)
{
  const v4f_T zero = v4f_zero();
  const v4f_T len2 = v4f_len2(v);
  const v4f_T len3 = v4f_len3(v);
  const v4f_T theta = v4f_sel
    (v4f_acos(v4f_div(v4f_zzzz(v), len3)), zero, v4f_eq(len3, zero));
  const v4f_T tmp_phi = v4f_sel
    (v4f_asin(v4f_div(v4f_yyyy(v), len2)), zero, v4f_eq(len2, zero));
  const v4f_T phi = v4f_sel
    (v4f_sub(v4f_set1((float)PI), tmp_phi),tmp_phi, v4f_ge(v4f_xxxx(v), zero));
  return v4f_xyab(v4f_xayb(len3, theta), phi);
}
#endif /* RSIMD_MATH4_H */
