/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"
#include <rsys/rsys.h>

/* Check macros */
#ifndef SOA_SIMD_WIDTH
  #error "Missing the SOA_SIMD_WIDTH macro"
#endif
#if SOA_SIMD_WIDTH != 4 && SOA_SIMD_WIDTH != 8
  #error "Invalid value for the SOA_SIMD_WIDTH macro"
#endif
#ifndef SOA_DIMENSION
  #error "Missing the SOA_DIMENSION macro"
#endif
#if SOA_DIMENSION < 2 || SOA_DIMENSION > 4
  #error "Invalid value for the SOA_DIMENSION macro"
#endif

/* Define macros generics to the SOA_SIMD_WIDTH parameter */
#if SOA_SIMD_WIDTH == 4
  #define soaX soa4
  #define vXf(Func) CONCAT(v4f_, Func)
  #define vXf_T v4f_T
  #define VEC(A, B, C, D, E, F, G, H) v4f_set(A, B, C, D)
  #define MASK(A, B, C, D, E, F, G, H) v4f_mask(A, B, C, D)
  #define CHKVX(V0, V1) {                                                      \
    const v4f_T v0__ = (V0);                                                   \
    const v4f_T v1__ = (V1);                                                   \
    CHK(v4f_mask_x(v0__) == v4f_mask_y(v1__));                                 \
    CHK(v4f_mask_y(v0__) == v4f_mask_y(v1__));                                 \
    CHK(v4f_mask_z(v0__) == v4f_mask_z(v1__));                                 \
    CHK(v4f_mask_w(v0__) == v4f_mask_w(v1__));                                 \
  } (void)0
#elif SOA_SIMD_WIDTH == 8
  #define soaX soa8
  #define vXf(Func) CONCAT(v8f_, Func)
  #define vXf_T v8f_T
  #define VEC(A, B, C, D, E, F, G, H) v8f_set(A, B, C, D, E, F, G, H)
  #define MASK(A, B, C, D, E, F, G, H) v8f_mask(A, B, C, D, E, F, G, H)
  #define CHKVX(V0, V1) {                                                      \
    const v8f_T v0__ = (V0);                                                   \
    const v8f_T v1__ = (V1);                                                   \
    CHK(v4f_mask_x(v8f_abcd(v0__)) == v4f_mask_y(v8f_abcd(v1__)));             \
    CHK(v4f_mask_y(v8f_abcd(v0__)) == v4f_mask_y(v8f_abcd(v1__)));             \
    CHK(v4f_mask_z(v8f_abcd(v0__)) == v4f_mask_z(v8f_abcd(v1__)));             \
    CHK(v4f_mask_w(v8f_abcd(v0__)) == v4f_mask_w(v8f_abcd(v1__)));             \
    CHK(v4f_mask_x(v8f_efgh(v0__)) == v4f_mask_y(v8f_efgh(v1__)));             \
    CHK(v4f_mask_y(v8f_efgh(v0__)) == v4f_mask_y(v8f_efgh(v1__)));             \
    CHK(v4f_mask_z(v8f_efgh(v0__)) == v4f_mask_z(v8f_efgh(v1__)));             \
    CHK(v4f_mask_w(v8f_efgh(v0__)) == v4f_mask_w(v8f_efgh(v1__)));             \
  } (void)0
#endif

/* Define macros generics to the SOA_DIMENSION parameter */
#if SOA_DIMENSION == 2
  #define soaXfY(Func) CONCAT(CONCAT(soaX, f2_), Func)
  #define SOA_VEC(Dst, X, Y, Z, W) CONCAT(soaX, f2)(Dst, X, Y)
#elif SOA_DIMENSION == 3
  #define soaXfY(Func) CONCAT(CONCAT(soaX, f3_), Func)
  #define SOA_VEC(Dst, X, Y, Z, W) CONCAT(soaX, f3)(Dst, X, Y, Z)
#elif SOA_DIMENSION == 4
  #define soaXfY(Func) CONCAT(CONCAT(soaX, f4_), Func)
  #define SOA_VEC(Dst, X, Y, Z, W) CONCAT(soaX, f4)(Dst, X, Y, Z, W)
#endif

/* Include the corresponding header */
#if SOA_SIMD_WIDTH == 4
  #if SOA_DIMENSION == 2
    #include "soa4f2.h"
  #elif SOA_DIMENSION == 3
    #include "soa4f3.h"
  #elif SOA_DIMENSION == 4
    #include "soa4f4.h"
  #endif
#else
  #if SOA_DIMENSION == 2
    #include "soa8f2.h"
  #elif SOA_DIMENSION == 3
    #include "soa8f3.h"
  #elif SOA_DIMENSION == 4
    #include "soa8f4.h"
  #endif
#endif

/* Define constants */
#define VXTRUE MASK(~0,~0,~0,~0,~0,~0,~0,~0)
#define VXFALSE MASK(0,0,0,0,0,0,0,0)

static void
CONCAT(CONCAT(CONCAT(test_, soaX), f), SOA_DIMENSION)(void)
{
  vXf_T a[SOA_DIMENSION], b[SOA_DIMENSION], c[SOA_DIMENSION];
  vXf_T v[4], f, tmp, mask;
  int i;

  v[0] = VEC(.5f, -1.f, -2.f, 3.f, -4.f, 5.f , 6.f , -7.f);
  v[1] = VEC(-8.f, 9.f, -10.f, 11.f, 12.f, -13.f, -14.f, -15.f);
  v[2] = VEC(16.f, -17.f, 18.f, -19.f, 20.f, 21.f, 22.f, -23.f);
  v[3] = VEC(0.6f, -0.1f, 0.8f, -0.9f, 0.02f, 0.1f,-0.22f, -0.3f);
  f = VEC(0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f);

  /* Setters */
  soaXfY(splat)(a, vXf(set1)(-1));
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(a[i], vXf(set1)(-1.f)), VXTRUE);
  }
  CHK(soaXfY(set)(b, a) == b);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(b[i], a[i]), VXTRUE);
  }
  CHK(SOA_VEC(a, v[0], v[1], v[2], v[3]) == a);
  CHK(soaXfY(set)(b, a) == b);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(a[i], v[i]), VXTRUE);
    CHKVX(vXf(eq)(b[i], v[i]), VXTRUE);
  }

  /* Unary operator */
  CHK(soaXfY(minus)(b, a) == b);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(a[i], v[i]), VXTRUE);
    CHKVX(vXf(eq)(b[i], vXf(minus)(v[i])), VXTRUE);
  }

  /* Regular binary operators */
  CHK(soaXfY(addf)(c, a, f) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(add)(a[i], f)), VXTRUE);
  CHK(soaXfY(subf)(c, a, f) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(sub)(a[i], f)), VXTRUE);
  CHK(soaXfY(mulf)(c, a, f) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(mul)(a[i], f)), VXTRUE);
  CHK(soaXfY(divf)(c, a, f) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(div)(a[i], f)), VXTRUE);
  CHK(soaXfY(add)(c, a, b) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(zero)()), VXTRUE);
  CHK(soaXfY(sub)(c, a, b) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(sub)(a[i], b[i])), VXTRUE);
  CHK(soaXfY(mul)(c, a, b) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(mul)(a[i], b[i])), VXTRUE);
  CHK(soaXfY(div)(c, a, b) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) CHKVX(vXf(eq)(c[i], vXf(div)(a[i], b[i])), VXTRUE);

  /* Linear interpolation */
  CHK(soaXfY(lerp)(c, a, b, f));
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(c[i], vXf(lerp)(a[i], b[i], f)), VXTRUE);
  }

  /* Sum operator */
  f = soaXfY(sum)(a);
  tmp = vXf(zero)();
  FOR_EACH(i, 0, SOA_DIMENSION) {
    tmp = vXf(add)(tmp, a[i]);
  }
  CHKVX(vXf(eq)(f, tmp), VXTRUE);

  /* Dot operator */
  f = soaXfY(dot)(a, b);
  tmp = vXf(zero)();
  FOR_EACH(i, 0, SOA_DIMENSION) {
    tmp = vXf(add)(tmp, vXf(mul(a[i], b[i])));
  }
  CHKVX(vXf(eq)(f, tmp), VXTRUE);

  /* Vector normalization functions */
  CHKVX(soaXfY(is_normalized)(a), VXFALSE);
  f = soaXfY(normalize)(c, a);
  CHKVX(vXf(eq)(soaXfY(len)(a), vXf(sqrt)(soaXfY(dot)(a, a))), VXTRUE);
  tmp = vXf(sqrt)(soaXfY(dot)(a, a));
  CHKVX(vXf(eq_eps)(f, vXf(sqrt)(soaXfY(dot)(a, a)), vXf(set1)(1.e-4f)), VXTRUE);
  CHKVX(soaXfY(is_normalized)(c), VXTRUE);
  CHKVX(vXf(eq_eps)(soaXfY(len)(c), vXf(set1)(1.f), vXf(set1)(1.e-4f)), VXTRUE);
  soaXfY(divf)(b, a, f);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq_eps)(b[i], c[i], vXf(set1)(1.e-4f)), VXTRUE);
  }

  /* Comparators */
  CHKVX(soaXfY(eq)(a, a), VXTRUE);
  CHKVX(soaXfY(eq)(a, b), VXFALSE);
  soaXfY(addf)(b, a, vXf(set1(1.e-4f)));
  CHKVX(soaXfY(eq)(a, b), VXFALSE);
  CHKVX(soaXfY(eq_eps)(a, b, vXf(set1)(1.e-3f)), VXTRUE);
  tmp = VEC(0, 0, 1.e-3f, 0, 0, 0, 1.e-3f, 1.e-3f);
  mask = MASK(0, 0, ~0, 0, 0, 0, ~0, ~0);
  CHKVX(soaXfY(eq_eps)(a, b, tmp), mask);

  /* Min/Max */
  CHK(soaXfY(min)(c, a, b) ==  c);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(c[i], vXf(min)(a[i], b[i])), VXTRUE);
  }
  CHK(soaXfY(max)(c, a, b) ==  c);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(c[i], vXf(max)(a[i], b[i])), VXTRUE);
  }

  /* Select */
  v[0] = MASK(0,0,~0,~0,0,~0,~0,0);
  v[1] = MASK(0,~0,~0,0,0,0,0,~0);
  v[2] = MASK(0, 0, 0,0,~0,~0,0, 0);
  v[3] = MASK(~0,~0,~0,0,~0,0,0,~0);
  CHK(soaXfY(sel)(c, b, a, v[0]) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(c[i], vXf(sel)(b[i], a[i], v[0])), VXTRUE);
  }
  CHK(soaXfY(selv)(c, b, a, v) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq)(c[i], vXf(sel)(b[i], a[i], v[i])), VXTRUE);
  }

  /* Cross product */
#if SOA_DIMENSION == 2
  v[0] = vXf(mul)(a[0], b[1]);
  v[1] = vXf(mul)(a[1], b[0]);
  tmp = vXf(sub)(v[0], v[1]);
  f = soaXfY(cross)(a, b);
  CHKVX(vXf(eq_eps)(f, tmp, vXf(set1)(1.e-6f)), VXTRUE);
#elif SOA_DIMENSION == 3
  v[0] = vXf(sub)(vXf(mul)(a[1], b[2]), vXf(mul)(a[2], b[1]));
  v[1] = vXf(sub)(vXf(mul)(a[2], b[0]), vXf(mul)(a[0], b[2]));
  v[2] = vXf(sub)(vXf(mul)(a[0], b[1]), vXf(mul)(a[1], b[0]));
  CHK(soaXfY(cross)(c, a, b) == c);
  FOR_EACH(i, 0, SOA_DIMENSION) {
    CHKVX(vXf(eq_eps)(c[i], v[i], vXf(set1)(1.e-6f)), VXTRUE);
  }
#endif
}

/* Generic parameters */
#undef SOA_SIMD_WIDTH
#undef SOA_DIMENSION

/* Macros generic to the SOA_SIMD_WIDTH parameter */
#undef soaX
#undef vXf
#undef vXf_T
#undef VEC
#undef MASK
#undef CHKVX

/* Macros generic to the SOA_DIMENSION parameter */
#undef soaXfY
#undef SOA_VEC

/* Constants */
#undef VXTRUE
#undef VXFALSE
