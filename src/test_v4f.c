/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"
#include "math.h"

int
main(int argc, char** argv)
{
  union { int32_t i[4]; float f[4]; } cast;
  v4f_T i, j, k;
  v4i_T l;
  ALIGN(16) float tmp[5] = { 0.f, 1.f, 2.f, 3.f, 4.f };
  (void)argc, (void)argv;

  i = v4f_loadu(tmp+1);
  CHK(v4f_x(i) == 1.f);
  CHK(v4f_y(i) == 2.f);
  CHK(v4f_z(i) == 3.f);
  CHK(v4f_w(i) == 4.f);

  i = v4f_loadu3(tmp);
  CHK(v4f_x(i) == 0.f);
  CHK(v4f_y(i) == 1.f);
  CHK(v4f_z(i) == 2.f);

  i = v4f_load(tmp);
  CHK(v4f_x(i) == 0.f);
  CHK(v4f_y(i) == 1.f);
  CHK(v4f_z(i) == 2.f);
  CHK(v4f_w(i) == 3.f);

  tmp[0] = tmp[1] = tmp[2] = tmp[3] = 0.f;
  CHK(v4f_store(tmp, i) == tmp);
  CHK(tmp[0] == 0.f);
  CHK(tmp[1] == 1.f);
  CHK(tmp[2] == 2.f);
  CHK(tmp[3] == 3.f);

  i = v4f_set(1.f, 2.f, 3.f, 4.f);
  CHK(v4f_x(i) == 1.f);
  CHK(v4f_y(i) == 2.f);
  CHK(v4f_z(i) == 3.f);
  CHK(v4f_w(i) == 4.f);

  i = v4f_set1(-2.f);
  CHK(v4f_x(i) == -2.f);
  CHK(v4f_y(i) == -2.f);
  CHK(v4f_z(i) == -2.f);
  CHK(v4f_w(i) == -2.f);

  i = v4f_zero();
  CHK(v4f_x(i) == 0.f);
  CHK(v4f_y(i) == 0.f);
  CHK(v4f_z(i) == 0.f);
  CHK(v4f_w(i) == 0.f);

  i = v4f_mask(~0, 0, ~0, ~0);
  cast.f[0] = v4f_x(i); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(i); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(i); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(i); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);

  i = v4f_mask1(8);
  cast.f[0] = v4f_x(i); CHK(cast.i[0] == 8);
  cast.f[1] = v4f_y(i); CHK(cast.i[1] == 8);
  cast.f[2] = v4f_z(i); CHK(cast.i[2] == 8);
  cast.f[3] = v4f_w(i); CHK(cast.i[3] == 8);

  i = v4f_true();
  cast.f[0] = v4f_x(i); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(i); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(i); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(i); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);

  i = v4f_false();
  cast.f[0] = v4f_x(i); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(i); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(i); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(i); CHK(cast.i[3] == (int32_t)0x00000000);

  i = v4f_mask(~0, 0, ~0, ~0);
  j = v4f_mask(0, 0, 0, ~0);
  k = v4f_or(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);
  CHK(v4f_mask_x(i) == ~0);
  CHK(v4f_mask_y(i) == 0);
  CHK(v4f_mask_z(i) == ~0);
  CHK(v4f_mask_w(i) == ~0);

  k = v4f_and(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);

  k = v4f_xor(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0x00000000);

  i = v4f_mask(0x00010203, 0x04050607, 0x08090A0B, 0x0C0D0E0F);
  j = v4f_mask(0x01020401, 0x70605040, 0x0F1F2F3F, 0x00000000);
  k = v4f_andnot(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == 0x01020400);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == 0x70605040);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == 0x07162534);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == 0x00000000);

  CHK(v4f_movemask(i) == 0);
  i = v4f_mask(0x01020401, (int32_t)0x80605040, 0x7F1F2F3F, 0x00000000);
  CHK(v4f_movemask(i) == 2);

  i = v4f_set(1.f, 2.f, 3.f, 4.f);
  j = v4f_set(5.f, 6.f, 7.f, 8.f);
  k = v4f_sel(i, j, v4f_mask(~0, 0, 0, ~0));
  CHK(v4f_x(k) == 5.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_xayb(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 5.f);
  CHK(v4f_z(k) == 2.f);
  CHK(v4f_w(k) == 6.f);

  k = v4f_xyab(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 5.f);
  CHK(v4f_w(k) == 6.f);

  k = v4f_zcwd(i, j);
  CHK(v4f_x(k) == 3.f);
  CHK(v4f_y(k) == 7.f);
  CHK(v4f_z(k) == 4.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_zwcd(i, j);
  CHK(v4f_x(k) == 3.f);
  CHK(v4f_y(k) == 4.f);
  CHK(v4f_z(k) == 7.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_ayzw(i, j);
  CHK(v4f_x(k) == 5.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 4.f);

  k = v4f_xycd(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 7.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_ywbd(i, j);
  CHK(v4f_x(k) == 2.f);
  CHK(v4f_y(k) == 4.f);
  CHK(v4f_z(k) == 6.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_xbzw(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 6.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 4.f);

  k = v4f_xycw(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 7.f);
  CHK(v4f_w(k) == 4.f);

  k = v4f_xyzd(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_048C(v4f_set1(1.f), v4f_set1(2.f), v4f_set1(3.f), v4f_set1(4.f));
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 4.f);

  i = v4f_set(-1.f, 2.f, -3.f, 4.f);
  j = v4f_minus(i);
  CHK(v4f_x(j) == 1.f);
  CHK(v4f_y(j) == -2.f);
  CHK(v4f_z(j) == 3.f);
  CHK(v4f_w(j) == -4.f);

  k = v4f_add(i, j);
  CHK(v4f_x(k) == 0.f);
  CHK(v4f_y(k) == 0.f);
  CHK(v4f_z(k) == 0.f);
  CHK(v4f_w(k) == 0.f);

  k = v4f_sub(i, j);
  CHK(v4f_x(k) == -2.f);
  CHK(v4f_y(k) == 4.f);
  CHK(v4f_z(k) == -6.f);
  CHK(v4f_w(k) == 8.f);

  k = v4f_mul(i, j);
  CHK(v4f_x(k) == -1.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == -9.f);
  CHK(v4f_w(k) == -16.f);

  k = v4f_div(k, i);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == -2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == -4.f);

  k = v4f_madd(i, j, k);
  CHK(v4f_x(k) == 0.f);
  CHK(v4f_y(k) == -6.f);
  CHK(v4f_z(k) == -6.f);
  CHK(v4f_w(k) == -20.f);

  k = v4f_abs(i);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 4.f);

  i = v4f_set(4.f, 9.f, 16.f, 25.f);
  k = v4f_sqrt(i);
  CHK(v4f_x(k) == 2.f);
  CHK(v4f_y(k) == 3.f);
  CHK(v4f_z(k) == 4.f);
  CHK(v4f_w(k) == 5.f);

  k = v4f_rsqrte(i);
  CHK(eq_eps(v4f_x(k), 1.f/2.f, 1.e-3f) == 1);
  CHK(eq_eps(v4f_y(k), 1.f/3.f, 1.e-3f) == 1);
  CHK(eq_eps(v4f_z(k), 1.f/4.f, 1.e-3f) == 1);
  CHK(eq_eps(v4f_w(k), 1.f/5.f, 1.e-3f) == 1);

  k = v4f_rsqrt(i);
  CHK(eq_eps(v4f_x(k), 1.f/2.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), 1.f/3.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), 1.f/4.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(k), 1.f/5.f, 1.e-6f) == 1);

  k = v4f_rcpe(i);
  CHK(eq_eps(v4f_x(k), 1.f/4.f, 1.e-3f) == 1);
  CHK(eq_eps(v4f_y(k), 1.f/9.f, 1.e-3f) == 1);
  CHK(eq_eps(v4f_z(k), 1.f/16.f, 1.e-3f) == 1);
  CHK(eq_eps(v4f_w(k), 1.f/25.f, 1.e-3f) == 1);

  k = v4f_rcp(i);
  CHK(eq_eps(v4f_x(k), 1.f/4.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), 1.f/9.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), 1.f/16.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(k), 1.f/25.f, 1.e-6f) == 1);

	i = v4f_set(0.f, 1.f, 2.f, 4.f);
	j = v4f_set(1.f, 2.f, -1.f, 1.f);
	k = v4f_lerp(i, j, v4f_set1(0.5f));
  CHK(v4f_x(k) == 0.5f);
  CHK(v4f_y(k) == 1.5f);
  CHK(v4f_z(k) == 0.5f);
  CHK(v4f_w(k) == 2.5f);

  k = v4f_sum(j);
  CHK(v4f_x(k) == 3.f);
  CHK(v4f_y(k) == 3.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 3.f);

  k = v4f_dot(i, j);
  CHK(v4f_x(k) == 4.f);
  CHK(v4f_y(k) == 4.f);
  CHK(v4f_z(k) == 4.f);
  CHK(v4f_w(k) == 4.f);

  k = v4f_len(i);
  CHK(eq_eps(v4f_x(k), (float)sqrt(21.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), (float)sqrt(21.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), (float)sqrt(21.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(k), (float)sqrt(21.0), 1.e-6f) == 1);

  i = v4f_set(0.f, 4.f, 2.f, 3.f);
  k = v4f_normalize(i);
  CHK(eq_eps(v4f_x(k), 0.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), 0.742781353f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), 0.371390676f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(k), 0.557086014f, 1.e-6f) == 1);

  i = v4f_set(1.f, 4.f, 2.f, 3.f);
  k = v4f_sum2(i);
  CHK(v4f_x(k) == 5.f);
  CHK(v4f_y(k) == 5.f);
  CHK(v4f_z(k) == 5.f);
  CHK(v4f_w(k) == 5.f);

  j = v4f_set(2.f, 3.f, 5.f, 1.f);
  k = v4f_dot2(i, j);
  CHK(v4f_x(k) == 14.f);
  CHK(v4f_y(k) == 14.f);
  CHK(v4f_z(k) == 14.f);
  CHK(v4f_w(k) == 14.f);

  k = v4f_len2(i);
  CHK(eq_eps(v4f_x(k), (float)sqrt(17.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), (float)sqrt(17.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), (float)sqrt(17.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(k), (float)sqrt(17.0), 1.e-6f) == 1);

  i = v4f_set(1.f, -2.f, 2.f, 5.f);
  j = v4f_set(3.f, 1.f, 1.f, 5.f);
  k = v4f_cross2(i, j);
  CHK(v4f_x(k) == 7.f);
  CHK(v4f_y(k) == 7.f);
  CHK(v4f_z(k) == 7.f);
  CHK(v4f_w(k) == 7.f);

  k = v4f_cross2(j, i);
  CHK(v4f_x(k) == -7.f);
  CHK(v4f_y(k) == -7.f);
  CHK(v4f_z(k) == -7.f);
  CHK(v4f_w(k) == -7.f);

  i = v4f_set(0.f, 4.f, 5.f, 7.f);
  k = v4f_normalize2(i);
  CHK(eq_eps(v4f_x(k), 0.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), 1.f, 1.e-6f) == 1);

  k = v4f_sum3(i);
  CHK(v4f_x(k) == 9.f);
  CHK(v4f_y(k) == 9.f);
  CHK(v4f_z(k) == 9.f);
  CHK(v4f_w(k) == 9.f);

  i = v4f_set(2.f, 3.f, 2.f, 4.f);
  j = v4f_set(0.f, 4.f, 2.f, 19.f);
  k = v4f_dot3(i, j);
  CHK(v4f_x(k) == 16.f);
  CHK(v4f_y(k) == 16.f);
  CHK(v4f_z(k) == 16.f);
  CHK(v4f_w(k) == 16.f);

  k = v4f_len3(j);
  CHK(eq_eps(v4f_x(k), (float)sqrt(20.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), (float)sqrt(20.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), (float)sqrt(20.0), 1.e-6f) == 1);
  CHK(eq_eps(v4f_w(k), (float)sqrt(20.0), 1.e-6f) == 1);

  k = v4f_normalize3(j);
  CHK(eq_eps(v4f_x(k), 0.f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_y(k), 0.8944271910f, 1.e-6f) == 1);
  CHK(eq_eps(v4f_z(k), 0.4472135995f, 1.e-6f) == 1);

  i = v4f_set(1.f, -2.f, 2.f, 4.f);
  j = v4f_set(3.f, 1.f, -1.5f, 2.f);
  k = v4f_cross3(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 7.5f);
  CHK(v4f_z(k) == 7.f);

  i = v4f_set(1.f, 2.f, 3.f, 4.f);
  j = v4f_set(-2.f, -4.f, 3.f, 6.f);
  k = v4f_eq(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0x00000000);

  k = v4f_neq(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);

  k = v4f_gt(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0x00000000);

  k = v4f_lt(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0x00000000);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);

  k = v4f_ge(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0xFFFFFFFF);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0x00000000);

  k = v4f_le(i, j);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0x00000000);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0xFFFFFFFF);

  i = v4f_set(1.01f, 2.01f, 3.02f, 0.02f);
  j = v4f_set(1.f, 2.f, 3.f, 0.f);
  k = v4f_set(0.f, 0.01f, 0.02f, 0.f);
  k = v4f_eq_eps(i, j, k);
  cast.f[0] = v4f_x(k); CHK(cast.i[0] == (int32_t)0x00000000);
  cast.f[1] = v4f_y(k); CHK(cast.i[1] == (int32_t)0xFFFFFFFF);
  cast.f[2] = v4f_z(k); CHK(cast.i[2] == (int32_t)0xFFFFFFFF);
  cast.f[3] = v4f_w(k); CHK(cast.i[3] == (int32_t)0x00000000);

  i = v4f_set(1.f, 2.f, 3.f, 4.f);
  j = v4f_set(-2.f, -4.f, 3.f, 6.f);
  k = v4f_min(i, j);
  CHK(v4f_x(k) == -2.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 4.f);

  k = v4f_max(i, j);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 6.f);

  k = v4f_reduce_min(i);
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 1.f);
  CHK(v4f_z(k) == 1.f);
  CHK(v4f_w(k) == 1.f);

  k = v4f_reduce_min(j);
  CHK(v4f_x(k) == -4.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == -4.f);
  CHK(v4f_w(k) == -4.f);

  k = v4f_reduce_max(i);
  CHK(v4f_x(k) == 4.f);
  CHK(v4f_y(k) == 4.f);
  CHK(v4f_z(k) == 4.f);
  CHK(v4f_w(k) == 4.f);

  k = v4f_reduce_max(j);
  CHK(v4f_x(k) == 6.f);
  CHK(v4f_y(k) == 6.f);
  CHK(v4f_z(k) == 6.f);
  CHK(v4f_w(k) == 6.f);

  k = v4f_clamp(i, v4f_set(0.f, 0.f, -1.f, 3.f), v4f_set(1.f, 3.f, 2.f, 3.1f));
  CHK(v4f_x(k) == 1.f);
  CHK(v4f_y(k) == 2.f);
  CHK(v4f_z(k) == 2.f);
  CHK(v4f_w(k) == 3.1f);

  l = v4f_to_v4i(j);
  CHK(v4i_x(l) == -2);
  CHK(v4i_y(l) == -4);
  CHK(v4i_z(l) == 3);
  CHK(v4i_w(l) == 6);

  k = v4i_to_v4f(l);
  CHK(v4f_x(k) == -2.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == 6.f);

  i = v4f_set(1.5f, 2.51f, 3.2f, 4.35f);
  l = v4f_to_v4i(i);
  CHK(v4i_x(l) == 2);
  CHK(v4i_y(l) == 3);
  CHK(v4i_z(l) == 3);
  CHK(v4i_w(l) == 4);

  l = v4f_trunk_v4i(i);
  CHK(v4i_x(l) == 1);
  CHK(v4i_y(l) == 2);
  CHK(v4i_z(l) == 3);
  CHK(v4i_w(l) == 4);

  cast.f[0] = 1.f;
  cast.f[1] = 2.f;
  cast.f[2] = 3.14f;
  cast.f[3] = -9.2f;

  i = v4f_set(cast.f[0], cast.f[1], cast.f[2], cast.f[3]);
  l = v4f_rcast_v4i(i);
  CHK(v4i_x(l) == cast.i[0]);
  CHK(v4i_y(l) == cast.i[1]);
  CHK(v4i_z(l) == cast.i[2]);
  CHK(v4i_w(l) == cast.i[3]);

  i = v4i_rcast_v4f(l);
  CHK(v4f_x(i) == cast.f[0]);
  CHK(v4f_y(i) == cast.f[1]);
  CHK(v4f_z(i) == cast.f[2]);
  CHK(v4f_w(i) == cast.f[3]);

  k = v4f_xxxx(j);
  CHK(v4f_x(k) == -2.f);
  CHK(v4f_y(k) == -2.f);
  CHK(v4f_z(k) == -2.f);
  CHK(v4f_w(k) == -2.f);

  k = v4f_yyxx(j);
  CHK(v4f_x(k) == -4.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == -2.f);
  CHK(v4f_w(k) == -2.f);

  k = v4f_wwxy(j);
  CHK(v4f_x(k) == 6.f);
  CHK(v4f_y(k) == 6.f);
  CHK(v4f_z(k) == -2.f);
  CHK(v4f_w(k) == -4.f);

  k = v4f_zyzy(j);
  CHK(v4f_x(k) == 3.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == 3.f);
  CHK(v4f_w(k) == -4.f);

  k = v4f_wyyz(j);
  CHK(v4f_x(k) == 6.f);
  CHK(v4f_y(k) == -4.f);
  CHK(v4f_z(k) == -4.f);
  CHK(v4f_w(k) == 3.f);

  i = v4f_xyz_to_rthetaphi(v4f_set(10.f, 5.f, 3.f, 0.f));
  CHK(eq_eps(v4f_x(i), 11.575836f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.308643f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 0.463647f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(8.56f, 7.234f, 33.587f, 0.f));
  CHK(eq_eps(v4f_x(i), 35.407498f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 0.322063f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 0.701638f, 1.e-5f) == 1);

  i = v4f_xyz_to_rthetaphi(v4f_set(0.f, 0.f, 0.f, 0.f));
  CHK(eq_eps(v4f_x(i), 0.f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 0.f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 0.f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, 0.f, 0.f, 0.f));
  CHK(eq_eps(v4f_x(i), 4.53f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.570796f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 0.f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(0.f, 7.2f, 0.f, 0.f));
  CHK(eq_eps(v4f_x(i), 7.2f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.570796f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 1.570796f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, 7.2f, 0.f, 0.f));
  CHK(eq_eps(v4f_x(i), 8.506521f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.570796f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 1.009206f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(0.f, 0.f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 3.1f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 0.f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 0.f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, 0.f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 5.489162f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 0.970666f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 0.f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(0.f, 7.2f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 7.839005f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.164229f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 1.570796f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, 7.2f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.221327f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 1.009206f, 1.e-5f) == 1);

  i = v4f_xyz_to_rthetaphi(v4f_set(-4.53f, 7.2f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.221327f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 2.132386f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(-4.53f, -7.2f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.221327f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), -2.132386f, 1.e-5f) 
   || eq_eps(v4f_z(i), 2*PI - 2.132386f, 1.e-5f));
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, -7.2f, 3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.221327f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), -1.009206f, 1.e-5f) 
   || eq_eps(v4f_z(i), 2*PI - 1.009206f, 1.e-5f));
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, 7.2f, -3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.920264f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 1.009206f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(-4.53f, 7.2f, -3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.920264f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), 2.132386f, 1.e-5f) == 1);
  i = v4f_xyz_to_rthetaphi(v4f_set(4.53f, -7.2f, -3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.920264f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), -1.009206f, 1.e-5f) 
   || eq_eps(v4f_z(i), 2*PI - 1.009206f, 1.e-5f));
  i = v4f_xyz_to_rthetaphi(v4f_set(-4.53f, -7.2f, -3.1f, 0.f));
  CHK(eq_eps(v4f_x(i), 9.053778f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_y(i), 1.920264f, 1.e-5f) == 1);
  CHK(eq_eps(v4f_z(i), -2.132386f, 1.e-5f)
   || eq_eps(v4f_z(i), 2*PI - 2.132386f, 1.e-5f));
  return 0;
}

