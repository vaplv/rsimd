/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "aosf44.h"

v4f_T
aosf44_inverse(v4f_T res[4], const v4f_T m[4])
{
  v4f_T c0, c1, c2, c3, r3;
  v4f_T f33_023_c0, f33_023_c1, f33_023_c2, f33_023_c3;
  v4f_T f33_123_c0, f33_123_c1, f33_123_c2, f33_123_c3;
  v4f_T f33_013_c0, f33_013_c1, f33_013_c2, f33_013_c3;
  v4f_T f33_012_012[3], f33_012_013[3], f33_012_023[3], f33_012_123[3];
  v4f_T f33_023_012[3], f33_023_013[3], f33_023_023[3], f33_023_123[3];
  v4f_T f33_123_012[3], f33_123_013[3], f33_123_023[3], f33_123_123[3];
  v4f_T f33_013_012[3], f33_013_013[3], f33_013_023[3], f33_013_123[3];
  v4f_T det_012, det_023, det_123, det_013;
  v4f_T cofacts, det, idet, mpmp_idet, pmpm_idet;
  ASSERT(res && m);

  /* Retrieve the columns 0, 1, 2 and 3 and the row 3 of the "m" matrix. */
  c0 = m[0];
  c1 = m[1];
  c2 = m[2];
  c3 = m[3];
  r3 = aosf44_row3(m);

  /* Define the 3x3 sub-matrix and compute their determinant */
  aosf33_set(f33_012_012, c0, c1, c2);
  aosf33_set(f33_012_013, c0, c1, c3);
  aosf33_set(f33_012_023, c0, c2, c3);
  aosf33_set(f33_012_123, c1, c2, c3);
  det_012 = v4f_048C
    (aosf33_det(f33_012_123),
     aosf33_det(f33_012_023),
     aosf33_det(f33_012_013),
     aosf33_det(f33_012_012));

  f33_023_c0 = v4f_xzww(c0);
  f33_023_c1 = v4f_xzww(c1);
  f33_023_c2 = v4f_xzww(c2);
  f33_023_c3 = v4f_xzww(c3);
  aosf33_set(f33_023_012, f33_023_c0, f33_023_c1, f33_023_c2);
  aosf33_set(f33_023_013, f33_023_c0, f33_023_c1, f33_023_c3);
  aosf33_set(f33_023_023, f33_023_c0, f33_023_c2, f33_023_c3);
  aosf33_set(f33_023_123, f33_023_c1, f33_023_c2, f33_023_c3);
  det_023 = v4f_048C
    (aosf33_det(f33_023_123),
     aosf33_det(f33_023_023),
     aosf33_det(f33_023_013),
     aosf33_det(f33_023_012));

  f33_123_c0 = v4f_yzww(c0);
  f33_123_c1 = v4f_yzww(c1);
  f33_123_c2 = v4f_yzww(c2);
  f33_123_c3 = v4f_yzww(c3);
  aosf33_set(f33_123_012, f33_123_c0, f33_123_c1, f33_123_c2);
  aosf33_set(f33_123_013, f33_123_c0, f33_123_c1, f33_123_c3);
  aosf33_set(f33_123_023, f33_123_c0, f33_123_c2, f33_123_c3);
  aosf33_set(f33_123_123, f33_123_c1, f33_123_c2, f33_123_c3);
  det_123 = v4f_048C
    (aosf33_det(f33_123_123),
     aosf33_det(f33_123_023),
     aosf33_det(f33_123_013),
     aosf33_det(f33_123_012));

  f33_013_c0 = v4f_xyww(c0);
  f33_013_c1 = v4f_xyww(c1);
  f33_013_c2 = v4f_xyww(c2);
  f33_013_c3 = v4f_xyww(c3);
  aosf33_set(f33_013_012, f33_013_c0, f33_013_c1, f33_013_c2);
  aosf33_set(f33_013_013, f33_013_c0, f33_013_c1, f33_013_c3);
  aosf33_set(f33_013_023, f33_013_c0, f33_013_c2, f33_013_c3);
  aosf33_set(f33_013_123, f33_013_c1, f33_013_c2, f33_013_c3);
  det_013 = v4f_048C
    (aosf33_det(f33_013_123),
     aosf33_det(f33_013_023),
     aosf33_det(f33_013_013),
     aosf33_det(f33_013_012));

  /* Compute the cofactors of the column 3 */
  cofacts = v4f_mul(det_012, v4f_set(-1.f, 1.f, -1.f, 1.f));

  /* Compute the determinant of the "m" matrix */
  det = v4f_dot(cofacts, r3);

  /* Invert the matrix */
  idet = v4f_rcp(det);
  mpmp_idet = v4f_xor
    (idet, v4f_mask((int32_t)0x80000000, 0, (int32_t)0x80000000, 0));
  pmpm_idet = v4f_xor
    (idet, v4f_mask(0, (int32_t)0x80000000, 0, (int32_t)0x80000000));
  res[0] = v4f_mul(det_123, pmpm_idet);
  res[1] = v4f_mul(det_023, mpmp_idet);
  res[2] = v4f_mul(det_013, pmpm_idet);
  res[3] = v4f_mul(det_012, mpmp_idet);

  return det;
}

