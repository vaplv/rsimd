/* Copyright (C) 2013-2021 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "rsimd.h"
#include "math.h"

#include <math.h>

#define LOG2E 1.4426950408889634074 /* log_2 e */
#define LN10 2.30258509299404568402 /* log_e 10 */

#define CHKV4_EPS(V, Ref, Eps) {                                               \
  CHK(eq_eps(v4f_x(V), Ref[0], fabsf(Ref[0]) * Eps));                          \
  CHK(eq_eps(v4f_y(V), Ref[1], fabsf(Ref[1]) * Eps));                          \
  CHK(eq_eps(v4f_z(V), Ref[2], fabsf(Ref[2]) * Eps));                          \
  CHK(eq_eps(v4f_w(V), Ref[3], fabsf(Ref[3]) * Eps));                          \
} (void)0

#define CHKV4_FUNC_EPS(V, Func, Eps) {                                         \
  const v4f_T r__ = v4f_##Func(V);                                             \
  float ref__[4];                                                              \
  ref__[0] = (float)Func(v4f_x(V));                                            \
  ref__[1] = (float)Func(v4f_y(V));                                            \
  ref__[2] = (float)Func(v4f_z(V));                                            \
  ref__[3] = (float)Func(v4f_w(V));                                            \
  CHKV4_EPS(r__, ref__, Eps);                                                  \
} (void)0

static void
test_trigo(void)
{
  v4f_T i, j, k;
  float ref[4];

  i = v4f_set((float)PI/2.f, (float)PI/3.f, (float)PI/4.f, (float)PI/6.f);

  CHKV4_FUNC_EPS(i, cos, 1.e-6f);
  CHKV4_FUNC_EPS(i, sin, 1.e-6f);

  v4f_sincos(i, &k, &j);
  ref[0] = (float)sin(v4f_x(i));
  ref[1] = (float)sin(v4f_y(i));
  ref[2] = (float)sin(v4f_z(i));
  ref[3] = (float)sin(v4f_w(i));
  CHKV4_EPS(k, ref, 1.e-6f);
  ref[0] = (float)cos(v4f_x(i));
  ref[1] = (float)cos(v4f_y(i));
  ref[2] = (float)cos(v4f_z(i));
  ref[3] = (float)cos(v4f_w(i));
  CHKV4_EPS(j, ref, 1.e-6f);

  i = v4f_set((float)PI/8.f, (float)PI/3.f, (float)PI/4.f, (float)PI/6.f);
  CHKV4_FUNC_EPS(i, tan, 1.e-6f);
  CHKV4_FUNC_EPS(v4f_cos(i), acos, 1.e-6f);
  CHKV4_FUNC_EPS(v4f_sin(i), asin, 1.e-6f);
  CHKV4_FUNC_EPS(v4f_tan(i), atan, 1.e-6f);
}

static void
test_exp(void)
{
  const v4f_T i = v4f_set(1.f, -1.234f, 0.f, 3.14156f);
  v4f_T j;
  float ref[4];

  CHKV4_FUNC_EPS(i, exp, 1.e-6f);
  CHKV4_FUNC_EPS(i, exp2, 1.e-6f);

  j = v4f_exp10(i);
  ref[0] = (float)exp2(LOG2E * LN10 * v4f_x(i));
  ref[1] = (float)exp2(LOG2E * LN10 * v4f_y(i));
  ref[2] = (float)exp2(LOG2E * LN10 * v4f_z(i));
  ref[3] = (float)exp2(LOG2E * LN10 * v4f_w(i));
  CHKV4_EPS(j, ref, 1.e-6f);
}

static void
test_log(void)
{
  const v4f_T i = v4f_set(4.675f, 3.14f, 9.99999f, 1.234e-13f);

  CHKV4_FUNC_EPS(i, log, 1.e-6f);
  CHKV4_FUNC_EPS(i, log2, 1.e-6f);
  CHKV4_FUNC_EPS(i, log10, 1.e-6f);
}

static void
test_misc(void)
{
  v4f_T i, j, k;
  float ref[4];

  i = v4f_set(-1.2345f, 9.3e-7f, 3.879e9f, -10.56f);
  j = v4f_set(7.89e-9f, 0.12f, -4.9e10f, 3.14f);
  k = v4f_copysign(i, j);
  ref[0] = (float)copysign(v4f_x(i), v4f_x(j));
  ref[1] = (float)copysign(v4f_y(i), v4f_y(j));
  ref[2] = (float)copysign(v4f_z(i), v4f_z(j));
  ref[3] = (float)copysign(v4f_w(i), v4f_w(j));
  CHKV4_EPS(k, ref, 1.e-6f);

  CHKV4_FUNC_EPS(i, floor, 1.e-6f);

  k = v4f_pow(v4f_abs(i), j);
  ref[0] = (float)pow(fabsf(v4f_x(i)), v4f_x(j));
  ref[1] = (float)pow(fabsf(v4f_y(i)), v4f_y(j));
  ref[2] = (float)pow(fabsf(v4f_z(i)), v4f_z(j));
  ref[3] = (float)pow(fabsf(v4f_w(i)), v4f_w(j));
  CHKV4_EPS(k, ref, 1.e-6f);
}

int
main(int argc, char** argv)
{
  (void)argc, (void)argv;

  test_trigo();
  test_exp();
  test_log();
  test_misc();

  return 0;
}

