/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"

/* This file can be included once */
#ifdef VXF_BEGIN_H
  #error "The vXf_begin.h header is already included"
#endif
#define VXF_BEGIN_H

/* Check parameter */
#if !defined(RSIMD_WIDTH__)
  #error "Undefined RSIMD_WIDTH__ macro"
#endif
#if RSIMD_WIDTH__ != 4 && RSIMD_WIDTH__ != 8
  #error "Unexpected RSIMD_WIDTH__ value of "STR(RSIMD_WIDTH__)
#endif

/* Check that internal macros are not already defined */
#if defined(RSIMD_vXf__)                                                       \
 || defined(RSIMD_vXf_T__)                                                     \
 || defined(RSIMD_Sleef__)                                                     \
 || defined(RSIMD_Sleef_ULP__)                                                 \
 || defined(RSIMD_Sleef_vecf__)
 #error "Unexpected macro definition"
#endif

/* Macros generic to RSIMD_WIDTH__ */
#define RSIMD_vXf__(Func) \
  CONCAT(CONCAT(CONCAT(CONCAT(v, RSIMD_WIDTH__), f), _), Func)
#define RSIMD_vXf_T__ CONCAT(CONCAT(v, RSIMD_WIDTH__), f_T)

/* Sleef macros */
#define RSIMD_Sleef__(Func) CONCAT(CONCAT(Sleef_, Func), RSIMD_WIDTH__)
#define RSIMD_Sleef_ULP__(Func, Suffix) \
   CONCAT(CONCAT(CONCAT(CONCAT(Sleef_, Func), RSIMD_WIDTH__), _), Suffix)

/* Vector types of the Sleef library */
#if RSIMD_WIDTH__ == 4
  #define RSIMD_Sleef_vecf__(Dim) CONCAT(Sleef___m128_, Dim)
#elif RSIMD_WIDTH__ == 8
  #define RSIMD_Sleef_vecf__(Dim) CONCAT(Sleef___m256_, Dim)
#endif

