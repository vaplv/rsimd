/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RSIMD_H
#define RSIMD_H

#include <rsys/rsys.h>

#if defined(RSIMD_SHARED_BUILD)
  #define RSIMD_API extern EXPORT_SYM
#elif defined(RSIMD_STATIC_BUILD)
  #define RSIMD_API extern LOCAL_SYM
#else
  #define RSIMD_API extern IMPORT_SYM
#endif

#ifdef SIMD_SSE2
  #include "sse/sse.h"
#endif
#ifdef SIMD_AVX
  #include "avx/avx.h"
#endif

#endif /* RSIMD_H */
