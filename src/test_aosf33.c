/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "aosf33.h"
#include <rsys/float33.h>

#define AOSF33_EQ_EPS(M, A, B, C, D, E, F, G, H, I, Eps)                       \
  {                                                                            \
    float a[9], b[9];                                                          \
    b[0] = (A); b[1] = (B); b[2] = (C);                                        \
    b[3] = (D); b[4] = (E); b[5] = (F);                                        \
    b[6] = (G); b[7] = (H); b[8] = (I);                                        \
    CHK(f33_eq_eps(aosf33_store(a, (M)), b, Eps) == 1);                        \
  } (void)0
#define AOSF33_EQ(M, A, B, C, D, E, F, G, H, I)                                \
  AOSF33_EQ_EPS(M, A, B, C, D, E, F, G, H, I, 0.f)

int
main(int argc, char** argv)
{
  float tmp[9];
  v4f_T m[3], n[3], o[3], v;
  (void)argc, (void)argv;

  CHK(aosf33_set(m,
    v4f_set(0.f, 1.f, 2.f, 0.f),
    v4f_set(3.f, 4.f, 5.f, 0.f),
    v4f_set(6.f, 7.f, 8.f, 0.f)) == m);
  CHK(aosf33_store(tmp, m) == tmp);
  CHK(tmp[0] == 0.f);
  CHK(tmp[1] == 1.f);
  CHK(tmp[2] == 2.f);
  CHK(tmp[3] == 3.f);
  CHK(tmp[4] == 4.f);
  CHK(tmp[5] == 5.f);
  CHK(tmp[6] == 6.f);
  CHK(tmp[7] == 7.f);
  CHK(tmp[8] == 8.f);
  AOSF33_EQ(m, 0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f);
  CHK(aosf33_identity(m) == m);
  AOSF33_EQ(m, 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 1.f);
  CHK(aosf33_zero(m) == m);
  AOSF33_EQ(m, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);

  f3(tmp+0, -1.f, -2.f, -3.f);
  f3(tmp+3, -4.f, -5.f, -6.f);
  f3(tmp+6, -7.f, -8.f, -9.f);
  CHK(aosf33_load(m, tmp) == m);
  AOSF33_EQ(m, -1.f, -2.f, -3.f, -4.f, -5.f, -6.f, -7.f, -8.f, -9.f);

  CHK(aosf33_zero(m) == m);
  CHK(aosf33_set_row0(m, v4f_set(0.f, 1.f, 2.f, 9.f)) == m);
  AOSF33_EQ(m, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 2.f, 0.f, 0.f);
  CHK(aosf33_set_row1(m, v4f_set(3.f, 4.f, 5.f, 10.f)) == m);
  AOSF33_EQ(m, 0.f, 3.f, 0.f, 1.f, 4.f, 0.f, 2.f, 5.f, 0.f);
  CHK(aosf33_set_row2(m, v4f_set(6.f, 7.f, 8.f, 11.f)) == m);
  AOSF33_EQ(m, 0.f, 3.f, 6.f, 1.f, 4.f, 7.f, 2.f, 5.f, 8.f);

  CHK(aosf33_zero(m) == m);
  CHK(aosf33_set_row(m, v4f_set(0.f, 1.f, 2.f, 9.f), 0) == m);
  AOSF33_EQ(m, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 2.f, 0.f, 0.f);
  CHK(aosf33_set_row(m, v4f_set(3.f, 4.f, 5.f, 10.f), 1) == m);
  AOSF33_EQ(m, 0.f, 3.f, 0.f, 1.f, 4.f, 0.f, 2.f, 5.f, 0.f);
  CHK(aosf33_set_row(m, v4f_set(6.f, 7.f, 8.f, 11.f), 2) == m);
  AOSF33_EQ(m, 0.f, 3.f, 6.f, 1.f, 4.f, 7.f, 2.f, 5.f, 8.f);

  CHK(aosf33_zero(m) == m);
  CHK(aosf33_set_col(m, v4f_set(0.f, 1.f, 2.f, 9.f), 0) == m);
  AOSF33_EQ(m, 0.f, 1.f, 2.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f);
  CHK(aosf33_set_col(m, v4f_set(3.f, 4.f, 5.f, 10.f), 1) == m);
  AOSF33_EQ(m, 0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 0.f, 0.f, 0.f);
  CHK(aosf33_set_col(m, v4f_set(6.f, 7.f, 8.f, 11.f), 2) == m);
  AOSF33_EQ(m, 0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f);

  v = aosf33_row0(m);
  CHK(v4f_x(v) == 0.f);
  CHK(v4f_y(v) == 3.f);
  CHK(v4f_z(v) == 6.f);

  v = aosf33_row1(m);
  CHK(v4f_x(v) == 1.f);
  CHK(v4f_y(v) == 4.f);
  CHK(v4f_z(v) == 7.f);

  v = aosf33_row2(m);
  CHK(v4f_x(v) == 2.f);
  CHK(v4f_y(v) == 5.f);
  CHK(v4f_z(v) == 8.f);

  v = aosf33_row(m, 0);
  CHK(v4f_x(v) == 0.f);
  CHK(v4f_y(v) == 3.f);
  CHK(v4f_z(v) == 6.f);

  v = aosf33_row(m, 1);
  CHK(v4f_x(v) == 1.f);
  CHK(v4f_y(v) == 4.f);
  CHK(v4f_z(v) == 7.f);

  v = aosf33_row(m, 2);
  CHK(v4f_x(v) == 2.f);
  CHK(v4f_y(v) == 5.f);
  CHK(v4f_z(v) == 8.f);

  v = aosf33_col(m, 0);
  CHK(v4f_x(v) == 0.f);
  CHK(v4f_y(v) == 1.f);
  CHK(v4f_z(v) == 2.f);

  v = aosf33_col(m, 1);
  CHK(v4f_x(v) == 3.f);
  CHK(v4f_y(v) == 4.f);
  CHK(v4f_z(v) == 5.f);

  v = aosf33_col(m, 2);
  CHK(v4f_x(v) == 6.f);
  CHK(v4f_y(v) == 7.f);
  CHK(v4f_z(v) == 8.f);

  aosf33_set(m,
    v4f_set(0.f, 1.f, 2.f, 0.f),
    v4f_set(3.f, 4.f, 5.f, 0.f),
    v4f_set(6.f, 7.f, 8.f, 0.f));
  aosf33_set(n,
    v4f_set(1.f, 2.f, 3.f, 0.f),
    v4f_set(4.f, 5.f, 6.f, 0.f),
    v4f_set(7.f, 8.f, 9.f, 0.f));
  CHK(aosf33_add(o, m, n) == o);
  AOSF33_EQ(o, 1.f, 3.f, 5.f, 7.f, 9.f, 11.f, 13.f, 15.f, 17.f);
  CHK(aosf33_sub(o, o, n) == o);
	AOSF33_EQ(o, 0.f, 1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f);

  aosf33_set(m,
    v4f_set(1.f, 2.f, -3.f, 0.f),
    v4f_set(-4.f, -5.f, 6.f, 0.f),
    v4f_set(7.f, -8.f, 9.f, 0.f));
  CHK(aosf33_minus(m, m) == m);
	AOSF33_EQ(m, -1.f, -2.f, 3.f, 4.f, 5.f, -6.f, -7.f, 8.f, -9.f);

  CHK(aosf33_mul(o, m, v4f_set1(2.f)) == o);
	AOSF33_EQ(o, -2.f, -4.f, 6.f, 8.f, 10.f, -12.f, -14.f, 16.f, -18.f);

  aosf33_set(m,
    v4f_set(1.f, 2.f, 3.f, 0.f),
    v4f_set(4.f, 5.f, 6.f, 0.f),
    v4f_set(7.f, 8.f, 9.f, 0.f));
  v = aosf33_mulf3(m, v4f_set(1.f, 2.f, 3.f, 0.f));
  CHK(v4f_x(v) == 30.f);
  CHK(v4f_y(v) == 36.f);
  CHK(v4f_z(v) == 42.f);
  v = aosf3_mulf33(v4f_set(1.f, 2.f, 3.f, 0.f), m);
  CHK(v4f_x(v) == 14.f);
  CHK(v4f_y(v) == 32.f);
  CHK(v4f_z(v) == 50.f);
  aosf33_set(n,
    v4f_set(2.f, 9.f, 8.f, 0.f),
    v4f_set(1.f, -2.f, 2.f, 0.f),
    v4f_set(1.f, -8.f, -4.f, 0.f));
  CHK(aosf33_mulf33(o, m, n) == o);
	AOSF33_EQ(o, 94.f, 113.f, 132.f, 7.f, 8.f, 9.f, -59.f, -70.f, -81.f);

  CHK(aosf33_transpose(o, m) == o);
	AOSF33_EQ(o, 1.f, 4.f, 7.f, 2.f, 5.f, 8.f, 3.f, 6.f, 9.f);

  aosf33_set(m,
    v4f_set(1.f, 2.f, 3.f, 0.f),
    v4f_set(4.f, 5.f, 6.f, 0.f),
    v4f_set(3.f, -4.f, 9.f, 0.f));
  v = aosf33_det(m);
  CHK(v4f_x(v) == -60.f);
  CHK(v4f_y(v) == -60.f);
  CHK(v4f_z(v) == -60.f);
  CHK(v4f_w(v) == -60.f);

  v = aosf33_inverse(n, m);
  CHK(v4f_x(v) == -60.f);
  CHK(v4f_y(v) == -60.f);
  CHK(v4f_z(v) == -60.f);
  CHK(v4f_w(v) == -60.f);
  aosf33_mulf33(o, m, n);
  AOSF33_EQ_EPS(o, 1.f, 0.f, 0.f, 0.f, 1.f, 0.f, 0.f, 0.f, 1.f, 1.e-6f);

  v = aosf33_invtrans(o, m);
  CHK(v4f_x(v) == -60.f);
  CHK(v4f_y(v) == -60.f);
  CHK(v4f_z(v) == -60.f);
  CHK(v4f_w(v) == -60.f);
  AOSF33_EQ(o,
    v4f_x(n[0]), v4f_x(n[1]), v4f_x(n[2]),
    v4f_y(n[0]), v4f_y(n[1]), v4f_y(n[2]),
    v4f_z(n[0]), v4f_z(n[1]), v4f_z(n[2]));
  return 0;
}

