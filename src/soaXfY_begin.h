/* Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
 *
 * The RSIMD library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The RSIMD library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the RSIMD library. If not, see <http://www.gnu.org/licenses/>. */

#include "rsimd.h"
#include "vXf_begin.h"

/* This file can be included once */
#ifdef SOAXFY_BEGIN_H
  #error "The soaXfY_begin.h header is already included"
#endif
#define SOAXFY_BEGIN_H

/* Check parameters */
#if !defined(RSIMD_SOA_DIMENSION__)
  #error "Undefined RSIMD_SOA_DIMENSION__ macro"
#endif
#if !defined(RSIMD_WIDTH__)
  #error "Undefined RSIMD_WIDTH__ macro"
#endif
#if RSIMD_SOA_DIMENSION__ < 1 || RSIMD_SOA_DIMENSION__ > 4
  #error "Unexpected RSIMD_SOA_DIMENSION__ value"
#endif
#if RSIMD_WIDTH__ != 4 && RSIMD_WIDTH__ != 8
  #error "Unexpected RSIMD_WIDTH__ value of "STR(RSIMD_WIDTH__)
#endif

/* Check that internal macros are not already defined */
#if defined(RSIMD_soaXfY_PREFIX__)                                             \
 || defined(RSIMD_soaXfY__)                                                    \
 || defined(SIZEOF_RSIMD_soaXfY__)
  #error "Unexpected macro definition"
#endif

/* Macros generic to RSIMD_WIDTH__ and RSIMD_SOA_DIMENSION__ */
#define RSIMD_soaXfY_PREFIX__ \
  CONCAT(CONCAT(CONCAT(soa, RSIMD_WIDTH__), f), RSIMD_SOA_DIMENSION__)
#define RSIMD_soaXfY__(Func) CONCAT(CONCAT(RSIMD_soaXfY_PREFIX__, _), Func)
#define SIZEOF_RSIMD_soaXfY__ sizeof(RSIMD_vXf_T__[RSIMD_SOA_DIMENSION__])

