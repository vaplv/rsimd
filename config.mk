VERSION = 0.5.0 # Library version
PREFIX = /usr/local

LIB_TYPE = SHARED
#LIB_TYPE = STATIC

BUILD_TYPE = RELEASE
#BUILD_TYPE = DEBUG

# If not set, SIMD WIDTH is retrieved from host CPU
#SIMD_WIDTH = 128
#SIMD_WIDTH = 256

################################################################################
# Tools
################################################################################
AR = ar
CC = cc
LD = ld
PKG_CONFIG = pkg-config
OBJCOPY = objcopy
RANLIB = ranlib

################################################################################
# Dependencies
################################################################################
PCFLAGS_SHARED =
PCFLAGS_STATIC = --static
PCFLAGS = $(PCFLAGS_$(LIB_TYPE))

RSYS_VERSION = 0.14
RSYS_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags rsys)
RSYS_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs rsys)

SLEEF_VERSION = 3.6
SLEEF_CFLAGS = $$($(PKG_CONFIG) $(PCFLAGS) --cflags sleef)
SLEEF_LIBS = $$($(PKG_CONFIG) $(PCFLAGS) --libs sleef)

DPDC_CFLAGS = $(RSYS_CFLAGS) $(SLEEF_CFLAGS)
DPDC_LIBS = $(RSYS_LIBS) $(SLEEF_LIBS)

################################################################################
# Compilation options
################################################################################
WFLAGS =\
 -Wall\
 -Wcast-align\
 -Wconversion\
 -Wextra\
 -Wmissing-declarations\
 -Wmissing-prototypes\
 -Wshadow

CFLAGS_HARDENED =\
 -D_FORTIFY_SOURCES=2\
 -fcf-protection=full\
 -fstack-clash-protection\
 -fstack-protector-strong

CFLAGS_SIMD = -march=native

CFLAGS_COMMON =\
 -pedantic\
 -fPIC\
 -fvisibility=hidden\
 -fstrict-aliasing\
 $(WFLAGS)\
 $(CFLAGS_HARDENED)\
 $(CFLAGS_SIMD)

CFLAGS_DEBUG = -g $(CFLAGS_COMMON)
CFLAGS_RELEASE = -O2 -DNDEBUG $(CFLAGS_COMMON)
CFLAGS = $(CFLAGS_$(BUILD_TYPE))

CFLAGS_SO = $(CFLAGS) -fPIC
CFLAGS_EXE = $(CFLAGS) -fPIE

################################################################################
# Linker options
################################################################################
LDFLAGS_HARDENED = -Wl,-z,relro,-z,now
LDFLAGS_DEBUG = $(LDFLAGS_HARDENED)
LDFLAGS_RELEASE = -s $(LDFLAGS_HARDENED)
LDFLAGS = $(LDFLAGS_$(BUILD_TYPE))

LDFLAGS_SO = $(LDFLAGS) -shared -Wl,--no-undefined
LDFLAGS_EXE = $(LDFLAGS) -pie

OCPFLAGS_DEBUG = --localize-hidden
OCPFLAGS_RELEASE = --localize-hidden --strip-unneeded
OCPFLAGS = $(OCPFLAGS_$(BUILD_TYPE))
