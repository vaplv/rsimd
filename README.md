# RSIMD

RSIMD is a C library whose purpose is to facilitate the use of SIMD
instruction sets. It implements linear algebra operations for 3x3 and
4x4 matrices stored in SIMD vectors as arrays of structures. Vectorized
linear algebra operations on structures of arrays, for 2, 3 and 4
dimensional vectors are also implemented.  Finally, it provides a
vectorized version of the standard mathematical functions.

## Requirements

- C compiler (C99)
- POSIX make
- [RSys](https://gitlab.com/vaplv/rsys/)
- [Sleef](https://sleef.org/)

## Installation

Edit config.mk as needed, then run:

    make clean install

## Release notes

### Version 0.5

- Replace CMake by Makefile as build system.
- Provide a pkg-config file to link the library as an external
  dependency.

### Version 0.4

- Make the sleef library an internal dependency that library users don't
  have to worry about: calls to the sleef library are moved from API
  headers to C files.
- Set the C standard to C99 instead of C89: this allows the use of
  official (i.e. unpatched) sleef headers whose comments are written
  using C99-style comments. Note that since sleef is now an internal
  dependency, the rsimd API remains C89 compliant.
- Remove the file COPYING.LESSER from the installation target: since
  version 0.3, the library is distributed under GPLv3+.

### Version 0.3

- Add 8-way vector API for the float and int32 types.
- Add the `v<4|8>i_[reduce_]<min|max>` functions.
- Add the `v4i_minus` function.
- Rely on the [Sleef](https://sleef.org) library to replace the
  hand-crafted implementation of the trigonometric functions.
- Add math functions for both 4-way and 8-way vectors. Provided math
  functions are: copysign, floor, pow, exp[<2|10>] and log[<2|10>].

### Version 0.2.1

- If supported by the compiler, use the SSE4.1 blendv instruction in the
  `v4f_sel` function
- Turns the RSIMD library in shared library.

## License

Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)

RSIMD is free software released under the GPL v3+ license: GNU GPL
version 3 or later.  You are welcome to redistribute it under certain
conditions; refer to the COPYING file for details.
