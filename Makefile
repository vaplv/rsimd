# Copyright (C) 2014-2019, 2021, 2023 Vincent Forest (vaplv@free.fr)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean up default inference rules

include config.mk

LIBNAME_STATIC = librsimd.a
LIBNAME_SHARED = librsimd.so
LIBNAME = $(LIBNAME_$(LIB_TYPE))

################################################################################
# Library building
################################################################################
SRC_SIMD128 = src/aosf44.c src/aosq.c src/math4.c
SRC_SIMD256 = src/math8.c $(SRC_SIMD128)
SRC = $(SRC_SIMD$(SIMD_WIDTH))
OBJ = $(SRC:.c=.o)
DEP = $(SRC:.c=.d)

build_library:

build_library__: .config $(DEP)
	@$(MAKE) -f.simd -fMakefile $$(for i in $(DEP); do echo -f $${i}; done) \
	$$(if [ -n "$(LIBNAME)" ]; then\
	     echo "$(LIBNAME)";\
	   else\
	     echo "$(LIBNAME_SHARED)";\
	   fi)

$(DEP) $(OBJ): config.mk

$(LIBNAME_SHARED): $(OBJ)
	$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -o $@ $(OBJ) $(LDFLAGS_SO) $(DPDC_LIBS)

$(LIBNAME_STATIC): librsimd.o
	$(AR) -rc $@ $?
	$(RANLIB) $@

librsimd.o: $(OBJ)
	$(LD) -r $(OBJ) -o $@
	$(OBJCOPY) $(OCPFLAGS) $@

.config: make.sh config.mk
	@if ! $(PKG_CONFIG) --atleast-version $(RSYS_VERSION) rsys; then \
	  echo "rsys $(RSYS_VERSION) not found"; exit 1; fi
	@if ! $(PKG_CONFIG) --atleast-version $(SLEEF_VERSION) sleef; then \
	  echo "sleef $(SLEEF_VERSION) not found"; exit 1; fi
	@echo "config done" > $@

.simd: make.sh config.mk
	@$(SHELL) make.sh config_simd $(MAKE) > $@

.SUFFIXES: .c .d .o
.c.d:
	@$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -MM -MT "$(@:.d=.o) $@" $< -MF $@

.c.o:
	$(CC) -std=c99 $(CFLAGS_SO) $(DPDC_CFLAGS) -DRSIMD_SHARED_BUILD -c $< -o $@

################################################################################
# Miscellaneous targets
################################################################################
all: build_library build_tests

clean__: clean_test
	rm -f $(OBJ) $(TEST_OBJ) $(LIBNAME)
	rm -f .config .simd .test librsimd.o rsimd.pc rsimd-local.pc

distclean__: clean__
	rm -f $(DEP) $(TEST_DEP) .test

lint:
	shellcheck -o all make.sh

build_library build_tests clean distclean install .test test uninstall: .simd
	@$(MAKE) -f.simd -fMakefile $@__

################################################################################
# Installation
################################################################################
API_SIMD128=\
 src/aosf33.h\
 src/aosf44.h\
 src/aosq.h\
 src/math.h\
 src/mathX.h\
 src/math4.h\
 src/rsimd.h\
 src/soaXfY.h\
 src/soaXfY_begin.h\
 src/soaXfY_end.h\
 src/soaXf2.h\
 src/soaXf3.h\
 src/soa4f2.h\
 src/soa4f3.h\
 src/soa4f4.h\
 src/vXf_begin.h\
 src/vXf_end.h\
 src/sse/sse.h\
 src/sse/ssef.h\
 src/sse/ssei.h\
 src/sse/sse_swz.h
API_SIMD256=\
 src/math8.h\
 src/soa8f2.h\
 src/soa8f3.h\
 src/soa8f4.h\
 src/avx/avx.h\
 src/avx/avxf.h\
 src/avx/avxi.h\
 $(API_SIMD128)
API = $(API_SIMD$(SIMD_WIDTH))

pkg:
	sed -e 's#@PREFIX@#$(PREFIX)#g' \
	    -e 's#@VERSION@#$(VERSION)#g' \
	    -e 's#@SLEEF_VERSION@#$(SLEEF_VERSION)#g' \
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g' \
	    -e 's#@CFLAGS_SIMD@#$(CFLAGS_SIMD)#g' \
	    rsimd.pc.in > rsimd.pc

# Remove the include directive rather than setting it to "./src". to prevent
# the source directory from having a higher priority than the system include
# directories. In such a situation, the local "math.h" file could be included
# instead of the "math.h" header provided by the C standard library. Note that
# this is no longer a problem with the common pc file: the "math.h" file is
# installed in the "rsimd" subdirectory, which is therefore a prefix of the
# header file allowing it to be distinguished from the header of the standard
# library
rsimd-local.pc: rsimd.pc.in
	sed -e '1,2d'\
	    -e 's#^libdir=.*#libdir=./#'\
	    -e 's#@VERSION@#$(VERSION)#g' \
	    -e 's#@SLEEF_VERSION@#$(SLEEF_VERSION)#g' \
	    -e 's#@RSYS_VERSION@#$(RSYS_VERSION)#g' \
	    -e 's#@CFLAGS_SIMD@#$(CFLAGS_SIMD)#g' \
	    -e 's#-I$${includedir}##g'\
	    rsimd.pc.in > $@

install__:  build_library pkg
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib" $(LIBNAME)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/lib/pkgconfig" rsimd.pc
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/include/rsimd" $(API)
	@$(SHELL) make.sh install "$(DESTDIR)$(PREFIX)/share/doc/rsimd" COPYING README.md

uninstall__:
	rm -f $(DESTDIR)$(PREFIX)/lib/$(LIBNAME)
	rm -f $(DESTDIR)$(PREFIX)/lib/pkgconfig/rsimd.pc
	rm -f $(DESTDIR)$(PREFIX)/share/doc/rsimd/COPYING
	rm -f $(DESTDIR)$(PREFIX)/share/doc/rsimd/README.md
	rm -f $$(echo $(API) | sed 's,src\/,$(DESTDIR)$(PREFIX)\/include\/rsimd\/,g')

################################################################################
# Tests
################################################################################
TEST_SIMD128=\
 src/test_aosf33.c\
 src/test_aosf44.c\
 src/test_aosq.c\
 src/test_math4.c\
 src/test_soa4f2.c\
 src/test_soa4f3.c\
 src/test_soa4f4.c\
 src/test_v4f.c\
 src/test_v4i.c
TEST_SIMD256=\
 src/test_math8.c\
 src/test_soa8f2.c\
 src/test_soa8f3.c\
 src/test_soa8f4.c\
 $(TEST_SIMD128)
TEST_SRC = $(TEST_SIMD$(SIMD_WIDTH))
TEST_OBJ = $(TEST_SRC:.c=.o)
TEST_DEP = $(TEST_SRC:.c=.d)

PKG_CONFIG_LOCAL = PKG_CONFIG_PATH="./:$${PKG_CONFIG_PATH}" $(PKG_CONFIG)
RSIMD_CFLAGS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --cflags rsimd-local.pc)
RSIMD_LIBS = $$($(PKG_CONFIG_LOCAL) $(PCFLAGS) --libs rsimd-local.pc)

build_tests__: build_library $(TEST_DEP) .test .simd
	@$(MAKE) -f.simd -fMakefile -f.test \
	$$(for i in $(TEST_DEP); do echo -f"$${i}"; done) test_bin

test__: build_tests__
	@$(SHELL) make.sh run_test $(TEST_SRC)

.test__: Makefile make.sh
	@$(SHELL) make.sh config_test $(TEST_SRC) > .test

clean_test:
	@$(SHELL) make.sh clean_test $(TEST_SRC)

$(TEST_DEP): config.mk rsimd-local.pc
	@$(CC) -std=c89 $(CFLAGS_EXE) $(RSIMD_CFLAGS) $(RSYS_CFLAGS) \
	-MM -MT "$(@:.d=.o) $@" $(@:.d=.c) -MF $@

$(TEST_OBJ): config.mk rsimd-local.pc
	$(CC) -std=c89 $(CFLAGS_EXE) $(RSIMD_CFLAGS) $(RSYS_CFLAGS) -c $(@:.o=.c) -o $@

test_aosf33 \
test_aosf44 \
test_aosq \
test_soa4f2 \
test_soa4f3 \
test_soa4f4 \
test_soa8f2 \
test_soa8f3 \
test_soa8f4 \
test_v4f \
test_v4i \
: config.mk rsimd-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(RSIMD_LIBS) $(RSYS_LIBS)

test_math4 \
test_math8 \
: config.mk rsimd-local.pc $(LIBNAME)
	$(CC) $(CFLAGS_EXE) -o $@ src/$@.o $(LDFLAGS_EXE) $(RSIMD_LIBS) $(RSYS_LIBS) -lm
